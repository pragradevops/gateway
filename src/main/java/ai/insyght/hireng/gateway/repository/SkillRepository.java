package ai.insyght.hireng.gateway.repository;

import ai.insyght.hireng.gateway.domain.Skill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SkillRepository extends JpaRepository<Skill, Long> {

    @Query("select skill from Skill skill where lower(skill.skill) like lower(concat(?1,'%'))")
    List<Skill> findBySkillContainingIgnoreCase(String skillInitial);

    List<Skill> findAll();

}
