package ai.insyght.hireng.gateway.repository;

import ai.insyght.hireng.gateway.domain.Specialization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SpecializationRepository extends JpaRepository<Specialization, Long> {

    @Query("select specialization from Specialization specialization where lower(specialization.specialization) like lower(concat(?1,'%'))")
    List<Specialization> findBySpecializationContainingIgnoreCase(String specializationInitial);

    List<Specialization> findAll();
}
