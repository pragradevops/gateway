package ai.insyght.hireng.gateway.repository;

import ai.insyght.hireng.gateway.domain.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LocationRepository extends JpaRepository<Location, Long> {

    @Query("select location from Location location where lower(location.city) like lower(concat(?1,'%'))")
    List<Location> findByCityNameContainingIgnoreCase(String cityInitial);
}
