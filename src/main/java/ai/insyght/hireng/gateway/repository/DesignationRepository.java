package ai.insyght.hireng.gateway.repository;

import ai.insyght.hireng.gateway.domain.Designation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DesignationRepository extends JpaRepository<Designation, Long> {

    @Query("select designation from Designation designation where lower(designation.designation) like lower(concat(?1,'%'))")
    List<Designation> findByDesignationContainingIgnoreCase(String designationInitial);

    List<Designation> findAll();
}
