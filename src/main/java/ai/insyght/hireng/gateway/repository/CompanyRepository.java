package ai.insyght.hireng.gateway.repository;

import ai.insyght.hireng.gateway.domain.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CompanyRepository extends JpaRepository<Company, Long> {

    @Query("select company from Company company where lower(company.company) like lower(concat(?1,'%'))")
    List<Company> findByCompanyContainingIgnoreCase(String companyInitial);

    List<Company> findAll();
}
