package ai.insyght.hireng.gateway.repository;

import ai.insyght.hireng.gateway.domain.UniversityBoard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UniversityBoardRepository extends JpaRepository<UniversityBoard, Long> {

    @Query("select universityBoard from UniversityBoard universityBoard where lower(universityBoard.universityBoardName) like lower(concat(?1,'%'))")
    List<UniversityBoard> findBySpecializationContainingIgnoreCase(String universityBoardInitial);

    List<UniversityBoard> findAll();

}
