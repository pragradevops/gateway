package ai.insyght.hireng.gateway.repository;

import ai.insyght.hireng.gateway.domain.Qualification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QualificationRepository extends JpaRepository<Qualification, Long> {

    @Query("select qualification from Qualification qualification where lower(qualification.qualification) like lower(concat(?1,'%'))")
    List<Qualification> findByQualificationContainingIgnoreCase(String qualificationInitial);

    List<Qualification> findAll();
}
