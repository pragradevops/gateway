package ai.insyght.hireng.gateway.repository;

import ai.insyght.hireng.gateway.domain.Discipline;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DisciplineRepository  extends JpaRepository<Discipline, Long> {

    @Query("select discipline from Discipline discipline where lower(discipline.discipline) like lower(concat(?1,'%'))")
    List<Discipline> findByDisciplineContainingIgnoreCase(String disciplineInitial);

    List<Discipline> findAll();

}
