package ai.insyght.hireng.gateway.web.rest.vm;

public class RegisterWithEmailVM {

    private String email;
    private String role;
    private String langKey;

    public String getEmail() {
        return email;
    }

    public String getRole() {
        return role;
    }

    public String getLangKey() {
        return langKey;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setLangKey(String langKey) {
        this.langKey = langKey;
    }

}
