package ai.insyght.hireng.gateway.web.rest.vm;

import ai.insyght.hireng.gateway.domain.enumeration.GenderEnum;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class FirstLastNameAndImageUrlVM {

    @NotNull
    private String firstName;

    private String lastName;

    private String imageUrl;

    @NotNull
    private String mobile;

    private String alternativeMobile;

    @NotNull
    private Date dateOfBirth;

    @NotNull
    private GenderEnum gender;

    @NotNull
    private String addressLine;

    @NotNull
    private  String lat;

    @NotNull
    private String lng;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAlternativeMobile() {
        return alternativeMobile;
    }

    public void setAlternativeMobile(String alternativeMobile) {
        this.alternativeMobile = alternativeMobile;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public GenderEnum getGender() {
        return gender;
    }

    public void setGender(GenderEnum gender) {
        this.gender = gender;
    }

    public String getAddressLine() {
        return addressLine;
    }

    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
}
