package ai.insyght.hireng.gateway.web.rest;

import ai.insyght.hireng.gateway.service.*;
import ai.insyght.hireng.gateway.service.dto.*;
import ai.insyght.hireng.gateway.web.rest.errors.InternalServerErrorException;
import com.codahale.metrics.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class ReferenceResource {

    private final Logger log = LoggerFactory.getLogger(ReferenceResource.class);

    private final SkillService skillService;

    private final CompanyService companyService;

    private final DesignationService designationService;

    private final QualificationService qualificationService;

    private final UniversityBoardService universityBoardService;

    private final LocationService locationService;

    public ReferenceResource(SkillService skillService, CompanyService companyService,
                             DesignationService designationService, QualificationService qualificationService,
                             UniversityBoardService universityBoardService, LocationService locationService) {
        this.skillService = skillService;
        this.companyService = companyService;
        this.designationService = designationService;
        this.qualificationService = qualificationService;
        this.universityBoardService = universityBoardService;
        this.locationService = locationService;
    }

    /**
     * GET  /city/:cityName : get the "countryId" City.
     *
     * @param cityInitial to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the completeProfileDTO, or with status 404 (Not Found)
     */
    @GetMapping ("/locations/{cityInitial}")
    @Timed
    public List<String> getCityByCountryId(@PathVariable String cityInitial){
        List<String> location = locationService.getCitiesByInitial(cityInitial);
        if (!location.isEmpty())
            return location;
        throw new InternalServerErrorException("No data found");
    }

    /**
     * GET  /reference-skills get skills
     *
     * @return the ResponseEntity with status 200 (OK) and with body the skillDTOS, or with status 404 (Not Found)
     */
    @GetMapping ("/skills")
    @Timed
    public List<SkillDTO> getAllSkills(){
        return skillService.getSkillList().stream()
            .filter(Objects::nonNull)
            .map(SkillDTO::new)
            .collect(Collectors.toList());
    }

    /**
     * GET  /skill/{skillInitial} get skills DTOs
     * @param skillInitial to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the skillDTOS, or with status 404 (Not Found)
     */
    @GetMapping ("/skill/{skillInitial}")
    @Timed
    public List<SkillDTO> getAllSkills(@PathVariable String skillInitial){

        return skillService.getQualificationsByInitial(skillInitial)
            .stream()
            .filter(Objects::nonNull)
            .map(SkillDTO::new)
            .collect(Collectors.toList());
    }

    /**
     * GET  /reference-companies get companies
     *
     * @return the ResponseEntity with status 200 (OK) and with body the companyDTOS, or with status 404 (Not Found)
     */
    @GetMapping ("/companies")
    @Timed
    public List<CompanyDTO> getAllCompanies(){
        return companyService.getCompanyList().stream()
            .filter(Objects::nonNull)
            .map(CompanyDTO::new)
            .collect(Collectors.toList());
    }

    /**
     * GET  /reference-companies get companies
     * @param companyInitial
     * @return the ResponseEntity with status 200 (OK) and with body the companyDTOS, or with status 404 (Not Found)
     */
    @GetMapping ("/company/{companyInitial}")
    @Timed
    public List<CompanyDTO> getAllCompanies(@PathVariable String companyInitial){
        return companyService.getCompaniesByInitial(companyInitial)
            .stream()
            .filter(Objects::nonNull)
            .map(CompanyDTO::new)
            .collect(Collectors.toList());
    }

    /**
     * GET  /reference-companies get designation
     *
     * @return the ResponseEntity with status 200 (OK) and with body the companyDTOS, or with status 404 (Not Found)
     */
    @GetMapping ("/designations")
    @Timed
    public List<DesignationDTO> getAllDesignation(){
        return designationService.getDesignationList().stream()
            .filter(Objects::nonNull)
            .map(DesignationDTO::new)
            .collect(Collectors.toList());
    }

    /**
     * GET  /reference-companies get designation
     * @param designationInitial
     * @return the ResponseEntity with status 200 (OK) and with body the companyDTOS, or with status 404 (Not Found)
     */
    @GetMapping ("/designations/{designationInitial}")
    @Timed
    public List<DesignationDTO> getAllDesignation(@PathVariable String designationInitial){
        return designationService.getDesignationsByInitial(designationInitial)
            .stream()
            .filter(Objects::nonNull)
            .map(DesignationDTO::new)
            .collect(Collectors.toList());
    }

    /**
     * GET  /reference-companies get qualification
     *
     * @return the ResponseEntity with status 200 (OK) and with body the companyDTOS, or with status 404 (Not Found)
     */
    @GetMapping ("/qualifications")
    @Timed
    public List<QualificationDTO> getAllQualifications(){
        return qualificationService.getQualificationList().stream()
            .filter(Objects::nonNull)
            .map(QualificationDTO::new)
            .collect(Collectors.toList());
    }

    /**
     * GET  /reference-companies get qualification
     * @param qualificationsInitial
     * @return the ResponseEntity with status 200 (OK) and with body the companyDTOS, or with status 404 (Not Found)
     */
    @GetMapping ("/qualifications/{qualificationsInitial}")
    @Timed
    public List<QualificationDTO> getAllQualifications(@PathVariable String qualificationsInitial){
        return qualificationService.getQualificationsByInitial(qualificationsInitial)
            .stream()
            .filter(Objects::nonNull)
            .map(QualificationDTO::new)
            .collect(Collectors.toList());
    }


    /**
     * GET  /reference-companies get universities board
     *
     * @return the ResponseEntity with status 200 (OK) and with body the companyDTOS, or with status 404 (Not Found)
     */
    @GetMapping ("/universitiesBoard")
    @Timed
    public List<UniversityBoardDTO> getAllUniversities(){
        return universityBoardService.GetUniversityBoardList().stream()
            .filter(Objects::nonNull)
            .map(UniversityBoardDTO::new)
            .collect(Collectors.toList());
    }

    /**
     * GET  /reference-companies get universities board
     * @param
     * @return the ResponseEntity with status 200 (OK) and with body the companyDTOS, or with status 404 (Not Found)
     */
    @GetMapping ("/universitiesBoard/{universitiesBoardInitial}")
    @Timed
    public List<UniversityBoardDTO> getAllUniversities(@PathVariable String universitiesBoardInitial ){
        return universityBoardService.getUniversityBoardByInitial(universitiesBoardInitial)
            .stream()
            .filter(Objects::nonNull)
            .map(UniversityBoardDTO::new)
            .collect(Collectors.toList());
    }


}
