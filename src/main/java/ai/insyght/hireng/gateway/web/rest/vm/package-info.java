/**
 * View Models used by Spring MVC REST controllers.
 */
package ai.insyght.hireng.gateway.web.rest.vm;
