package ai.insyght.hireng.gateway.config;


import ai.insyght.hireng.gateway.messaging.Consumer;
import ai.insyght.hireng.gateway.messaging.Producer;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;

@EnableBinding(value = {Source.class , Producer.class , Consumer.class})
public class MessagingConfiguration {
}
