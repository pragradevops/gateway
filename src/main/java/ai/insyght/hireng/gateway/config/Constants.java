package ai.insyght.hireng.gateway.config;

import java.util.regex.Pattern;

/**
 * Application constants.
 */
public final class Constants {

    // Regex for acceptable logins
    public static final String LOGIN_REGEX = "^[_'.@A-Za-z0-9-]*$";
    //Regex for acceptable document format
    public static Pattern DOCUMENT_FILE_EXTENSION = Pattern.compile("([^\\s]+(\\.(?i)(txt|doc|pdf))$)");
    //Regex for acceptable image format
    public static Pattern IMAGE_FILE_EXTENSION = Pattern.compile("([^\\s]+(\\.(?i)(png|jpeg|jpg))$)");

    public static final String SYSTEM_ACCOUNT = "system";
    public static final String ANONYMOUS_USER = "anonymoususer";
    public static final String DEFAULT_LANGUAGE = "en";

    // Maximum Size of user image 1 mb
    public static final Long MAX_IMAGE_SIZE = 1048576L;
    //Maximum Size of user document  1 mb
    public static final Long MAX_DOCUMENT_SIZE = 1048576L;

    //File type of users
    public static final String DOCUMENT_FILE = "docfile";
    public static final String IMAGE_FILE = "imgfile";

    private Constants() {
    }
}
