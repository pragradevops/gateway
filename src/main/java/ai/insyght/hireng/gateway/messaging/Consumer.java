package ai.insyght.hireng.gateway.messaging;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;


public interface Consumer {

   String CHANNEL = "profileSubscribeChannel";

   @Input
   SubscribableChannel subscriberChannel();
}
