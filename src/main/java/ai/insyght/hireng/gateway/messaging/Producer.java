package ai.insyght.hireng.gateway.messaging;


import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface Producer {

   String CHANNEL = "profileMessageChannel";

   @Output
   MessageChannel messageChannel();
}
