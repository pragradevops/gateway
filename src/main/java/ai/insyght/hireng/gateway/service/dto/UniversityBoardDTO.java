package ai.insyght.hireng.gateway.service.dto;

import ai.insyght.hireng.gateway.domain.UniversityBoard;

import javax.validation.constraints.NotNull;

public class UniversityBoardDTO {

    private Long id;

    @NotNull
    private String UniversityBoardName;

    public UniversityBoardDTO() {}

    public UniversityBoardDTO(Long id, String universityBoardName) {
        this.id = id;
        UniversityBoardName = universityBoardName;
    }

    public UniversityBoardDTO(UniversityBoard universityBoard) {
        this.id = universityBoard.getId();
        UniversityBoardName = universityBoard.getUniversityBoardName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUniversityBoardName() {
        return UniversityBoardName;
    }

    public void setUniversityBoardName(String universityBoardName) {
        UniversityBoardName = universityBoardName;
    }
}
