package ai.insyght.hireng.gateway.service.mapper;

import ai.insyght.hireng.gateway.domain.UniversityBoard;
import ai.insyght.hireng.gateway.service.dto.UniversityBoardDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity CompleteProfile and its DTO CompleteProfileDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface UniversityBoardMapper extends EntityMapper<UniversityBoardDTO, UniversityBoard> {
}
