package ai.insyght.hireng.gateway.service.mapper;

import ai.insyght.hireng.gateway.domain.Company;
import ai.insyght.hireng.gateway.service.dto.CompanyDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity CompleteProfile and its DTO CompleteProfileDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CompanyMapper extends EntityMapper<CompanyDTO, Company> {
}

