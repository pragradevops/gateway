package ai.insyght.hireng.gateway.service;

import ai.insyght.hireng.gateway.service.mapper.SkillMapper;
import ai.insyght.hireng.gateway.domain.Skill;
import ai.insyght.hireng.gateway.repository.SkillRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class SkillService {

    private final Logger log = LoggerFactory.getLogger(SkillService.class);

    private final SkillRepository skillRepository;

    public SkillService(SkillRepository skillRepository, SkillMapper skillMapper) {
        this.skillRepository = skillRepository;
    }

    public List<Skill> getSkillList(){
        return skillRepository.findAll();
    }

    /**
     * Return List of Skill using initial
     */
    public List<Skill> getQualificationsByInitial(String skillInitial) {
        return skillRepository.findBySkillContainingIgnoreCase(skillInitial);
    }
}
