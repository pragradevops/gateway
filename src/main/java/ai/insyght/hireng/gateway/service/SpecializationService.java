package ai.insyght.hireng.gateway.service;

import ai.insyght.hireng.gateway.domain.Specialization;
import ai.insyght.hireng.gateway.repository.SpecializationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class SpecializationService {

    private final Logger log = LoggerFactory.getLogger(SpecializationService.class);

    private final SpecializationRepository specializationRepository;

    public SpecializationService(SpecializationRepository specializationRepository) {
        this.specializationRepository = specializationRepository;
    }

    List<Specialization> getSpecializationList() {
        return specializationRepository.findAll();
    }

    /**
     * Return List of Specialization using initial
     */
    public List<Specialization> getSpecializationsByInitial(String specializationInitial) {
        return specializationRepository.findBySpecializationContainingIgnoreCase(specializationInitial);
    }
}
