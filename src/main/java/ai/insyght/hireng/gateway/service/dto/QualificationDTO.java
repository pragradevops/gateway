package ai.insyght.hireng.gateway.service.dto;

import ai.insyght.hireng.gateway.domain.Qualification;

import javax.validation.constraints.NotNull;
import java.util.Objects;

public class QualificationDTO {

    private Long id;

    @NotNull
    private String qualification;

    public QualificationDTO() {}

    public QualificationDTO(Long id, String qualification) {
        this.id = id;
        this.qualification = qualification;
    }

    public QualificationDTO(Qualification qualification) {
        this.id = qualification.getId();
        this.qualification = qualification.getQualification();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QualificationDTO that = (QualificationDTO) o;
        return Objects.equals(id, that.id) && Objects.equals(qualification, that.qualification);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, qualification);
    }

    @Override
    public String toString() {
        return "QualificationDTO{" + "id=" + id + ", qualification='" + qualification + '\'' + '}';
    }
}
