package ai.insyght.hireng.gateway.service;

import ai.insyght.hireng.gateway.security.AuthoritiesConstants;
import ai.insyght.hireng.gateway.security.SecurityUtils;
import ai.insyght.hireng.gateway.service.dto.UserDTO;
import ai.insyght.hireng.gateway.service.util.RandomUtil;
import ai.insyght.hireng.gateway.domain.Authority;
import ai.insyght.hireng.gateway.domain.User;
import ai.insyght.hireng.gateway.domain.UserProfile;
import ai.insyght.hireng.gateway.domain.enumeration.GenderEnum;
//import io.aleeph.hireng.gateway.messaging.Producer;
import ai.insyght.hireng.gateway.repository.AuthorityRepository;
import ai.insyght.hireng.gateway.config.Constants;
import ai.insyght.hireng.gateway.repository.UserRepository;
//import io.aleeph.hireng.gateway.repository.search.//UserSearchRepository;

import ai.insyght.hireng.gateway.web.rest.vm.FirstLastNameAndImageUrlVM;
import ai.insyght.hireng.gateway.web.rest.vm.KeyLoginAndPasswordVM;
import ai.insyght.hireng.gateway.web.rest.vm.RegisterWithEmailVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import ai.insyght.hireng.gateway.web.rest.errors.InvalidPasswordException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserService {

    private final Logger log = LoggerFactory.getLogger(UserService.class);

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    //private final //UserSearchRepository userSearchRepository;

    private final AuthorityRepository authorityRepository;

    private final CacheManager cacheManager;

   // private final MessageChannel channel;

    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder,
                       //UserSearchRepository userSearchRepository,
                       AuthorityRepository authorityRepository,
                       CacheManager cacheManager
                       //Producer channel
                       ) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        //this.userSearchRepository = userSearchRepository;
        this.authorityRepository = authorityRepository;
        this.cacheManager = cacheManager;
        //this.channel = channel.messageChannel();
    }

    public Optional<User> activateRegistration(String key) {
        log.debug("Activating user for activation key {}", key);
        return userRepository.findOneByActivationKey(key)
            .map(user -> {
                // activate given user for the registration key.
                user.setActivated(true);
                user.setActivationKey(null);
                //userSearchRepository.save(user);
                this.clearUserCaches(user);
                log.debug("Activated user: {}", user);
                UserProfile profile = new UserProfile();
//                profile.setUserId(user.getId());
                log.debug("Creating Message for profile ms with payload : {}", profile);

            //    channel.send(MessageBuilder.withPayload(profile).build());

                return user;
            });
    }

    //user password and login set-up for register e-mail (account activation)
    public Optional<User> activateUserRegistration(KeyLoginAndPasswordVM userLoginDetails) {
        log.debug("Activating user for activation key {}", userLoginDetails.getKey());
        return userRepository.findOneByActivationKey(userLoginDetails.getKey())
            .map(user -> {
                // activate given user for the registration key.
                user.setActivated(true);
                user.setActivationKey(null);
                user.setLogin(userLoginDetails.getLogin());
                user.setPassword(passwordEncoder.encode(userLoginDetails.getPassword()));
                //userSearchRepository.save(user);
                cacheManager.getCache(UserRepository.USERS_BY_LOGIN_CACHE).evict(user.getLogin());
                cacheManager.getCache(UserRepository.USERS_BY_EMAIL_CACHE).evict(user.getEmail());
                log.debug("Activated user: {}", user);
                UserProfile profile = new UserProfile();
//                profile.setUserId(user.getId());
                log.debug("Creating Message for profile ms with payload : {}", profile);

       //         channel.send(MessageBuilder.withPayload(profile).build());

                return user;
            });
    }

    public Optional<User> completePasswordReset(String newPassword, String key) {
       log.debug("Reset user password for reset key {}", key);

       return userRepository.findOneByResetKey(key)
           .filter(user -> user.getResetDate().isAfter(Instant.now().minusSeconds(86400)))
           .map(user -> {
                user.setPassword(passwordEncoder.encode(newPassword));
                user.setResetKey(null);
                user.setResetDate(null);
                this.clearUserCaches(user);
                return user;
           });
    }

    public Optional<User> requestPasswordReset(String mail) {
        return userRepository.findOneByEmailIgnoreCase(mail)
            .filter(User::getActivated)
            .map(user -> {
                user.setResetKey(RandomUtil.generateResetKey());
                user.setResetDate(Instant.now());
                this.clearUserCaches(user);
                return user;
            });
    }

    public User registerUser(UserDTO userDTO, String password) {

        User newUser = new User();
        String encryptedPassword = passwordEncoder.encode(password);
        newUser.setLogin(userDTO.getLogin());
        // new user gets initially a generated password
        newUser.setPassword(encryptedPassword);
        newUser.setFirstName(userDTO.getFirstName());
        newUser.setLastName(userDTO.getLastName());
        newUser.setEmail(userDTO.getEmail());
        newUser.setImageUrl(userDTO.getImageUrl());
        newUser.setLangKey(userDTO.getLangKey());
        // new user is not active
        newUser.setActivated(false);
        // new user gets registration key
        newUser.setActivationKey(RandomUtil.generateActivationKey());
        Set<Authority> authorities = new HashSet<>();
        authorityRepository.findById(AuthoritiesConstants.USER).ifPresent(authorities::add);
        newUser.setAuthorities(authorities);
        userRepository.save(newUser);
        //userSearchRepository.save(newUser);
        this.clearUserCaches(newUser);
        log.debug("Created Information for User: {}", newUser);
        return newUser;
    }

    //User registration with email-id
    public User registerUserWithEmail(RegisterWithEmailVM registerWithEmailVM) {
        User newUser = new User();
        log.debug("Created Information for User Role: {}", registerWithEmailVM.getRole());
        Authority authority = authorityRepository.findById(AuthoritiesConstants.USER).get();
        if (AuthoritiesConstants.EMPLOYER.equals(registerWithEmailVM.getRole()))
            authority = authorityRepository.findById(AuthoritiesConstants.EMPLOYER).get();
        else if(AuthoritiesConstants.TALENT.equals(registerWithEmailVM.getRole()))
            authority = authorityRepository.findById(AuthoritiesConstants.TALENT).get();
        Set<Authority> authorities = new HashSet<>();
        String encryptedPassword = passwordEncoder.encode(RandomUtil.generatePassword());
        newUser.setLogin(RandomUtil.generateTempLogin());
        newUser.setPassword(encryptedPassword);
        newUser.setEmail(registerWithEmailVM.getEmail());
        newUser.setLangKey(registerWithEmailVM.getLangKey());
        // new user is not active
        newUser.setActivated(false);
        // new user gets registration key
        newUser.setActivationKey(RandomUtil.generateActivationKey());
        authorities.add(authority);
        newUser.setAuthorities(authorities);
        userRepository.save(newUser);
        //userSearchRepository.save(newUser);
        cacheManager.getCache(UserRepository.USERS_BY_LOGIN_CACHE).evict(newUser.getLogin());
        cacheManager.getCache(UserRepository.USERS_BY_EMAIL_CACHE).evict(newUser.getEmail());
        log.debug("Created Information for User: {}", newUser);
        return newUser;
    }

    public User createUser(UserDTO userDTO) {
        User user = new User();
        user.setLogin(userDTO.getLogin());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setEmail(userDTO.getEmail());
        user.setImageUrl(userDTO.getImageUrl());
        if (userDTO.getLangKey() == null) {
            user.setLangKey(Constants.DEFAULT_LANGUAGE); // default language
        } else {
            user.setLangKey(userDTO.getLangKey());
        }
        if (userDTO.getAuthorities() != null) {
            Set<Authority> authorities = userDTO.getAuthorities().stream()
                .map(authorityRepository::findById)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet());
            user.setAuthorities(authorities);
        }
        String encryptedPassword = passwordEncoder.encode(RandomUtil.generatePassword());
        user.setPassword(encryptedPassword);
        user.setResetKey(RandomUtil.generateResetKey());
        user.setResetDate(Instant.now());
        user.setActivated(true);
        userRepository.save(user);
        //userSearchRepository.save(user);
        this.clearUserCaches(user);
        log.debug("Created Information for User: {}", user);
        return user;
    }

    /**
     * Update basic information (first name, last name, email, language) for the current user.
     *
     * @param firstName first name of user
     * @param lastName last name of user
     * @param email email id of user
     * @param langKey language key
     * @param imageUrl image URL of user
     */
    public void updateUser(String firstName, String lastName, String email, String langKey, String imageUrl) {
        SecurityUtils.getCurrentUserLogin()
            .flatMap(userRepository::findOneByLogin)
            .ifPresent(user -> {
                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setEmail(email);
                user.setLangKey(langKey);
                user.setImageUrl(imageUrl);
                //userSearchRepository.save(user);
                this.clearUserCaches(user);
                log.debug("Changed Information for User: {}", user);
            });
    }

    /**
     * Update basic information (first name, last name, email, language) for the current user.
     *
     * @param basicDetails of user
     */
    public User updateUser(FirstLastNameAndImageUrlVM basicDetails) {
        User user = SecurityUtils.getCurrentUserLogin()
            .flatMap(userRepository::findOneByLogin).orElse(null);
        if (user != null){
            user.setFirstName(basicDetails.getFirstName());
            user.setMobile(basicDetails.getMobile());
            user.setDateOfBirth(basicDetails.getDateOfBirth());
            if (GenderEnum.MALE.equals(basicDetails.getGender()))
                user.setGender(GenderEnum.MALE);
            else if (GenderEnum.FEMALE.equals(basicDetails.getGender()))
                user.setGender(GenderEnum.FEMALE);
            else
                user.setGender(GenderEnum.NOT_DEFINED);
            if (basicDetails.getLastName()!= null)
                user.setLastName(basicDetails.getLastName());
            if (basicDetails.getImageUrl()!= null)
                user.setImageUrl(basicDetails.getImageUrl());
            if (basicDetails.getAlternativeMobile()!=null)
                user.setAlternativeMobile(basicDetails.getAlternativeMobile());
        }
        log.debug("Changed Information for User: {}", user);
        return user;
    }

    /**
     * Update all information for a specific user, and return the modified user.
     *
     * @param userDTO user to update
     * @return updated user
     */
    public Optional<UserDTO> updateUser(UserDTO userDTO) {
        return Optional.of(userRepository
            .findById(userDTO.getId()))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(user -> {
                this.clearUserCaches(user);
                user.setLogin(userDTO.getLogin());
                user.setFirstName(userDTO.getFirstName());
                user.setLastName(userDTO.getLastName());
                user.setEmail(userDTO.getEmail());
                user.setImageUrl(userDTO.getImageUrl());
                user.setActivated(userDTO.isActivated());
                user.setLangKey(userDTO.getLangKey());
                Set<Authority> managedAuthorities = user.getAuthorities();
                managedAuthorities.clear();
                userDTO.getAuthorities().stream()
                    .map(authorityRepository::findById)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .forEach(managedAuthorities::add);
                //userSearchRepository.save(user);
                this.clearUserCaches(user);
                log.debug("Changed Information for User: {}", user);
                return user;
            })
            .map(UserDTO::new);
    }

    public void deleteUser(String login) {
        userRepository.findOneByLogin(login).ifPresent(user -> {
            userRepository.delete(user);
            //userSearchRepository.delete(user);
            this.clearUserCaches(user);
            log.debug("Deleted User: {}", user);
        });
    }

    public void changePassword(String currentClearTextPassword, String newPassword) {
        SecurityUtils.getCurrentUserLogin()
            .flatMap(userRepository::findOneByLogin)
            .ifPresent(user -> {
                String currentEncryptedPassword = user.getPassword();
                if (!passwordEncoder.matches(currentClearTextPassword, currentEncryptedPassword)) {
                    throw new InvalidPasswordException();
                }
                String encryptedPassword = passwordEncoder.encode(newPassword);
                user.setPassword(encryptedPassword);
                this.clearUserCaches(user);
                log.debug("Changed password for User: {}", user);
            });
    }

    @Transactional(readOnly = true)
    public Page<UserDTO> getAllManagedUsers(Pageable pageable) {
        return userRepository.findAllByLoginNot(pageable, Constants.ANONYMOUS_USER).map(UserDTO::new);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthoritiesByLogin(String login) {
        return userRepository.findOneWithAuthoritiesByLogin(login);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthorities(Long id) {
        return userRepository.findOneWithAuthoritiesById(id);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthorities() {
        log.debug(SecurityUtils.getCurrentUserLogin().toString());
        return SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneWithAuthoritiesByLogin);
    }

    /**
     * Not activated users should be automatically deleted after 3 days.
     * <p>
     * This is scheduled to get fired everyday, at 01:00 (am).
     */
    @Scheduled(cron = "0 0 1 * * ?")
    public void removeNotActivatedUsers() {
        List<User> users = userRepository.findAllByActivatedIsFalseAndCreatedDateBefore(Instant.now().minus(3, ChronoUnit.DAYS));
        for (User user : users) {
            log.debug("Deleting not activated user {}", user.getLogin());
            userRepository.delete(user);
            //userSearchRepository.delete(user);
            this.clearUserCaches(user);
        }
    }

    /**
     * @return a list of all the authorities
     */
    public List<String> getAuthorities() {
        return authorityRepository.findAll().stream().map(Authority::getName).collect(Collectors.toList());
    }

    private void clearUserCaches(User user) {
        Objects.requireNonNull(cacheManager.getCache(UserRepository.USERS_BY_LOGIN_CACHE)).evict(user.getLogin());
        Objects.requireNonNull(cacheManager.getCache(UserRepository.USERS_BY_EMAIL_CACHE)).evict(user.getEmail());
    }
}
