package ai.insyght.hireng.gateway.service.mapper;

import ai.insyght.hireng.gateway.domain.Designation;
import ai.insyght.hireng.gateway.service.dto.DesignationDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity CompleteProfile and its DTO CompleteProfileDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DesignationMapper extends EntityMapper<DesignationDTO, Designation>{
}
