package ai.insyght.hireng.gateway.service.dto;

import ai.insyght.hireng.gateway.domain.UserAddress;

import javax.validation.constraints.NotNull;

public class UserAddressDTO {

    private Long id;

    @NotNull
    private Long userId;

    @NotNull
    private String addressLine;

    @NotNull
    private String latitude;

    @NotNull
    private String longitude;

    public UserAddressDTO(){
        // Empty constructor needed for Jackson.
    }

    public UserAddressDTO(UserAddress address) {
        this.id = address.getId();
        this.userId = address.getUserId();
        this.addressLine = address.getAddressLine();
        this.latitude = address.getLatitude();
        this.longitude = address.getLongitude();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getAddressLine() {
        return addressLine;
    }

    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "UserAddressDTO{" +
            "id=" + id +
            ", userId=" + userId +
            ", addressLine='" + addressLine + '\'' +
            ", latitude='" + latitude + '\'' +
            ", longitude='" + longitude + '\'' +
            '}';
    }
}

