package ai.insyght.hireng.gateway.service.dto;

import ai.insyght.hireng.gateway.domain.Discipline;

import javax.validation.constraints.NotNull;
import java.util.Objects;

public class DisciplineDTO {

    private Long id;

    @NotNull
    private String discipline;

    public DisciplineDTO() {}

    public DisciplineDTO(Long id, String discipline) {
        this.id = id;
        this.discipline = discipline;
    }


    public DisciplineDTO(Discipline discipline) {
        this.id = discipline.getId();
        this.discipline = discipline.getDiscipline();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDiscipline() {
        return discipline;
    }

    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DisciplineDTO that = (DisciplineDTO) o;
        return Objects.equals(id, that.id) && Objects.equals(discipline, that.discipline);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, discipline);
    }

}
