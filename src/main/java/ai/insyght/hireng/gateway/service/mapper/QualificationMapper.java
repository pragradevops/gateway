package ai.insyght.hireng.gateway.service.mapper;

import ai.insyght.hireng.gateway.domain.Qualification;
import ai.insyght.hireng.gateway.service.dto.QualificationDTO;
import org.mapstruct.Mapper;


/**
 * Mapper for the entity CompleteProfile and its DTO CompleteProfileDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface QualificationMapper extends EntityMapper<QualificationDTO, Qualification> {
}
