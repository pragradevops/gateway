package ai.insyght.hireng.gateway.service;

import ai.insyght.hireng.gateway.domain.Qualification;
import ai.insyght.hireng.gateway.repository.QualificationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class QualificationService {

    private final Logger log = LoggerFactory.getLogger(QualificationService.class);

    private final QualificationRepository qualificationRepository;

    public QualificationService(QualificationRepository qualificationRepository) {
        this.qualificationRepository = qualificationRepository;
    }

    public List<Qualification> getQualificationList(){
        return qualificationRepository.findAll();
    }

    /**
     * Return List of qualifications using initial
     */
    public List<Qualification> getQualificationsByInitial(String qualificationInitial) {
        return qualificationRepository.findByQualificationContainingIgnoreCase(qualificationInitial);
    }
}
