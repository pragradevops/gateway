package ai.insyght.hireng.gateway.service.dto;

import ai.insyght.hireng.gateway.domain.Company;

import javax.validation.constraints.NotNull;
import java.util.Objects;

public class CompanyDTO {

    private Long id;

    @NotNull
    private String company;

    public CompanyDTO() {}

    public CompanyDTO(Long id, String company) {
        this.id = id;
        this.company = company;
    }

    public CompanyDTO(Company company) {
        this.id = company.getId();
        this.company = company.getCompany();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyDTO that = (CompanyDTO) o;
        return Objects.equals(id, that.id) && Objects.equals(company, that.company);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, company);
    }
}
