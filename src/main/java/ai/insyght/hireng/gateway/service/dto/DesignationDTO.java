package ai.insyght.hireng.gateway.service.dto;

import ai.insyght.hireng.gateway.domain.Designation;

import javax.validation.constraints.NotNull;
import java.util.Objects;

public class DesignationDTO {

    private Long id;

    @NotNull
    private String designation;

    public DesignationDTO() {}

    public DesignationDTO(Long id, String designation) {
        this.id = id;
        this.designation = designation;
    }

    public DesignationDTO(Designation designation) {
        this.id = designation.getId();
        this.designation = designation.getDesignation();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DesignationDTO that = (DesignationDTO) o;
        return Objects.equals(id, that.id) && Objects.equals(designation, that.designation);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, designation);
    }
}
