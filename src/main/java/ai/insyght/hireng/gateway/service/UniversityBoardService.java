package ai.insyght.hireng.gateway.service;

import ai.insyght.hireng.gateway.domain.UniversityBoard;
import ai.insyght.hireng.gateway.repository.UniversityBoardRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class UniversityBoardService {

    private final Logger log = LoggerFactory.getLogger(UniversityBoardService.class);

    private final UniversityBoardRepository universityBoardRepository;

    public UniversityBoardService(UniversityBoardRepository universityBoardRepository) {
        this.universityBoardRepository = universityBoardRepository;
    }

    public List<UniversityBoard> GetUniversityBoardList() {
        return universityBoardRepository.findAll();
    }

    // onKeyDown   10-20  // 1000  2000
    /**
     * Return List of UniversityBoard using initial
     */
    // it will kill mysql db in 10 min
    public List<UniversityBoard> getUniversityBoardByInitial(String universityBoardInitial) {
        return universityBoardRepository.findBySpecializationContainingIgnoreCase(universityBoardInitial);
    }
}
