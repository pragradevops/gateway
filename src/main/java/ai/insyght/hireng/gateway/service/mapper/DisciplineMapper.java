package ai.insyght.hireng.gateway.service.mapper;

import ai.insyght.hireng.gateway.domain.Discipline;
import ai.insyght.hireng.gateway.service.dto.DisciplineDTO;

public interface DisciplineMapper extends EntityMapper<DisciplineDTO, Discipline> {
}
