package ai.insyght.hireng.gateway.service.mapper;

import ai.insyght.hireng.gateway.domain.Specialization;
import ai.insyght.hireng.gateway.service.dto.SpecializationDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity CompleteProfile and its DTO CompleteProfileDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SpecializationMapper  extends EntityMapper<SpecializationDTO, Specialization> {
}
