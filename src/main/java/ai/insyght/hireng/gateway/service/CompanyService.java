package ai.insyght.hireng.gateway.service;

import ai.insyght.hireng.gateway.domain.Company;
import ai.insyght.hireng.gateway.repository.CompanyRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class CompanyService {

    private final Logger log = LoggerFactory.getLogger(CompanyService.class);

    private final CompanyRepository companyRepository;

    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public List<Company> getCompanyList(){
        return companyRepository.findAll();
    }

    /**
     * Return List of companies using initial
     */
    public List<Company> getCompaniesByInitial(String companyInitial) {
        return companyRepository.findByCompanyContainingIgnoreCase(companyInitial);
    }
}
