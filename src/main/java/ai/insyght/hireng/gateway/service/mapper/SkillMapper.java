package ai.insyght.hireng.gateway.service.mapper;


import ai.insyght.hireng.gateway.domain.Skill;
import ai.insyght.hireng.gateway.service.dto.SkillDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity CompleteProfile and its DTO CompleteProfileDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SkillMapper extends EntityMapper<SkillDTO, Skill> {
}
