package ai.insyght.hireng.gateway.service;

import ai.insyght.hireng.gateway.domain.UserAddress;
import ai.insyght.hireng.gateway.repository.UserAddressRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserAddressService {

    private final Logger log = LoggerFactory.getLogger(UserAddressService.class);

    private final UserAddressRepository userAddressRepository;

    public UserAddressService(UserAddressRepository userAddressRepository){
        this.userAddressRepository = userAddressRepository;
    }

    public void registerAddress(Long userId, String addressLine, String lat, String lng) {
        log.info("User ID: "+ userId);
        log.info("Address ID: "+ addressLine);
        log.info("lat: "+ lat);
        log.info("lng: "+ userId);
        UserAddress address = new UserAddress();
        address.setUserId(userId);
        address.setAddressLine(addressLine);
        address.setLatitude(lat);
        address.setLongitude(lng);
        userAddressRepository.save(address);
    }
}
