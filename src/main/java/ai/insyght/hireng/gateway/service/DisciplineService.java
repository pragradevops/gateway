package ai.insyght.hireng.gateway.service;

import ai.insyght.hireng.gateway.domain.Discipline;
import ai.insyght.hireng.gateway.repository.DisciplineRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class DisciplineService {

    private final Logger log = LoggerFactory.getLogger(DisciplineService.class);

    private final DisciplineRepository disciplineRepository;

    public DisciplineService(DisciplineRepository disciplineRepository) {
        this.disciplineRepository = disciplineRepository;
    }

    public List<Discipline> getAllDisciplineList() {
        return disciplineRepository.findAll();
    }

    /**
     * Return List of Designation using initial
     */
    public List<Discipline> getDisciplinesByInitial(String disciplineInitial) {
        return disciplineRepository.findByDisciplineContainingIgnoreCase(disciplineInitial);
    }
}
