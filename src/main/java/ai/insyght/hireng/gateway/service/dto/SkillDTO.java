package ai.insyght.hireng.gateway.service.dto;

import ai.insyght.hireng.gateway.domain.Skill;

import javax.validation.constraints.NotNull;
import java.util.Objects;

public class SkillDTO {

    private Long id;

    @NotNull
    private String skill;

    public SkillDTO() {}

    public SkillDTO(Long id, String skill) {
        this.id = id;
        this.skill = skill;
    }

    public SkillDTO(Skill skill) {
        this.id = skill.getId();
        this.skill = skill.getSkill();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SkillDTO skillDTO = (SkillDTO) o;
        return Objects.equals(id, skillDTO.id) && Objects.equals(skill, skillDTO.skill);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, skill);
    }
}
