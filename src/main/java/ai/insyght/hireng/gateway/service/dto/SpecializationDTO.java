package ai.insyght.hireng.gateway.service.dto;

import ai.insyght.hireng.gateway.domain.Specialization;

import javax.validation.constraints.NotNull;
import java.util.Objects;

public class SpecializationDTO {

    private Long id;

    @NotNull
    private String specialization;

    public SpecializationDTO() {}

    public SpecializationDTO(Long id, String specialization) {
        this.id = id;
        this.specialization = specialization;
    }

    public SpecializationDTO(Specialization specialization) {
        this.id = specialization.getId();
        this.specialization = specialization.getSpecialization();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SpecializationDTO that = (SpecializationDTO) o;
        return Objects.equals(id, that.id) && Objects.equals(specialization, that.specialization);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, specialization);
    }
}
