package ai.insyght.hireng.gateway.service;

import ai.insyght.hireng.gateway.domain.Designation;
import ai.insyght.hireng.gateway.repository.DesignationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class DesignationService {

    private final Logger log = LoggerFactory.getLogger(DesignationService.class);

    private final DesignationRepository designationRepository;

    public DesignationService(DesignationRepository designationRepository) {
        this.designationRepository = designationRepository;
    }

    public List<Designation> getDesignationList(){
        return designationRepository.findAll();
    }

    /**
     * Return List of Designation using initial
     */
    public List<Designation> getDesignationsByInitial(String designationInitial) {
        return designationRepository.findByDesignationContainingIgnoreCase(designationInitial);
    }
}
