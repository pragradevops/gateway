package ai.insyght.hireng.gateway.service;

import ai.insyght.hireng.gateway.domain.Location;
import ai.insyght.hireng.gateway.repository.LocationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class LocationService {

    private final Logger log = LoggerFactory.getLogger(LocationService.class);

    private final LocationRepository locationRepository;

    public LocationService(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    /**
     * Return List of concat string of city and country
     */
    public List<String> getCitiesByInitial(String cityInitial) {
        List<String> completeLocation = new ArrayList<>();
        List<Location> locations = locationRepository.
            findByCityNameContainingIgnoreCase(cityInitial);
        if ( locations != null || !locations.isEmpty()){
            locations.forEach(location -> {completeLocation.add(
                location.getCity().concat("-").concat(location.getCountry()));});
        }
        return completeLocation;
    }
}
