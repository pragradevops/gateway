package ai.insyght.hireng.gateway.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name= "table_location")
public class Location {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "city")
    private String city;

    @Column(name = "country")
    private String country;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Location location = (Location) o;
        return Objects.equals(id, location.id) && Objects.equals(city, location.city) && Objects.equals(country, location.country);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, city, country);
    }

    @Override
    public String toString() {
        return "Location{" + "id=" + id + ", city='" + city + '\'' + ", country='" + country + '\'' + '}';
    }
}
