package ai.insyght.hireng.gateway.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;

@Entity
@Table(name = "table_qualification")
public class Qualification {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 250)
    @Column(name = "qualification")
    private String qualification;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Qualification that = (Qualification) o;
        return Objects.equals(id, that.id) && Objects.equals(qualification, that.qualification);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, qualification);
    }

    @Override
    public String toString() {
        return "Qualification{" + "id=" + id + ", qualification='" + qualification + '\'' + '}';
    }
}
