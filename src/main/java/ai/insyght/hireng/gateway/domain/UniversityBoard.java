package ai.insyght.hireng.gateway.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;

@Entity
@Table (name = "table_university_board")
public class UniversityBoard {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 250)
    @Column(name = "university_board_name")
    private String universityBoardName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUniversityBoardName() {
        return universityBoardName;
    }

    public void setUniversityBoardName(String universityBoardName) {
        universityBoardName = universityBoardName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UniversityBoard that = (UniversityBoard) o;
        return Objects.equals(id, that.id) && Objects.equals(universityBoardName, that.universityBoardName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, universityBoardName);
    }

    @Override
    public String toString() {
        return "UniversityBoard{" + "id=" + id + ", UniversityBoardName='" + universityBoardName + '\'' + '}';
    }
}
