package ai.insyght.hireng.gateway.domain.enumeration;

public enum GenderEnum {
    MALE, FEMALE, NOT_DEFINED
}
