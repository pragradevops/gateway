export interface IProfile {
    id?: number;
    userId?: number;
    profileName?: string;
    profileScore?: number;
}

export class Profile implements IProfile {
    constructor(public id?: number, public userId?: number, public profileName?: string, public profileScore?: number) {}
}
