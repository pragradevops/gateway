import { NgModule } from '@angular/core';

import { JhiAlertComponent, JhiAlertErrorComponent, NghireSharedLibsModule as GatewaySharedLibsModule } from './';
import { JhiSocialComponent } from 'app/shared/social/social.component';

@NgModule({
    imports: [GatewaySharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent, JhiSocialComponent],
    exports: [GatewaySharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent, JhiSocialComponent]
})
export class GatewaySharedCommonModule {}
