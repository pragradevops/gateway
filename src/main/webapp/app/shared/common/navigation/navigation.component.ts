import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NavigationEnd, Router } from '@angular/router';
import { ProfileService } from 'app/layouts/profiles/profile.service';
import { LoginModalService, LoginService, Principal } from 'app/core';
import { RegisterModalService } from 'app/account';
import { VERSION } from 'app/app.constants';

@Component({
    selector: 'jhi-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.scss']
})
export class CommonNavigationComponent implements OnInit {
    inProduction: boolean;
    isNavbarCollapsed: boolean;
    languages: any[];
    swaggerEnabled: boolean;
    modalRef: NgbModalRef;
    version: string;

    constructor(
        private loginService: LoginService,
        private principal: Principal,
        private loginModalService: LoginModalService,
        private profileService: ProfileService,
        private registerModalService: RegisterModalService,
        private router: Router
    ) {
        this.version = VERSION ? 'v' + VERSION : '';
        this.isNavbarCollapsed = true;
    }

    ngOnInit() {
        // logic to move at the top
        this.router.events.subscribe(evt => {
            if (!(evt instanceof NavigationEnd)) {
                return;
            }
        });
    }

    collapseNavbar() {
        this.isNavbarCollapsed = true;
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }
    logout() {
        this.collapseNavbar();
        this.loginService.logout();
        this.router.navigate(['']);
    }

    toggleNavbar() {
        this.isNavbarCollapsed = !this.isNavbarCollapsed;
    }

    getImageUrl() {
        return this.isAuthenticated() ? this.principal.getImageUrl() : null;
    }
    register() {
        this.modalRef = this.registerModalService.open();
    }
    login() {
        // this.router.navigate(['/', { outlets: { popup: ['talent-basic-personal-detail'] } }]);
        this.modalRef = this.loginModalService.open();
    }
}
