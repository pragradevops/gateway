import { CommonNavFooterModule } from './common.module';

describe('CommonModule', () => {
    let commonModule: CommonNavFooterModule;

    beforeEach(() => {
        commonModule = new CommonNavFooterModule();
    });

    it('should create an instance', () => {
        expect(commonModule).toBeTruthy();
    });
});
