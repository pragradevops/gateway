import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ContactUsComponent } from 'app/user/talent/modal/home/contact-us/contact-us.component';

// HireNg services
import { ModalService } from 'app/user/talent/service/modal.service';

@Component({
    selector: 'jhi-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.scss']
})
export class CommonFooterComponent implements OnInit {
    modalRef: NgbModalRef;
    sectionScroll = null;

    constructor(private modalService: ModalService, private router: Router) {}

    ngOnInit() {
        // logic to move at the top
        this.router.events.subscribe(evt => {
            if (!(evt instanceof NavigationEnd)) {
                return;
            }
            window.scroll(0, 0);
            this.doScroll();
            this.sectionScroll = null;
        });

        // this is for click on button and go to top by scroll
        document.getElementById('top-btn').addEventListener('click', function() {
            window.scroll(0, 0);
        });

        // this event is for scroll from top then hide & show (go to top button)
        window.onscroll = function() {
            backTop();
        };
        function backTop() {
            if (document.body.scrollTop > window.innerHeight / 2) {
                document.getElementById('top-btn').classList.add('bt-top');
            } else {
                document.getElementById('top-btn').classList.remove('bt-top');
            }
        }
        // backTop function //
    }

    openContactUs() {
        this.modalRef = this.modalService.openXL(ContactUsComponent);
    }

    // section routing
    fragmentrouting(dst) {
        // property, sectionScroll, to specify the specific section of the fragment
        this.sectionScroll = dst;

        if (
            (this.router.url === '/we' && this.sectionScroll === 'investors-relation') ||
            (this.router.url === '/' && this.sectionScroll === 'our-accociates')
        ) {
            this.doScroll();
            this.sectionScroll = null;
        }
    }

    doScroll() {
        if (!this.sectionScroll) {
            return;
        }
        try {
            const elements = document.getElementById(this.sectionScroll);
            console.log(elements);
            elements.scrollIntoView();

            const scrolledY = window.scrollY;

            if (scrolledY) {
                window.scroll(0, scrolledY - 76);
            }
        } finally {
            this.sectionScroll = null;
        }
    }
}
