import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedMaterialModule } from '../shared-material.module';

// components
import { CommonNavigationComponent } from './navigation/navigation.component';
import { CommonFooterComponent } from './footer/footer.component';

@NgModule({
    imports: [CommonModule, RouterModule, SharedMaterialModule],
    declarations: [CommonNavigationComponent, CommonFooterComponent],
    exports: [CommonNavigationComponent, CommonFooterComponent]
})
export class CommonNavFooterModule {}
