import { SERVER_API_URL } from 'app/app.constants';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { createRequestOption } from 'app/shared/model/request-util';

export class HttpService<T> {
    // private resourceUrl = SERVER_API_URL + 'api/users';

    constructor(protected http: HttpClient, protected resourceUrl: string = SERVER_API_URL, protected endpoint: string) {}

    create(entity: T): Observable<any> {
        return this.http.post<T>(this.resourceUrl + this.endpoint, this.convert(entity));
    }
    update(entity: T): Observable<any> {
        return this.http.put<T>(this.resourceUrl + this.endpoint, this.convert(entity));
    }
    find(id: number): Observable<any> {
        return this.http.get<T>(`${this.resourceUrl + this.endpoint}/${id}`);
    }
    delete(id: number): Observable<any> {
        return this.http.delete<T>(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + this.endpoint, { params: options });
    }

    /**
     * Convert a Profile to a JSON which can be sent to the server.
     */
    private convert(entity: T): T {
        return Object.assign({}, entity);
    }
    /**
     * Convert a returned JSON object to Profile.
     */
    private convertItemFromServer(json: any): T {
        const entity: T = JSON.parse(json);
        return entity;
    }
}
