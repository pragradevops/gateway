import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SERVER_API_URL } from '../../app.constants';
import { User } from './user.model';
import { HttpClient } from '@angular/common/http';
import { HttpService } from 'app/shared/httpservice/http.service';

@Injectable()
export class UserService extends HttpService<User> {
    constructor(http: HttpClient) {
        super(http, SERVER_API_URL, 'api/users');
    }

    authorities(): Observable<string[]> {
        return this.http.get(SERVER_API_URL + 'api/users/authorities').pipe(
            map(res => {
                return <string[]>res;
            })
        );
    }
}
