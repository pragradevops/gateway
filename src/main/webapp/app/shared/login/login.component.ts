import { AfterViewInit, Component, ElementRef, forwardRef, Inject, Renderer } from '@angular/core';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { RegisterModalService } from '../../account/register/register-modal.service';
import { Router } from '@angular/router';
import { JhiEventManager } from 'ng-jhipster';

import { LoginService } from 'app/core/login/login.service';
import { StateStorageService } from 'app/core/auth/state-storage.service';
import { PasswordResetInitService } from 'app/account';
import { Principal, Account } from 'app/core';

@Component({
    selector: 'jhi-login-modal',
    templateUrl: './login.component.html',
    styleUrls: ['./login.scss']
})
export class JhiLoginModalComponent {
    authenticationError: boolean;
    password: string;
    rememberMe: boolean;
    username: string;
    credentials: any;
    modalRef: NgbModalRef;
    account: Account;
    hide = true;

    constructor(
        @Inject(forwardRef(() => RegisterModalService))
        private registerModalService: RegisterModalService,
        @Inject(forwardRef(() => PasswordResetInitService))
        private passwordResetInitService: PasswordResetInitService,
        private eventManager: JhiEventManager,
        private loginService: LoginService,
        private stateStorageService: StateStorageService,
        private router: Router,
        private renderer: Renderer,
        private elementRef: ElementRef,
        public activeModal: NgbActiveModal,
        private principal: Principal
    ) {
        this.credentials = {};
    }

    cancel() {
        this.credentials = {
            username: null,
            password: null,
            rememberMe: true
        };
        this.authenticationError = false;
        this.activeModal.dismiss('cancel');
    }

    login() {
        this.loginService
            .login({
                username: this.username,
                password: this.password,
                rememberMe: this.rememberMe
            })
            .then(() => {
                this.authenticationError = false;
                this.activeModal.dismiss('login success');
                if (this.router.url === '/register' || /^\/activate\//.test(this.router.url) || /^\/reset\//.test(this.router.url)) {
                    this.router.navigate(['']);
                } else {
                    this.principal.identity().then(account => {
                        this.account = account;
                        if (this.account.authorities[0] === 'ROLE_EMPLOYEE') {
                            this.router.navigate(['talent/dashboard']);
                        } else if (this.account.authorities[0] === 'ROLE_EMPLOYER') {
                            this.router.navigate(['employer/dashboard']);
                        } else if (this.account.authorities[0] === 'ROLE_ADMIN') {
                            this.router.navigate(['/admin/home']);
                        } else if (this.account.authorities[0] === 'ROLE_USER') {
                            this.router.navigate(['/admin/home']);
                        } else {
                            this.router.navigate(['']);
                        }
                    });
                    // this.principal.hasAnyAuthority(['ROLE_ADMIN']).then(
                    //     auth => {
                    //         if (auth) {
                    //             this.router.navigate(['admin/home']);
                    //         } else {
                    //             this.router.navigate(['']);
                    //         }
                    //     },
                    //     error => {
                    //         // push notification
                    //         console.log('User has no autorites');
                    //     }
                    // );

                    // if (this.principal.hasAnyAuthority(['ROLE_EMPLOYEE']).then(data => data)) {
                    //     this.principal.hasAnyAuthority(['ROLE_EMPLOYEE']).then(
                    //         auth => {
                    //             if (auth) {

                    //                 // for mean time, as we don't have basic-profile page
                    //                 this.router.navigate(['talent/dashboard']);

                    //                 // if (!this.account.firstName) {
                    //                 //     this.router.navigate(['talent/basic-details']);
                    //                 // } else {
                    //                 //     this.router.navigate(['talent/dashboard']);
                    //                 // }
                    //             } else {
                    //                 this.router.navigate(['']);
                    //             }
                    //         },
                    //         error => {
                    //             console.log('User has no autorites');
                    //         }
                    //     );
                    // }
                    // else if (this.principal.hasAnyAuthority(['ROLE_EMPLOYER']).then(data => data)) {
                    //     this.principal.hasAnyAuthority(['ROLE_EMPLOYER']).then(
                    //         auth => {
                    //             if (auth) {

                    //                 // for mean time, as we don't have basic-profile page
                    //                 this.router.navigate(['employer/dashboard']);

                    //                 // if (!this.account.firstName) {
                    //                 //     this.router.navigate(['employer/basic-profile']);
                    //                 // } else {
                    //                 //     this.router.navigate(['employer/dashboard']);
                    //                 // }
                    //             } else {
                    //                 this.router.navigate(['']);
                    //             }
                    //         },
                    //         error => {
                    //             // push notification
                    //             console.log('User has no autorites');
                    //         }
                    //     );
                    // }
                }

                // this.principal.identity().then((account) => {
                //     this.account = account;
                // console.log("this.account....................");
                // if (this.account.authorities[0] === 'ROLE_EMPLOYEE' && this.account.firstName != null) {
                //     this.router.navigate(['talent/dashboard']);
                // } else if (this.account.authorities[0] === 'ROLE_EMPLOYEE' && this.account.firstName == null) {
                //     this.router.navigate(['talent/basic-details']);
                // } else if (this.account.authorities[0] === 'ROLE_EMPLOYER' && this.account.firstName != null) {
                //     this.router.navigate(['employer/dashboard']);
                // } else if (this.account.authorities[0] === 'ROLE_EMPLOYER' && this.account.firstName == null) {
                //     this.router.navigate(['employer/basic-profile']);
                // } else if (this.account.authorities[0] === 'ROLE_ADMIN') {
                //     this.router.navigate(['/admin/home']);
                // } else if (this.account.authorities[0] === 'ROLE_USER') {
                //     this.router.navigate(['/admin/home']);
                // } else {
                // this.router.navigate(['']);
                // }
                // });
                // console.log('user');

                this.eventManager.broadcast({
                    name: 'authenticationSuccess',
                    content: 'Sending Authentication Success'
                });

                // // previousState was set in the authExpiredInterceptor before being redirected to login modal.
                // // since login is succesful, go to stored previousState and clear previousState
                const redirect = this.stateStorageService.getUrl();
                if (redirect) {
                    this.stateStorageService.storeUrl(null);
                    this.router.navigate([redirect]);
                }
            })
            .catch(() => {
                this.authenticationError = true;
            });
    }

    openRegister() {
        this.activeModal.dismiss('to state register');
        this.modalRef = this.registerModalService.open();
    }

    requestResetPassword() {
        this.activeModal.dismiss('to state requestReset');
        this.modalRef = this.passwordResetInitService.open();
    }
}
