import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';

import { NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';

import { NgbDateMomentAdapter } from './util/datepicker-adapter';
import { GatewaySharedCommonModule, HasAnyAuthorityDirective, JhiLoginModalComponent, NghireSharedLibsModule } from './';
import { SharedMaterialModule } from '../shared/shared-material.module';

@NgModule({
    imports: [NghireSharedLibsModule, GatewaySharedCommonModule, SharedMaterialModule],
    declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
    providers: [{ provide: NgbDateAdapter, useClass: NgbDateMomentAdapter }],
    entryComponents: [JhiLoginModalComponent],
    exports: [NghireSharedLibsModule, JhiLoginModalComponent, HasAnyAuthorityDirective],

    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NghireSharedModule {}
