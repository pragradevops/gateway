import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

// Module for footer and Navigation bar
import { CommonNavFooterModule } from 'app/shared/common/common.module';

// We page components
import {
    WeComponent,
    AboutUsComponent,
    AdvisorsComponent,
    ChiefProponentComponent,
    CoreTeamComponent,
    InvestorsComponent,
    SpecialThanksComponent,
    VideoComponent
} from './';

// HireNg Modules
import { NghireSharedModule } from 'app/shared';

// We route variable
import { We_ROUTE } from './we.route';

@NgModule({
    imports: [RouterModule.forChild([We_ROUTE]), NghireSharedModule, CommonNavFooterModule],
    declarations: [
        WeComponent,
        VideoComponent,
        AboutUsComponent,
        CoreTeamComponent,
        AdvisorsComponent,
        InvestorsComponent,
        SpecialThanksComponent,
        ChiefProponentComponent
    ],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NghireWeModule {}
