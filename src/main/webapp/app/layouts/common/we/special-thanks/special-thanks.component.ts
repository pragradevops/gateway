import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { RegisterModalService } from 'app/account';

@Component({
    selector: 'jhi-special-thanks',
    templateUrl: './special-thanks.component.html',
    styleUrls: ['./special-thanks.scss']
})
export class SpecialThanksComponent implements OnInit {
    modalRef: NgbModalRef;

    constructor(private registerModalService: RegisterModalService) {}

    ngOnInit() {}
    register() {
        this.modalRef = this.registerModalService.open();
    }
}
