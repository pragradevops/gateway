import { Route } from '@angular/router';

import { WeComponent } from './we.component';
import { UserRouteSessionService } from 'app/core/auth/user-route-session.service';

export const We_ROUTE: Route = {
    path: 'we',
    component: WeComponent,
    data: {
        authorities: [],
        pageTitle: 'Aleeph Advisors | We'
    },
    canActivate: [UserRouteSessionService]
};
