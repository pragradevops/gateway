// We pages components
export * from './we.component';
export * from './about-us/about-us.component';
export * from './advisors/advisors.component';
export * from './chief-proponent/chief-proponent.component';
export * from './core-team/core-team.component';
export * from './investors/investors.component';
export * from './special-thanks/special-thanks.component';
export * from './video/video.component';

// We route variable
export * from './we.route';
