import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// Module for footer and Navigation bar
import { CommonNavFooterModule } from 'app/shared/common/common.module';

// common footer pages components
import {
    // main footer page
    PageFooterComponent,
    //
    AdsChoicesComponent,
    AgreementComponent,
    CareerComponent,
    CareerDetailComponent,
    CookiesComponent,
    CopyRightComponent,
    FaqsComponent,
    PrivacyComponent,
    SupportComponent,
    SupportDetailComponent,
    TermsComponent
} from './';

// HireNg Modules
import { SharedMaterialModule } from 'app/shared/shared-material.module';

// common footer route variable
import { FOOTER_PAGES_ROUTE } from './pages.route';

@NgModule({
    imports: [CommonModule, SharedMaterialModule, RouterModule.forChild(FOOTER_PAGES_ROUTE), CommonNavFooterModule],
    declarations: [
        PageFooterComponent,
        AdsChoicesComponent,
        PrivacyComponent,
        CookiesComponent,
        TermsComponent,
        FaqsComponent,
        CareerDetailComponent,
        CopyRightComponent,
        AgreementComponent,
        SupportComponent,
        CareerComponent,
        SupportDetailComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PageFooterModule {}
