import { Routes } from '@angular/router';

import {
    PageFooterComponent,
    // sub-components
    AdsChoicesComponent,
    AgreementComponent,
    CareerComponent,
    CareerDetailComponent,
    CookiesComponent,
    CopyRightComponent,
    FaqsComponent,
    PrivacyComponent,
    SupportDetailComponent,
    TermsComponent
} from './';

export const FOOTER_PAGES_ROUTE: Routes = [
    {
        path: 'page',
        component: PageFooterComponent,
        canActivate: [],
        children: [
            {
                path: 'ads-choices',
                component: AdsChoicesComponent,
                data: {
                    pageTitle: 'Aleeph Advisors | Ads Choices'
                }
            },
            {
                path: 'user-agreement',
                component: AgreementComponent,
                data: {
                    pageTitle: 'Aleeph Advisors | User Agreement'
                }
            },
            {
                path: 'career-detail',
                component: CareerDetailComponent,
                data: {
                    pageTitle: 'Aleeph Advisors | Career Detail'
                }
            },
            {
                path: 'faqs',
                component: FaqsComponent,
                data: {
                    pageTitle: 'Aleeph Advisors | FAQs'
                }
            },
            {
                path: 'privacy-policy',
                component: PrivacyComponent,
                data: {
                    pageTitle: 'Aleeph Advisors | Privacy Policy'
                }
            },
            {
                path: 'cookie-policy',
                component: CookiesComponent,
                data: {
                    pageTitle: 'Aleeph Advisors | Cookie Policy'
                }
            },
            {
                path: 'copyright-policy',
                component: CopyRightComponent,
                data: {
                    pageTitle: 'Aleeph Advisors | Copyright Policy'
                }
            },
            {
                path: 'terms-conditions',
                component: TermsComponent,
                data: {
                    pageTitle: 'Aleeph Advisors | Terms & Conditions'
                }
            },
            {
                path: 'careers',
                component: CareerComponent,
                data: {
                    pageTitle: 'Aleeph Advisors | Careers'
                }
            },
            {
                path: 'support',
                component: SupportDetailComponent,
                data: {
                    pageTitle: 'Aleeph Advisors | Support'
                }
            }
        ]
    }
];
