import { Component, OnInit } from '@angular/core';

declare let $: any;
@Component({
    selector: 'jhi-support-detail',
    templateUrl: './support-detail.component.html',
    styleUrls: ['./support.scss']
})
export class SupportDetailComponent implements OnInit {
    constructor() {}

    ngOnInit() {
        $('.left p a.fav').click(function() {
            $(this)
                .parent('p')
                .addClass('active');
            $(this)
                .parent('p')
                .siblings('p')
                .removeClass('active');
            $('.right .collapse-data').addClass('hide active');
            $('.right .what-aleeph').removeClass('hide active');
        });
        $('.left p a.under').click(function() {
            $(this)
                .parent('p')
                .addClass('active');
            $(this)
                .parent('p')
                .siblings('p')
                .removeClass('active');
            $('.right .collapse-data').addClass('hide active');
            $('.right .Understanding').removeClass('hide active');
        });
        $('.left p a.pre').click(function() {
            $(this)
                .parent('p')
                .addClass('active');
            $(this)
                .parent('p')
                .siblings('p')
                .removeClass('active');
            $('.right .collapse-data').addClass('hide active');
            $('.right .premium').removeClass('hide active');
        });
        $('.left p a.sub').click(function() {
            $(this)
                .parent('p')
                .addClass('active');
            $(this)
                .parent('p')
                .siblings('p')
                .removeClass('active');
            $('.right .collapse-data').addClass('hide active');
            $('.right .submit').removeClass('hide active');
        });
        $('.left p a.match-candi').click(function() {
            $(this)
                .parent('p')
                .addClass('active');
            $(this)
                .parent('p')
                .siblings('p')
                .removeClass('active');
            $('.right .collapse-data').addClass('hide active');
            $('.right .matching-candidate').removeClass('hide active');
        });
        $('.left p a.email-candi').click(function() {
            $(this)
                .parent('p')
                .addClass('active');
            $(this)
                .parent('p')
                .siblings('p')
                .removeClass('active');
            $('.right .collapse-data').addClass('hide active');
            $('.right .email-candidate').removeClass('hide active');
        });
    }
}
