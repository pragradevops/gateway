// common footer page components
export * from './page-footer.component';
export * from './ads-choices/ads-choices.component';
export * from './agreement/agreement.component';
export * from './career/career.component';
export * from './career-detail/career-detail.component';
export * from './cookies/cookies.component';
export * from './copy-right/copy-right.component';
export * from './faqs/faqs.component';
export * from './privacy/privacy.component';
export * from './support/support.component';
export * from './support-detail/support-detail.component';
export * from './terms/terms.component';

// common footer page variable
export * from './pages.route';
