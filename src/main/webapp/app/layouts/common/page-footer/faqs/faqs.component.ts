import { Component, OnInit } from '@angular/core';

declare let $: any;
@Component({
    selector: 'jhi-faqs',
    templateUrl: './faqs.component.html',
    styleUrls: ['../page-footer.scss']
})
export class FaqsComponent implements OnInit {
    constructor() {}

    ngOnInit() {
        $('p.heading').click(function() {
            if (
                $(this)
                    .children('i.fa-plus')
                    .hasClass('fa-plus')
            ) {
                $(this)
                    .siblings('div')
                    .show();
                $(this)
                    .children('i.fa-plus')
                    .removeClass('fa-plus')
                    .addClass('fa-minus');
            } else if (
                $(this)
                    .children('i.fa-minus')
                    .hasClass('fa-minus')
            ) {
                $(this)
                    .siblings('div')
                    .hide();
                $(this)
                    .children('i.fa-minus')
                    .removeClass('fa-minus')
                    .addClass('fa-plus');
            }
        });
    }
}
