import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

// Module for footer and Navigation bar
import { CommonNavFooterModule } from 'app/shared/common/common.module';

// Devise Page Components
import {
    DeviseComponent,
    BannerComponent,
    TechnologyComponent,
    FeatureComponent,
    ProblemSolutionsComponent,
    DesignDevelopDeployComponent,
    ProductVisionComponent,
    CaseStudyComponent,
    SolutionComponent
} from './';

// HireNg Modules
import { NghireSharedModule } from 'app/shared';

// Route variable
import { DEVISE_ROUTE } from './';

@NgModule({
    imports: [RouterModule.forChild([DEVISE_ROUTE]), NghireSharedModule, CommonNavFooterModule],
    declarations: [
        DeviseComponent,
        BannerComponent,
        TechnologyComponent,
        FeatureComponent,
        ProblemSolutionsComponent,
        DesignDevelopDeployComponent,
        ProductVisionComponent,
        CaseStudyComponent,
        SolutionComponent
    ],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NghireDeviseModule {}
