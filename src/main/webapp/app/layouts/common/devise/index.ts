// Devise page components
export * from './devise.component';
export * from './banner/banner.component';
export * from './case-study/case-study.component';
export * from './design-develop-deploy/design-develop-deploy.component';
export * from './feature/feature.component';
export * from './problem-solutions/problem-solutions.component';
export * from './product-vision/product-vision.component';
export * from './solution/solution.component';
export * from './technology/technology.component';

// Devise route variable
export * from './devise.route';
