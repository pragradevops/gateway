import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { RegisterModalService } from 'app/account';

@Component({
    selector: 'jhi-problem-solutions',
    templateUrl: './problem-solutions.component.html',
    styleUrls: ['./problem-solution.scss']
})
export class ProblemSolutionsComponent implements OnInit {
    modalRef: NgbModalRef;

    constructor(private registerModalService: RegisterModalService) {}

    ngOnInit() {}
    register() {
        this.modalRef = this.registerModalService.open();
    }
}
