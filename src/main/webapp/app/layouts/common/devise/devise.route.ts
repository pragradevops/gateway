import { Route } from '@angular/router';

import { DeviseComponent } from './devise.component';
import { UserRouteSessionService } from 'app/core/auth/user-route-session.service';

export const DEVISE_ROUTE: Route = {
    path: 'devise',
    component: DeviseComponent,
    data: {
        authorities: [],
        pageTitle: 'Aleeph Advisors | Devise'
    },
    canActivate: [UserRouteSessionService]
};
