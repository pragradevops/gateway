import { Component, OnInit } from '@angular/core';
import 'jquery';
declare let $: any;
@Component({
    selector: 'jhi-live-streaming',
    templateUrl: './live-streaming.component.html',
    styleUrls: ['./live-streaming.scss']
})
export class LiveStreamingComponent implements OnInit {
    constructor() {}

    ngOnInit() {
        $('.owl-carousel').owlCarousel({
            loop: true,
            // autoplay:4000,
            margin: 10,
            nav: false,
            dots: false,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 3
                },
                1000: {
                    items: 5
                }
            }
        });
        const owl = $('.owl-carousel');
        owl.owlCarousel();
        // Go to the next item
        $('.next-cont').click(function() {
            owl.trigger('next.owl.carousel');
        });
        // Go to the previous item
        $('.prev-cont').click(function() {
            // With optional speed parameter
            // Parameters has to be in square bracket '[]'
            owl.trigger('prev.owl.carousel', [300]);
        });
    }
}
