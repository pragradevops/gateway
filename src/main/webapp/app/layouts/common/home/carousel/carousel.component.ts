import { Component, OnInit } from '@angular/core';
import { RegisterModalService } from 'app/account';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'jhi-carousel',
    templateUrl: './carousel.component.html',
    styleUrls: ['./carousel.scss']
})
export class CarouselComponent implements OnInit {
    images: Array<string>;
    modalRef: NgbModalRef;
    constructor(private registerModalService: RegisterModalService) {}

    ngOnInit() {
        this.images = [
            require('../../../../../assets/img/0.png'),
            require('../../../../../assets/img/1.png'),
            require('../../../../../assets/img/2.png'),
            require('../../../../../assets/img/m1.png'),
            require('../../../../../assets/img/m2.png'),
            require('../../../../../assets/img/m3.png')
        ];
    }
    register() {
        this.modalRef = this.registerModalService.open();
    }
}
