import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'jhi-we',
    templateUrl: './we.component.html',
    styleUrls: ['./we.scss']
})
export class WeComponent implements OnInit {
    constructor(private router: Router) {}

    ngOnInit() {}

    moveToWe() {
        window.scroll(0, 0);
    }
}
