import { Route } from '@angular/router';

import { HomeComponent } from './';
import { UserRouteSessionService } from 'app/core/auth/user-route-session.service';

export const HOME_ROUTE: Route = {
    path: '',
    component: HomeComponent,
    data: {
        pageTitle: 'Insyght AI | Hiring Made Easy',
        authorities: ['']
    },
    canActivate: [UserRouteSessionService]
};
