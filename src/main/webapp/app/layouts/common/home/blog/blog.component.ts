import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'jhi-blog',
    templateUrl: './blog.component.html',
    styleUrls: ['./blog.scss']
})
export class BlogComponent implements OnInit {
    blogs: any[];
    constructor() {}

    ngOnInit() {
        (function() {
            const ficon = document.querySelectorAll('i.fa-heart.float-right');
            let i;
            for (i = 0; i < ficon.length; i++) {
                ficon[i].addEventListener('click', function() {
                    this.classList.toggle('active');
                });
            }
        })();
        this.blogs = [
            {
                img: require('../../../../../assets/img/img1.png'),
                title: 'InsyghtAI solution- making things simple',
                // tslint:disable-next-line:max-line-length
                desc:
                    'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.'
            },
            {
                img: require('../../../../../assets/img/img2.png'),
                title: 'InsyghtAI solution- making things simple',
                // tslint:disable-next-line:max-line-length
                desc:
                    'Searching and hiring best quality of candidates have' +
                    ' become a difficult task nowadays. Today’s evolving and ' +
                    'competitive environment have made a significant pain area for ' +
                    'companies looking to grow their team. ' +
                    'But our system is an employment service provider that will ' +
                    'connect both job seekers and companies...'
            },
            {
                img: require('../../../../../assets/img/img3.png'),
                title: 'Fantastic Ways for making job posts out of box',
                // tslint:disable-next-line:max-line-length
                desc:
                    'When posting openings, recruiters should incorporate their ' +
                    'future plans for the position in the depiction. ' +
                    'Then you will discover pulling in more qualified candidates on ' +
                    'the off chance that you appear that you’re contributing in ' +
                    'extra assets and have particular targets to hit the...'
            },
            {
                img: require('../../../../../assets/img/img4.png'),
                title: 'InsyghtAI Data Platform',
                // tslint:disable-next-line:max-line-length
                desc:
                    'We have adopted a service oriented architecture which is basically a ' +
                    'collection of services that communicate with each other for building ' +
                    'thousands of micro-services that power specific user experiences like job search, ' +
                    'job listings, background check, social media logins, text-to-speech...'
            },
            {
                img: require('../../../../../assets/img/img5.png'),
                title: 'How to make video resume impressive',
                // tslint:disable-next-line:max-line-length
                desc:
                    'A video resume is a good way to show off your personality and highlight skills.' +
                    'So, keep it short and creative. To make it more attractive you should follow some tips.' +
                    'Firstly, be professional and dress as you would for an interview and maintain a professional demeanor...'
            }
        ];
    }
    movetoTop() {
        window.scroll(0, 0);
    }
}
