import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ContactUsComponent } from 'app/user/talent/modal/home/contact-us/contact-us.component';
// service
import { ModalService } from 'app/user/talent/service/modal.service';

@Component({
    selector: 'jhi-map-query',
    templateUrl: './map-query.component.html',
    styleUrls: ['./map-query.scss']
})
export class MapQueryComponent implements OnInit {
    @ViewChild('map') gmapElement: any;
    map: google.maps.Map;

    modalRef: NgbModalRef;
    isEmp = false;
    featuresopt;
    constructor(private modalService: ModalService) {}

    ngOnInit() {
        const mapProp = {
            center: new google.maps.LatLng(28.599608, 77.3825164),
            zoom: 1,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);

        this.featuresopt = [
            {
                featureType: 'all',
                elementType: 'all',
                stylers: [
                    {
                        visibility: 'on'
                    },
                    {
                        hue: '#ff0000'
                    }
                ]
            },
            {
                featureType: 'all',
                elementType: 'geometry',
                stylers: [
                    {
                        color: '#ffffff'
                    },
                    {
                        visibility: 'on'
                    }
                ]
            },
            {
                featureType: 'all',
                elementType: 'geometry.fill',
                stylers: [
                    {
                        saturation: '-67'
                    },
                    {
                        lightness: '100'
                    },
                    {
                        gamma: '9.89'
                    },
                    {
                        color: '#ffffff'
                    }
                ]
            },
            {
                featureType: 'all',
                elementType: 'labels',
                stylers: [
                    {
                        color: '#ff0000'
                    },
                    {
                        visibility: 'off'
                    }
                ]
            },
            {
                featureType: 'all',
                elementType: 'labels.text.fill',
                stylers: [
                    {
                        gamma: 0.01
                    },
                    {
                        lightness: 20
                    }
                ]
            },
            {
                featureType: 'all',
                elementType: 'labels.text.stroke',
                stylers: [
                    {
                        saturation: -31
                    },
                    {
                        lightness: -33
                    },
                    {
                        weight: 2
                    },
                    {
                        gamma: 0.8
                    }
                ]
            },
            {
                featureType: 'all',
                elementType: 'labels.icon',
                stylers: [
                    {
                        visibility: 'off'
                    }
                ]
            },
            {
                featureType: 'administrative.province',
                elementType: 'all',
                stylers: [
                    {
                        visibility: 'off'
                    }
                ]
            },
            {
                featureType: 'administrative.province',
                elementType: 'geometry',
                stylers: [
                    {
                        visibility: 'off'
                    }
                ]
            },
            {
                featureType: 'administrative.province',
                elementType: 'geometry.fill',
                stylers: [
                    {
                        visibility: 'off'
                    }
                ]
            },
            {
                featureType: 'administrative.province',
                elementType: 'geometry.stroke',
                stylers: [
                    {
                        visibility: 'off'
                    }
                ]
            },
            {
                featureType: 'administrative.province',
                elementType: 'labels',
                stylers: [
                    {
                        visibility: 'off'
                    }
                ]
            },
            {
                featureType: 'administrative.province',
                elementType: 'labels.text',
                stylers: [
                    {
                        visibility: 'off'
                    }
                ]
            },
            {
                featureType: 'administrative.locality',
                elementType: 'geometry.fill',
                stylers: [
                    {
                        visibility: 'off'
                    }
                ]
            },
            {
                featureType: 'administrative.neighborhood',
                elementType: 'geometry.stroke',
                stylers: [
                    {
                        visibility: 'off'
                    }
                ]
            },
            {
                featureType: 'landscape',
                elementType: 'geometry',
                stylers: [
                    {
                        lightness: 30
                    },
                    {
                        saturation: 30
                    }
                ]
            },
            {
                featureType: 'landscape',
                elementType: 'geometry.fill',
                stylers: [
                    {
                        color: '#c5dbed'
                    }
                ]
            },
            {
                featureType: 'poi',
                elementType: 'geometry',
                stylers: [
                    {
                        saturation: 20
                    }
                ]
            },
            {
                featureType: 'poi.park',
                elementType: 'geometry',
                stylers: [
                    {
                        lightness: 20
                    },
                    {
                        saturation: -20
                    }
                ]
            },
            {
                featureType: 'road',
                elementType: 'geometry',
                stylers: [
                    {
                        lightness: 10
                    },
                    {
                        saturation: -30
                    }
                ]
            },
            {
                featureType: 'road',
                elementType: 'geometry.stroke',
                stylers: [
                    {
                        saturation: 25
                    },
                    {
                        lightness: 25
                    }
                ]
            },
            {
                featureType: 'water',
                elementType: 'all',
                stylers: [
                    {
                        lightness: -20
                    }
                ]
            },
            {
                featureType: 'water',
                elementType: 'geometry.fill',
                stylers: [
                    {
                        color: '#ffffff'
                    },
                    {
                        saturation: '69'
                    },
                    {
                        lightness: '100'
                    },
                    {
                        gamma: '7.26'
                    }
                ]
            }
        ];

        this.map.setOptions({
            styles: this.featuresopt
        });
        this.addMarkers([
            {
                latLong: { lat: 28.631027, lng: 77.382022 },
                // tslint:disable-next-line:max-line-length
                company: `<div class="pop" style="text-align:left;">
                        <h4 style="margin-top:5px;margin-bottom:5px;font-size:18px;">INDIA</h4>
                        <p style="margin-top:1px;color:#878787;font-size:12px;">H-160, Sector-63 Rd, <br> Noida Uttar Pradesh 201307</p>
                    </div>`
            },
            {
                latLong: { lat: 43.660375, lng: -79.675897 },
                // tslint:disable-next-line:max-line-length
                company: `<div class="pop" style="text-align:left;">
                        <h4 style="margin-top:5px;margin-bottom:5px;font-size:18px;">CANADA</h4>
                        <p style="margin-top:1px;color:#878787;font-size:12px;">6705 Tomken Rd,<br> Mississauga, ON L5T 2J6</p>
                    </div>`
            },
            {
                latLong: { lat: -33.867178, lng: 151.204759 },
                // tslint:disable-next-line:max-line-length
                company: `<div class="pop" style="text-align:left;">
                        <h4 style="margin-top:5px;margin-bottom:5px;font-size:18px;">AUSTRALIA</h4>
                        <p style="margin-top:1px;color:#878787;font-size:12px;">Level 15, 309 Kent street,<br> Sydney NSW2000 E15 4QZ</p>
                    </div>`
            },
            {
                latLong: { lat: 51.540054, lng: 0.000496 },
                // tslint:disable-next-line:max-line-length
                company: `<div class="pop" style="text-align:left;">
                        <h4 style="margin-top:5px;margin-bottom:5px;font-size:18px;">UK</h4>
                        <p style="margin-top:1px;color:#878787;font-size:12px;">415, High street,<br> Stratford, London, E15 4QZ</p>
                    </div>`
            },
            {
                latLong: { lat: 40.884682, lng: -73.948975 },
                // tslint:disable-next-line:max-line-length
                company: `<div class="pop" style="text-align:left;">
                        <h4 style="margin-top:5px;margin-bottom:5px;font-size:18px;">USA</h4>
                        <p style="margin-top:1px;color:#878787;font-size:12px;"> 560 Syvlan Ave. Suite 309,<br> Englewood Cliffs, NJ 07632</p>
                    </div>`
            }
        ]);
        // console.log(this.map);
    }
    addMarkers(nearByJobs: any[]) {
        for (let i = 0; i < nearByJobs.length; ++i) {
            this.addMarker(nearByJobs[i].latLong, nearByJobs[i].company);
        }
    }
    addMarker(cordinates, company) {
        // marker position
        const marker = new google.maps.Marker({
            position: cordinates,
            icon: require('../../../../../content/images/marker-icon.png'),
            map: this.map
        });

        // marker title
        const infoWindow = new google.maps.InfoWindow({
            content: company
        });

        // title events
        marker.addListener('mouseover', function() {
            infoWindow.open(this.map, marker);
        });
        marker.addListener('mouseout', function() {
            infoWindow.close();
        });
    }

    setEmployer() {
        this.isEmp = false;
    }
    setTalent() {
        this.isEmp = true;
    }
    // getNearByJobs(): void{
    //   this.addMarkers(this.nearByJobs);
    // }
    // addMarkers(nearByJobs: any[]){
    //   for ( let i = 0; i < nearByJobs.length; ++i ) {
    //     this.addMarker(nearByJobs[i].latLong, nearByJobs[i].companyName , nearByJobs[i].designation );
    //   }
    // }
    // addMarker(cordinates, companyName, designation ) {
    //   // marker position
    //   var marker = new google.maps.Marker({
    //     position: cordinates,
    //     map: this.map
    //   });
    //   // marker title
    //   var infoWindow = new google.maps.InfoWindow({
    //     content: "<h6>" + companyName + "</h6> <br /> <p> " + designation
    //   });
    openContactUs() {
        this.modalRef = this.modalService.openXL(ContactUsComponent);
    }
}
