import { Component, OnInit } from '@angular/core';

declare let $: any;

@Component({
    selector: 'jhi-device',
    templateUrl: './device.component.html',
    styleUrls: ['./device.scss']
})
export class DeviceComponent implements OnInit {
    constructor() {}

    ngOnInit() {
        $('.content li').click(function() {
            $(this).addClass('active');
            $(this)
                .siblings('li')
                .removeClass('active');
            if ($(this).hasClass('design') === true) {
                $('.content .preview1').removeClass('hide');
                $('.content .preview1')
                    .siblings('div')
                    .addClass('hide');
            } else if ($(this).hasClass('develop') === true) {
                $('.content .preview2').removeClass('hide');
                $('.content .preview2')
                    .siblings('div')
                    .addClass('hide');
            } else if ($(this).hasClass('deploy') === true) {
                $('.content .preview3').removeClass('hide');
                $('.content .preview3')
                    .siblings('div')
                    .addClass('hide');
            }
        });
    }
}
