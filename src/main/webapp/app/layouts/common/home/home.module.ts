import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// Module for footer and Navigation bar
import { CommonNavFooterModule } from 'app/shared/common/common.module';

// Home Page Components
import {
    HomeComponent,
    AssociatesComponent,
    BlogComponent,
    MapQueryComponent,
    CarouselComponent,
    HowItWorksComponent,
    DeviceComponent,
    TechnologiesComponent,
    OpportunitiesComponent,
    FeaturesComponent,
    WeComponent
} from './';

import { LiveStreamingComponent } from './live-streaming/live-streaming.component';

// HireNg Modules
import { NghireSharedModule } from 'app/shared';

// Route variable
import { HOME_ROUTE } from './';

@NgModule({
    imports: [RouterModule.forChild([HOME_ROUTE]), CommonModule, NghireSharedModule, CommonNavFooterModule],
    declarations: [
        HomeComponent,
        AssociatesComponent,
        BlogComponent,
        MapQueryComponent,
        CarouselComponent,
        HowItWorksComponent,
        DeviceComponent,
        TechnologiesComponent,
        OpportunitiesComponent,
        FeaturesComponent,
        WeComponent,
        LiveStreamingComponent
    ],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NghireHomeModule {}
