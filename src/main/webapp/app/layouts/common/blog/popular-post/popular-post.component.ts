import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'jhi-popular-post',
    templateUrl: './popular-post.component.html',
    styleUrls: ['./popular-post.scss']
})
export class PopularPostComponent implements OnInit {
    constructor() {}

    ngOnInit() {}
    movetoTop() {
        window.scroll(0, 0);
    }
}
