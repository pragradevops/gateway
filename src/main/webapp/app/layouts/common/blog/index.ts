// Blog pages components
export * from './blog.component';
export * from './banner/banner.component';
export * from './blog-detail/blog-detail.component';
export * from './blog-detail/first/first.component';
export * from './blog-detail/second/second.component';
export * from './blog-detail/third/third.component';
export * from './blog-detail/fourth/fourth.component';
export * from './header/header.component';
export * from './main-blog/main-blog.component';
export * from './news-subscription/news-subscription.component';
export * from './popular-post/popular-post.component';
export * from './trending/trending.component';

// Blog route variable
export * from './blog.route';
