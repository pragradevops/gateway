import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'jhi-main-blog',
    templateUrl: './main-blog.component.html',
    styleUrls: ['./main.scss']
})
export class MainBlogComponent implements OnInit {
    constructor() {}

    ngOnInit() {}
    movetoTop() {
        window.scroll(0, 0);
    }
}
