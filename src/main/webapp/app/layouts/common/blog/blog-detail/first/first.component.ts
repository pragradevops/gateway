import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { RegisterModalService } from '../../../../../account';

@Component({
    selector: 'jhi-first',
    templateUrl: './first.component.html',
    styleUrls: ['../blog-detail.scss']
})
export class FirstComponent implements OnInit {
    modalRef: NgbModalRef;
    constructor(private registerModalService: RegisterModalService) {}

    ngOnInit() {}

    movetoTop() {
        window.scroll(0, 0);
    }

    register() {
        this.modalRef = this.registerModalService.open();
    }
}
