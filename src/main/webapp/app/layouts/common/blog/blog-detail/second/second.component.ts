import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { RegisterModalService } from '../../../../../account';

@Component({
    selector: 'jhi-second',
    templateUrl: './second.component.html',
    styleUrls: ['../blog-detail.scss']
})
export class SecondComponent implements OnInit {
    modalRef: NgbModalRef;
    constructor(private registerModalService: RegisterModalService) {}

    ngOnInit() {}
    register() {
        this.modalRef = this.registerModalService.open();
    }
}
