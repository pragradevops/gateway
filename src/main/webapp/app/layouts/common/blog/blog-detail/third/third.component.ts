import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { RegisterModalService } from '../../../../../account';

@Component({
    selector: 'jhi-third',
    templateUrl: './third.component.html',
    styleUrls: ['../blog-detail.scss']
})
export class ThirdComponent implements OnInit {
    modalRef: NgbModalRef;
    constructor(private registerModalService: RegisterModalService) {}

    ngOnInit() {}
    register() {
        this.modalRef = this.registerModalService.open();
    }
}
