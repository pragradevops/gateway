import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { RegisterModalService } from '../../../../../account';

@Component({
    selector: 'jhi-fourth',
    templateUrl: './fourth.component.html',
    styleUrls: ['../blog-detail.scss']
})
export class FourthComponent implements OnInit {
    modalRef: NgbModalRef;
    constructor(private registerModalService: RegisterModalService) {}

    ngOnInit() {}
    movetoTop() {
        window.scroll(0, 0);
    }
    register() {
        this.modalRef = this.registerModalService.open();
    }
}
