import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// Module for footer and Navigation bar
import { CommonNavFooterModule } from 'app/shared/common/common.module';

// Blog page components
import {
    BlogComponent,
    BannerComponent,
    BlogDetailComponent,
    FirstComponent,
    SecondComponent,
    ThirdComponent,
    FourthComponent,
    HeaderComponent,
    MainBlogComponent,
    NewsSubscriptionComponent,
    PopularPostComponent,
    TrendingComponent
} from './';

// Blog route variable
import { BLOG_ROUTE } from './blog.route';

@NgModule({
    imports: [CommonModule, RouterModule.forChild(BLOG_ROUTE), CommonNavFooterModule],
    declarations: [
        BlogComponent,
        HeaderComponent,
        BannerComponent,
        PopularPostComponent,
        NewsSubscriptionComponent,
        TrendingComponent,
        MainBlogComponent,
        BlogDetailComponent,
        FirstComponent,
        SecondComponent,
        ThirdComponent,
        FourthComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BlogModule {}
