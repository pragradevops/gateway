import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'jhi-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.scss']
})
export class HeaderComponent implements OnInit {
    constructor() {}

    ngOnInit() {}
    movetoTop() {
        window.scroll(0, 0);
    }
}
