import { Routes } from '@angular/router';
import { BlogComponent } from './blog.component';
// import { BlogDetailComponent } from './blog-detail/blog-detail.component';
import { FirstComponent } from './blog-detail/first/first.component';
import { SecondComponent } from './blog-detail/second/second.component';
import { ThirdComponent } from './blog-detail/third/third.component';
import { FourthComponent } from './blog-detail/fourth/fourth.component';

export const BLOG_ROUTE: Routes = [
    {
        path: 'blogs',
        component: BlogComponent,
        data: {
            pageTitle: 'Aleeph Advisors | Blogs'
        }
    },
    {
        path: 'blogs/blog-detail/aleeph-solution-making-things-simple',
        component: FirstComponent,
        data: {
            pageTitle: 'Aleeph Advisors | Aleeph Solution Making Things Simple'
        }
    },
    {
        path: 'blogs/blog-detail/aleeph-data-platform',
        component: SecondComponent,
        data: {
            pageTitle: 'Aleeph Advisors | Aleeph Data Platform'
        }
    },
    {
        path: 'blogs/blog-detail/helping-hands-chatbot',
        component: ThirdComponent,
        data: {
            pageTitle: 'Aleeph Advisors | Helping Hands Chatbot'
        }
    },
    {
        path: 'blogs/blog-detail/how-to-make-video-resume-impressive',
        component: FourthComponent,
        data: {
            pageTitle: 'Aleeph Advisors | How To Make Video Resume Impressive'
        }
    }
];
