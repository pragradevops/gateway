import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { RegisterModalService } from '../../../account';

@Component({
    selector: 'jhi-blog',
    templateUrl: './blog.component.html',
    styleUrls: ['./blog.scss']
})
export class BlogComponent implements OnInit {
    modalRef: NgbModalRef;
    constructor(private registerModalService: RegisterModalService) {}

    ngOnInit() {}
    movetoTop() {
        window.scroll(0, 0);
    }

    register() {
        this.modalRef = this.registerModalService.open();
    }
}
