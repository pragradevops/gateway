import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Common directory modules
import { NghireHomeModule, NghireWeModule, NghireDeviseModule, NghireOpportunityModule, BlogModule, PageFooterModule } from './';

@NgModule({
    imports: [CommonModule, NghireHomeModule, NghireWeModule, NghireDeviseModule, NghireOpportunityModule, BlogModule, PageFooterModule],
    declarations: []
})
export class NgHireCommonModule {}
