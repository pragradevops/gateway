import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { RegisterModalService } from 'app/account';

@Component({
    selector: 'jhi-features',
    templateUrl: './features.component.html',
    styleUrls: ['./features.scss']
})
export class FeaturesComponent implements OnInit {
    modalRef: NgbModalRef;

    constructor(private registerModalService: RegisterModalService) {}

    ngOnInit() {}
    register() {
        this.modalRef = this.registerModalService.open();
    }
}
