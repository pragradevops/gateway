import { Route } from '@angular/router';

import { OpportunityComponent } from './opportunity.component';
import { UserRouteSessionService } from 'app/core/auth/user-route-session.service';

export const OPPORTUNITY_ROUTE: Route = {
    path: 'opportunity',
    component: OpportunityComponent,
    data: {
        authorities: [],
        pageTitle: 'InsyghtAI | Opportunity'
    },
    canActivate: [UserRouteSessionService]
};
