import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

// Module for footer and Navigation bar
import { CommonNavFooterModule } from 'app/shared/common/common.module';

// Opportunity page components
import { OpportunityComponent, FeaturesComponent, MainBannerComponent, ModifyComponent } from './';

// HireNg Modules
import { NghireSharedModule } from 'app/shared';

// Opportunity route variable
import { OPPORTUNITY_ROUTE } from './opportunity.route';

@NgModule({
    imports: [RouterModule.forChild([OPPORTUNITY_ROUTE]), NghireSharedModule, CommonNavFooterModule],
    declarations: [OpportunityComponent, MainBannerComponent, ModifyComponent, FeaturesComponent],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NghireOpportunityModule {}
