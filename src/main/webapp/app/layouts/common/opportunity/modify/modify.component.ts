import { Component, OnInit } from '@angular/core';

declare let $: any;

@Component({
    selector: 'jhi-modify',
    templateUrl: './modify.component.html',
    styleUrls: ['./modify.scss']
})
export class ModifyComponent implements OnInit {
    constructor() {}

    ngOnInit() {
        $('#nav-home-tab').click(function() {
            $('.over-hover1').removeClass('over-fix1');
            $('.over-hover1').addClass('over-animation');
            $('#nav-home').removeClass('hide');

            $('#nav-contact .over-hover1').removeClass('over-animation');
            $('#nav-contact .over-hover1').addClass('over-fix3');
            $('#nav-contact').addClass('hide');

            $('#nav-profile .over-hover1').removeClass('over-animation');
            $('#nav-profile .over-hover1').addClass('over-fix2');
            $('#nav-profile').addClass('hide');
        });

        $('#nav-profile-tab').click(function() {
            $('#nav-profile .over-hover1').removeClass('over-fix2');
            $('#nav-profile .over-hover1').addClass('over-animation');
            $('#nav-profile').removeClass('hide');

            $('#nav-home .over-hover1').removeClass('over-animation');
            $('#nav-home .over-hover1').addClass('over-fix1');
            $('#nav-home').addClass('hide');

            $('#nav-contact .over-hover1').removeClass('over-animation');
            $('#nav-contact .over-hover1').addClass('over-fix3');
            $('#nav-contact').addClass('hide');
        });

        $('#nav-contact-tab').click(function() {
            $('#nav-contact .over-hover1').removeClass('over-fix3');
            $('#nav-contact .over-hover1').addClass('over-animation');
            $('#nav-contact').removeClass('hide');

            $('#nav-profile .over-hover1').removeClass('over-animation');
            $('#nav-profile .over-hover1').addClass('over-fix2');
            $('#nav-profile').addClass('hide');

            $('#nav-home .over-hover1').removeClass('over-animation');
            $('#nav-home .over-hover1').addClass('over-fix1');
            $('#nav-home').addClass('hide');
        });
    }
}
