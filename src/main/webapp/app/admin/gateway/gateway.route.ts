import { Route } from '@angular/router';
import { JhiGatewayComponent } from 'app/admin';

export const gatewayRoute: Route = {
    path: 'gateway',
    component: JhiGatewayComponent,
    data: {
        pageTitle: 'Gateway'
    }
};
