import { Routes } from '@angular/router';

import { AdminComponent } from './admin.component';
import { HomeComponent } from './home/home.component';

import { homeRoute } from './home/home.route';
import { auditsRoute, configurationRoute, docsRoute, healthRoute, logsRoute, metricsRoute, userMgmtRoute } from './';

import { UserRouteAccessService } from 'app/core';

const ADMIN_ROUTES = [auditsRoute, configurationRoute, docsRoute, healthRoute, logsRoute, homeRoute, ...userMgmtRoute, metricsRoute];

export const adminState: Routes = [
    // { path: '', redirectTo: '/admin/home', pathMatch: 'full' },
    {
        path: 'admin',
        component: AdminComponent,
        data: {
            authorities: ['ROLE_ADMIN']
        },
        canActivate: [UserRouteAccessService],
        children: ADMIN_ROUTES
    }
];
