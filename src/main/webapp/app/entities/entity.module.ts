import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';

import { ProfilemsProfileModule } from './profilems/profile/profile.module';

/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        ProfilemsProfileModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NghireEntityModule {}
