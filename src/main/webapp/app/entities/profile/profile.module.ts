import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { NghireSharedModule } from '../../shared';
import {
    ProfileComponent,
    ProfileDeleteDialogComponent,
    ProfileDeletePopupComponent,
    ProfileDetailComponent,
    ProfileDialogComponent,
    ProfilePopupComponent,
    profilePopupRoute,
    ProfilePopupService,
    profileRoute,
    ProfileService
} from './';

const ENTITY_STATES = [...profileRoute, ...profilePopupRoute];

@NgModule({
    imports: [NghireSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ProfileComponent,
        ProfileDetailComponent,
        ProfileDialogComponent,
        ProfileDeleteDialogComponent,
        ProfilePopupComponent,
        ProfileDeletePopupComponent
    ],
    entryComponents: [
        ProfileComponent,
        ProfileDialogComponent,
        ProfilePopupComponent,
        ProfileDeleteDialogComponent,
        ProfileDeletePopupComponent
    ],
    providers: [ProfileService, ProfilePopupService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayProfileModule {}
