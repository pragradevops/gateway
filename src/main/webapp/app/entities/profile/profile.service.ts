import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SERVER_API_URL } from '../../app.constants';
import { map } from 'rxjs/operators';
import { Profile } from './profile.model';
import { createRequestOption } from '../../shared';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ProfileService {
    private resourceUrl = SERVER_API_URL + '/profilems/api/profiles';
    private resourceSearchUrl = SERVER_API_URL + '/profilems/api/_search/profiles';

    constructor(private http: HttpClient) {}

    create(profile: Profile): Observable<Profile> {
        const copy = this.convert(profile);
        return this.http.post(this.resourceUrl, copy).pipe(
            map(res => {
                return this.convertItemFromServer(res);
            })
        );
    }

    update(profile: Profile): Observable<Profile> {
        const copy = this.convert(profile);
        return this.http.put(this.resourceUrl, copy).pipe(
            map(res => {
                return this.convertItemFromServer(this.resourceSearchUrl);
            })
        );
    }

    find(id: number): Observable<Profile> {
        return this.http.get<Profile>(`${this.resourceUrl}/${id}`).pipe(
            map(res => {
                return this.convertItemFromServer(res);
            })
        );
    }

    query(req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.get<Profile>(this.resourceUrl, { params: options });
    }

    delete(id: number): Observable<any> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.get<Profile>(this.resourceSearchUrl, { params: options });
    }

    /**
     * Convert a returned JSON object to Profile.
     */
    private convertItemFromServer(json: any): Profile {
        const entity: Profile = Object.assign(new Profile(), json);
        return entity;
    }

    /**
     * Convert a Profile to a JSON which can be sent to the server.
     */
    private convert(profile: Profile): Profile {
        const copy: Profile = Object.assign({}, profile);
        return copy;
    }
}
