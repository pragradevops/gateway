import { BaseEntity } from './../../shared/model/base-entity';

export class Profile implements BaseEntity {
    constructor(public id?: number, public userId?: number, public profileName?: string, public profileScore?: number) {}
}
