import { Component, OnInit } from '@angular/core';
import { MatchingCandidateService } from 'app/user/talent/service/employer/candidates/matching.service';
// import { MatchingJobService } from '../../../service/matching-job.service';

declare let $: any;

@Component({
    selector: 'jhi-trending',
    templateUrl: './trending.component.html',
    styleUrls: ['./trending.scss']
})
export class TrendingComponent implements OnInit {
    src: string[] = [
        // tslint:disable-next-line:max-line-length
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQU-XdCROfQds_Rw8QZpb4tx6P29uw55wrAa8_9GNoHbn-4CW25',
        'https://static.dealdash.com/team/James.jpg',
        'https://www.cada.ca/images/headshots/quote_ken_shaw.jpg',
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQeCkPYWxM19ruENfPzyhizj3-EVLfDFA8RpO5lpG1V69iwfHbMHg',
        'https://jnswire.s3.amazonaws.com/jns-media/9e/dd/91311/Silverman-Michael.jpeg',
        'https://static.dealdash.com/team/James.jpg'
    ];

    map1: any;
    activeSliderId;
    matchingCandidates: any = {};
    matchingCandidatesFirst: any;

    constructor(private matchingCandidateService: MatchingCandidateService) {}

    ngOnInit() {
        $(document).ready(function() {
            $('.slider-icon-bar .like').click(function() {
                $(this).toggleClass('active');
            });
        });

        // pass a function to map
        this.map1 = this.src.map(function(x, index, arr) {
            if (index < arr.length) {
                return {
                    src: arr[index],
                    src1: arr[++index],
                    src2: arr[++index]
                };
            }
        });
        this.map1 = this.map1.slice(0, this.src.length - 2);
        this.getMatchingJobs();
        // console.log(this.map1);
    }

    getMatchingJobs(): void {
        this.matchingCandidates = this.matchingCandidateService.getMatchingCandidates();
        this.matchingCandidatesFirst = this.matchingCandidates.slice(0, 1);
    }

    cycleToSlide(photo) {
        // console.log(photo.id - 1);
        const slideId = photo.id - 1;

        this.activeSliderId = 'ngb-slide-' + slideId;
        // console.log(this.activeSliderId);
    }
}
