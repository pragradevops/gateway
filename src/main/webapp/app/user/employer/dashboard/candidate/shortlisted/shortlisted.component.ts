import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ShortlistedCandidateService } from 'app/user/talent/service/employer/candidates/shortlist.service';
import { ModalService } from 'app/user/talent/service/modal.service';
import { WithdrawalComponent } from 'app/user/talent/modal/job/withdrawal/withdrawal.component';

@Component({
    selector: 'jhi-shortlisted',
    templateUrl: './shortlisted.component.html',
    styleUrls: ['./shortlisted.scss']
})
export class ShortlistedComponent implements OnInit {
    shortlistedCandidates: any = {};
    modalRef: NgbModalRef;

    constructor(private ShortlistedCandidatesService: ShortlistedCandidateService, private modalService: ModalService) {}

    ngOnInit() {
        this.getShortlistedCandidates();
    }

    getShortlistedCandidates(): void {
        this.shortlistedCandidates = this.ShortlistedCandidatesService.getShortlistedCandidates().slice(0, 4);
    }

    openWithdrawal() {
        this.modalRef = this.modalService.open(WithdrawalComponent);
    }
}
