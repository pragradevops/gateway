import { Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';

import { UserRouteAccessService } from 'app/core';

export const DASHBOARD_EMPLOYER_ROUTE: Routes = [
    {
        path: 'employer/dashboard',
        component: DashboardComponent,
        data: {
            pageTitle: 'Aleeph Advisors | Dashboard'
            // authorities: ['ROLE_EMPLOYER'],
        }
        // canActivate: [UserRouteAccessService]
    }
];
