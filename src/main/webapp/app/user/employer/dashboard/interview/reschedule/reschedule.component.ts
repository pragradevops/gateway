import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { RescheduleInterviewService } from 'app/user/talent/service/employer/interviews/reschedule.service';
import { ModalService } from 'app/user/talent/service/modal.service';
import { RescheduleModalComponent, CancelModalComponent } from 'app/user/talent/modal/interview';

@Component({
    selector: 'jhi-reschedule',
    templateUrl: './reschedule.component.html',
    styleUrls: ['./reschedule.scss']
})
export class RescheduleComponent implements OnInit {
    modalRef: NgbModalRef;
    rescheduleInterviews: any = {};

    constructor(private modalService: ModalService, private rescheduleInterviewsService: RescheduleInterviewService) {}

    ngOnInit() {
        this.getScheduleInterview();
    }
    getScheduleInterview(): void {
        this.rescheduleInterviews = this.rescheduleInterviewsService.getRescheduleInterviews().slice(0, 4);
        console.log(this.rescheduleInterviews);
    }
    openCancel() {
        this.modalRef = this.modalService.open(CancelModalComponent);
    }
    openReschedule() {
        this.modalRef = this.modalService.open(RescheduleModalComponent);
    }
}
