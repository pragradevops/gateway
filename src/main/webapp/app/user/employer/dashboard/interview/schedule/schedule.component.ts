import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ScheduleInterviewService } from 'app/user/talent/service/employer/interviews/schedule.service';
import { ModalService } from 'app/user/talent/service/modal.service';
import { RescheduleModalComponent, CancelModalComponent } from 'app/user/talent/modal/interview';

@Component({
    selector: 'jhi-schedule',
    templateUrl: './schedule.component.html',
    styleUrls: ['./schedule.scss']
})
export class ScheduleComponent implements OnInit {
    modalRef: NgbModalRef;
    scheduleInterviews: any = {};

    constructor(private modalService: ModalService, private ScheduleInterviewsService: ScheduleInterviewService) {}

    ngOnInit() {
        this.getScheduleInterview();
    }
    getScheduleInterview(): void {
        this.scheduleInterviews = this.ScheduleInterviewsService.getScheduleInterviews().slice(0, 4);
        console.log(this.scheduleInterviews);
    }
    openCancel() {
        this.modalRef = this.modalService.open(CancelModalComponent);
    }
    openReschedule() {
        this.modalRef = this.modalService.open(RescheduleModalComponent);
    }
}
