import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

// HireNg modules
import { SharedMaterialModule } from 'app/shared/shared-material.module';
import { SharedModule } from '../../shared/shared.module';

// components
import { DashboardComponent } from './dashboard.component';
import { InterviewComponent } from './interview/interview.component';
import { ScheduleComponent } from './interview/schedule/schedule.component';
import { RescheduleComponent } from './interview/reschedule/reschedule.component';
import { StreamingComponent } from './streaming/streaming.component';
import { ShortlistedComponent } from './candidate/shortlisted/shortlisted.component';
import { TrendingComponent } from './candidate/trending/trending.component';

// Dashboard route variable
import { DASHBOARD_EMPLOYER_ROUTE } from './dashboard.route';

@NgModule({
    imports: [
        CommonModule,
        SharedMaterialModule,
        NgbModule.forRoot(),
        RouterModule.forChild(DASHBOARD_EMPLOYER_ROUTE),
        SharedModule,
        FontAwesomeModule
    ],
    declarations: [
        DashboardComponent,
        InterviewComponent,
        ScheduleComponent,
        RescheduleComponent,
        StreamingComponent,
        ShortlistedComponent,
        TrendingComponent
    ]
})
export class DashboardModule {}
