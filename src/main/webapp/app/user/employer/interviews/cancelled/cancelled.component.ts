import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
// services
import { CancelledInterviewService } from '../../../talent/service/interview/cancelled-interview.service';
import { ModalService } from '../../../talent/service/modal.service';
// model
import { CancelModalComponent } from '../../../talent/modal/interview/cancel/cancel.component';
// components
import { RescheduleModalComponent } from '../../../talent/modal/interview/reschedule/reschedule.component';

@Component({
    selector: 'jhi-cancelled',
    templateUrl: './cancelled.component.html',
    styleUrls: ['./cancelled.scss']
})
export class CancelledComponent implements OnInit {
    cancelledInterviews: CancelModalComponent[];
    cancelledInterviewsFirst: any = {};

    modalRef: NgbModalRef;
    constructor(private modalService: ModalService, private cancelledInterviewService: CancelledInterviewService) {}

    ngOnInit() {
        this.getCancelledInterviews();
    }
    getCancelledInterviews(): void {
        this.cancelledInterviews = this.cancelledInterviewService.getCancelledInterviews();
        this.cancelledInterviewsFirst = this.cancelledInterviews.slice(0, 1);
    }

    openReschedule() {
        this.modalRef = this.modalService.open(RescheduleModalComponent);
    }
}
