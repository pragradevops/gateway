import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// HireNG Modules
import { SharedModule } from '../../shared/shared.module';
// Components
import { InterviewsComponent } from './interviews.component';
import { UpcomingComponent } from './upcoming/upcoming.component';
import { AvailableComponent } from './available/available.component';
import { CancelledComponent } from './cancelled/cancelled.component';
// route class
import { interviewRoute } from './interviews.route';
import { InterviewsDetailComponent } from './interviews-detail/interviews-detail.component';

@NgModule({
    imports: [CommonModule, SharedModule, RouterModule.forChild(interviewRoute), NgbModule.forRoot()],
    declarations: [InterviewsComponent, UpcomingComponent, AvailableComponent, CancelledComponent, InterviewsDetailComponent]
})
export class InterviewModule {}
