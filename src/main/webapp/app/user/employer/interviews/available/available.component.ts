import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
// services
import { AvailableInterviewService } from '../../../talent/service/interview/available-interview.service';
import { PaginationService } from '../../../talent/service/pagination.service';
// model
import { AvailableInterview } from '../../../talent/model/interviews/available';
// components
import { AcceptModalComponent, CancelModalComponent, RescheduleModalComponent } from '../../../talent/modal/interview';

// Modal service
import { ModalService } from '../../../talent/service/modal.service';

@Component({
    selector: 'jhi-available',
    templateUrl: './available.component.html',
    styleUrls: ['./available.scss']
})
export class AvailableComponent implements OnInit {
    availableInterviews: AvailableInterview[];
    availableInterviewsFirst: any = {};
    modalRef: NgbModalRef;
    constructor(private modalService: ModalService, private availableInterviewService: AvailableInterviewService) {}

    // pager object
    // pager: any = {};

    // paged items
    // pagedAvailableInterviews: any[];

    // constructor(
    //   private availableInterviewService: AvailableInterviewService,
    //   private paginationService: PaginationService
    // ) { }

    ngOnInit() {
        this.getAvailableInterviews();
        // console.log('availableInterviews');
    }

    // getUpcomingInterviews(): void {
    //   this.availableInterviews = this.availableInterviewService.getAvailableInterviews();
    // }

    // setPage(page: number) {
    //   // get pager object from service
    //   this.pager = this.paginationService.getPager(this.availableInterviews.length, page);
    //   // get current page of items
    //   this.pagedAvailableInterviews = this.availableInterviews.slice(this.pager.startIndex, this.pager.endIndex + 1);
    // }
    getAvailableInterviews(): void {
        this.availableInterviews = this.availableInterviewService.getAvailableInterviews();
        this.availableInterviewsFirst = this.availableInterviews.slice(0, 1);
    }
    openAccept() {
        console.log('inn accept function');
        this.modalRef = this.modalService.open(AcceptModalComponent);
    }
    openCancel() {
        this.modalRef = this.modalService.open(CancelModalComponent);
    }
    openReschedule() {
        this.modalRef = this.modalService.open(RescheduleModalComponent);
    }
}
