import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// HireNg Module
import { DashboardModule } from './dashboard/dashboard.module';

@NgModule({
    imports: [CommonModule, DashboardModule],
    declarations: []
})
export class EmployerModule {}
