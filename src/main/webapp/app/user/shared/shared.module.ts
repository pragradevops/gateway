import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
// Components
// import { FooterComponent } from './footer/footer.component';
// import { TopNavComponent } from './navigation/top-nav/top-nav.component';
// import { HeaderProfileComponent } from './navigation/header-profile/header-profile.component';
// import { MainNavComponent } from './navigation/main-nav/main-nav.component';
// import { NavigationComponent } from './navigation/navigation.component';

// import { EmpNavigationComponent } from './emp-navigation/emp-navigation.component';
// import { EmpHeaderProfileComponent } from './emp-navigation/header-profile/header-profile.component';
// import { EmpMainNavComponent } from './emp-navigation/main-nav/main-nav.component';
// import { EmpTopNavComponent } from './emp-navigation/top-nav/top-nav.component';
import { SideBarComponent } from './component/side-bar/side-bar.component';
import { TopBarComponent } from './component/top-bar/top-bar.component';

@NgModule({
    imports: [CommonModule, RouterModule, FontAwesomeModule, NgbModule.forRoot()],
    declarations: [
        // FooterComponent,
        // TopNavComponent,
        // HeaderProfileComponent,
        // MainNavComponent,
        // NavigationComponent,
        // EmpNavigationComponent,
        // EmpHeaderProfileComponent,
        // EmpMainNavComponent,
        // EmpTopNavComponent,
        SideBarComponent,
        TopBarComponent
    ],
    exports: [SideBarComponent, TopBarComponent],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SharedModule {}
