import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { faUser, faSearch, faChevronDown } from '@fortawesome/free-solid-svg-icons';

// services
import { LoginService } from 'app/core/login/login.service';
// import { ProfileService } from '../../../talent/profile/profile.service';

@Component({
    selector: 'jhi-top-bar',
    templateUrl: './top-bar.component.html',
    styleUrls: ['./top-bar.scss']
})
export class TopBarComponent implements OnInit {
    //  fart icons
    faUser = faUser;
    faSearch = faSearch;
    faChevronDown = faChevronDown;

    //
    user: any = {};
    userExist = false;
    profileExist = false;
    completeProfile: any;
    numberOfProfiles: number;
    profileTitles: { profileName: 'string'; profileStatus: 'string'; profileId: 'string' }[] = [];

    constructor(private loginService: LoginService, private router: Router) {}

    ngOnInit() {
        // fetching user
        // this.getuser();
    }

    // fetching user and if user exist then call method to fetch all user profiles
    // getuser() {
    //     this.profileService.getuser().subscribe((res: any) => {
    //         this.user = res;
    //         this.userExist = true;
    //         // fetching userAllProfiles
    //         const userId: number = this.user.id;
    //         if (userId) {
    //             this.getUserProfiles(userId);
    //         }
    //     });
    // }

    // method to fetch all the profiles of an user
    // getUserProfiles(userId: number) {
    //     this.profileService.getAllProfiles(userId).subscribe((res: any) => {
    //         this.completeProfile = res;
    //         this.userProfileTitles();
    //     });
    // }

    // method to find the all the profile titles
    // userProfileTitles() {
    //     this.numberOfProfiles = this.completeProfile.length;

    //     if (this.numberOfProfiles) {
    //         this.profileExist = true;
    //         for (let i = 0; i < this.numberOfProfiles; i++) {
    //             this.profileTitles.push({
    //                 profileName:
    //                     this.completeProfile[i].profileDTO.profileName.charAt(0).toUpperCase() +
    //                     this.completeProfile[i].profileDTO.profileName.slice(1),
    //                 profileStatus: this.completeProfile[i].profileDTO.profileStatus,
    //                 profileId: this.completeProfile[i].profileDTO.id
    //             });
    //         }
    //     } else {
    //         this.profileExist = false;
    //     }
    // }

    logout() {
        this.loginService.logout();
        this.router.navigate(['']);
    }
}
