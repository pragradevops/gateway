import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { ContactUsComponent } from '../../../../user/talent/modal/home/contact-us/contact-us.component';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
// HireNg services
import { ModalService } from '../../../../user/talent/service/modal.service';

//
import { SIDE_TABS } from './side-tabs';

@Component({
    selector: 'jhi-side-bar',
    templateUrl: './side-bar.component.html',
    styleUrls: ['./side-bar.scss']
})
export class SideBarComponent implements OnInit, AfterViewInit {
    modalRef: NgbModalRef;
    // Assigns variable from HTML
    @ViewChild('scoreLabal') public scoreLabal: ElementRef;
    @ViewChild('score_rate') public score_rate: ElementRef;

    @ViewChild('strateRate') public straterate: ElementRef;
    @ViewChild('strateProgressBar') public strateProgressBar: ElementRef;

    // Variables for canvas
    border_width = 6;
    border_blur = '10';
    background_color = 'rgba(0,0,0,0)';
    border_shadow_color = 'rgba(144,100,253,0.8';
    second_color = '#ce1541';
    first_color = '#fe6fac';
    unfill_color = '#2c2d36';
    canvasFrame: any;
    progressRate: any;

    strateRate: any;
    strateCanvas: any;
    strokeWidth: any;
    strokeHeight = 4;

    // assing
    sideTabs = SIDE_TABS;
    gradient: any;

    constructor(private modalService: ModalService) {}

    ngOnInit() {}
    openContactUs() {
        this.modalRef = this.modalService.openXL(ContactUsComponent);
    }
    ngAfterViewInit() {
        this.canvasFrame = this.scoreLabal.nativeElement.getContext('2d');
        this.progressRate = +this.score_rate.nativeElement.innerHTML;

        // calling function
        this.scoreLoader(this.progressRate);

        this.strateRate = +this.straterate.nativeElement.innerHTML;
        this.strateCanvas = this.strateProgressBar.nativeElement.getContext('2d');
        this.strokeWidth = this.strateRate * 1.6;

        // Create gradient

        this.gradient = this.strateCanvas.createLinearGradient(0, 0, this.strokeWidth / 2, 0);
        this.gradient.addColorStop(0, '#27add0');
        this.gradient.addColorStop(1, '#158db6');
        // Fill with gradient

        this.strateCanvas.fillStyle = this.gradient;
        this.strateCanvas.fillRect(0, 0, this.strokeWidth, this.strokeHeight);
    }

    scoreLoader(p: number) {
        return { ctx: this.canvasFrame, display: this.display(p) };
    }

    display(p: number) {
        // setting properties of canvas
        this.canvasFrame.lineWidth = this.border_width;
        this.canvasFrame.lineCap = 'round';
        this.canvasFrame.shadowColor = this.border_shadow_color;
        this.canvasFrame.shadowBlur = this.border_blur;
        this.canvasFrame.fillStyle = this.background_color;
        this.canvasFrame.fillRect(0, 0, this.canvasFrame.canvas.width, this.canvasFrame.canvas.height);
        const center = this.canvasFrame.canvas.width / 2;

        // for gredient
        const grd = this.canvasFrame.createLinearGradient(0, 0, 170, 0);
        grd.addColorStop(0, this.second_color);
        grd.addColorStop(0.3, this.first_color);
        grd.addColorStop(1, this.first_color);

        // for pink circle
        this.canvasFrame.beginPath();

        this.canvasFrame.arc(center, center, 50, 0, 2 * (p / 10) * Math.PI);
        this.canvasFrame.strokeStyle = grd;
        console.log(this.canvasFrame);
        this.canvasFrame.stroke();
    }
}
