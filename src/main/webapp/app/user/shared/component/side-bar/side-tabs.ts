export const SIDE_TABS: any[] = [
    { name: 'Live Streaming', routerLink: '/talent/streaming', disableStatus: true },
    { name: 'Dashboard', routerLink: '/talent/dashboard', disableStatus: false },
    { name: 'Profile', routerLink: '/talent/profile', disableStatus: false },
    { name: 'Jobs', routerLink: '/talent/jobs', disableStatus: false },
    { name: 'Interviews', routerLink: '/talent/interview', disableStatus: false },
    { name: 'Connections', routerLink: '/talent/connection', disableStatus: true },
    { name: 'Support', routerLink: '/talent/support', disableStatus: true },
    { name: 'Social Media', routerLink: '/talent/social-media', disableStatus: true }
];
