import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SERVER_API_URL } from '../../../app.constants';
import { AuthServerProvider } from 'app/core';

// service
// import { AuthServerProvider } from '../../../shared/auth/auth-jwt.service';

@Injectable()
export class JwtTokenService {
    private resourceUrl = SERVER_API_URL + '/profilems/api/complete-profile_1';

    constructor(private authServerProvider: AuthServerProvider, private httpClient: HttpClient) {}

    getHttpHeader(): any {
        const jwt = this.authServerProvider.getToken();
        // let headers = new HttpHeaders().set('Content-Type', 'application/json')
        //     .set('authorization', 'Bearer ' + jwt);

        // console.log(headers);
    }
}
