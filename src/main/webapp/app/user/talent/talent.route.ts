import { Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';

// components
// dashboard
import { DashboardComponent } from './dashboard/dashboard.component';

// profile
// import { EditComponent } from './profile/edit.component';
import { AddComponent } from './profile/add/add.component';
import { ProfileComponent } from './profile/exist/profile/profile.component';
// import { EducationModalComponent } from './profile/education/education-modal/education-modal.component';
// import { WorkExperienceModalComponent } from './profile/work-experience/work-experience-modal/work-experience-modal.component';
// import { SkillsModalComponent } from './profile/skills/skills-modal/skills-modal.component';
// import { DesiredJobModalComponent } from './profile/desired-job/desired-job-modal/desired-job-modal.component';
// import { BasicPersonalDetailsComponent } from './profile/basic-profile/personal-detail/personal-detail.component';
// import { BasicProfileUploadComponent } from './profile/basic-profile/profile-upload/profile-upload.component';
import { UserRouteAccessBasicDetailsService } from 'app/shared/auth/user-route-access-basic-details.service';

export const talentRoute: Routes = [
    {
        path: 'talent',
        children: [
            {
                path: 'dashboard',
                component: DashboardComponent,
                data: {
                    pageTitle: 'Aleeph Advisors | Dashboard'
                },
                canActivate: [UserRouteAccessService]
            },
            // {
            //     path: 'profile',
            //     component: BasicPersonalDetailsComponent,
            //     data: {
            //         pageTitle: 'Aleeph Advisors | Profile',
            //         authorities: ['ROLE_EMPLOYEE']
            //     },
            //     canActivate: [UserRouteAccessBasicDetailsService]
            // },
            // {
            //     path: 'resume-upload',
            //     component: BasicProfileUploadComponent,
            //     data: {
            //         pageTitle: 'Aleeph Advisors | Profile',
            //         authorities: ['ROLE_EMPLOYEE']
            //     },
            //     canActivate: [UserRouteAccessService, UserRouteAccessBasicDetailsService]
            // },
            // {
            //     path: 'profile',
            //     component: ProfileComponent,
            //     data: {
            //         pageTitle: 'Aleeph Advisors | Profile'
            //         // authorities: ['ROLE_EMPLOYEE']
            //     },
            //     // canActivate: [UserRouteAccessService],
            //     children: [
            //         {
            //             path: ':id',
            //             component: ProfileComponent
            //         }
            //     ]
            // },
            // {
            //     path: 'profile/add',
            //     component: AddComponent,
            //     data: {
            //         pageTitle: 'Aleeph Advisors | Profile'
            //         // authorities: ['ROLE_EMPLOYEE']
            //     },
            //     // canActivate: [UserRouteAccessService],
            //     children: [
            //         {
            //             path: ':id',
            //             component: AddComponent
            //         }
            //         // {
            //         //     path: 'add',
            //         //     component: AddComponent,
            //         //     data: {
            //         //         pageTitle: 'Aleeph Advisors | Profile'
            //         //     },
            //         //     canActivate: [UserRouteAccessService]
            //         // }
            //     ]
            // },
            {
                path: 'profile',
                children: [
                    {
                        path: 'add',
                        component: AddComponent,
                        data: {
                            pageTitle: 'Aleeph Advisors | Profile'
                            // authorities: ['ROLE_EMPLOYEE']
                        }
                    },
                    {
                        path: 'summary',
                        component: ProfileComponent,
                        data: {
                            pageTitle: 'Aleeph Advisors | Profile'
                            // authorities: ['ROLE_EMPLOYEE']
                        }
                    }
                ]
                // component: ProfileComponent,
                // data: {
                //     pageTitle: 'Aleeph Advisors | Profile'
                //     // authorities: ['ROLE_EMPLOYEE']
                // },
                // // canActivate: [UserRouteAccessService],
                // children: [
                //     {
                //         path: ':id',
                //         component: ProfileComponent
                //     }
                //     // {
                //     //     path: 'add',
                //     //     component: AddComponent,
                //     //     data: {
                //     //         pageTitle: 'Aleeph Advisors | Profile'
                //     //     },
                //     //     canActivate: [UserRouteAccessService]
                //     // }
                // ]
            }
            // {
            //     path: 'profile',
            //     data: {
            //         pageTitle: 'Aleeph Advisors | Profile',
            //         authorities: ['ROLE_EMPLOYEE']
            //     },
            //     canActivate: [UserRouteAccessService],
            //     children: [
            //         {
            //             path: ':id',
            //             component: ProfileComponent
            //         },
            //         {
            //             path: ':id',
            //             component: EditComponent,
            //             children: [
            //                 {
            //                     path: 'work-experience/:id',
            //                     component: WorkExperienceModalComponent,
            //                     data: {
            //                         authorities: ['ROLE_EMPLOYEE'],
            //                         pageTitle: 'Aleeph Advisors | Profile'
            //                     },
            //                     canActivate: [UserRouteAccessService]
            //                 },
            //                 {
            //                     path: 'education/:id',
            //                     component: EducationModalComponent,
            //                     data: {
            //                         authorities: ['ROLE_EMPLOYEE'],
            //                         pageTitle: 'Aleeph Advisors | Profile'
            //                     },
            //                     canActivate: [UserRouteAccessService]
            //                 },
            //                 {
            //                     path: 'skill/:id',
            //                     component: SkillsModalComponent,
            //                     data: {
            //                         authorities: ['ROLE_EMPLOYEE'],
            //                         pageTitle: 'Aleeph Advisors | Profile'
            //                     },
            //                     canActivate: [UserRouteAccessService]
            //                 },
            //                 {
            //                     path: 'desired-job/:id',
            //                     component: DesiredJobModalComponent,
            //                     data: {
            //                         authorities: ['ROLE_EMPLOYEE'],
            //                         pageTitle: 'Aleeph Advisors | Profile'
            //                     },
            //                     canActivate: [UserRouteAccessService]
            //                 }
            //             ]
            //         }
            //     ]
            // }
        ]
    }
];
