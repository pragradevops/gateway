import { Component, OnInit } from '@angular/core';
// Services
import { FavouriteJobService } from '../../service/favourite-job.service';
import { PaginationService } from '../../service/pagination.service';
// Model classes
declare let $: any;
@Component({
    selector: 'jhi-favourite',
    templateUrl: './favourite.component.html',
    styleUrls: ['./favourite.scss']
})
export class FavouriteComponent implements OnInit {
    favouriteJobs: any = {};
    favouriteJobFirst: any;
    // pager object
    pager: any = {};

    // paged items
    pagedFavouriteJobs: any[];

    constructor(private favouriteJobsService: FavouriteJobService, private paginationService: PaginationService) {}

    ngOnInit() {
        this.getFavouriteJobs();
        this.setPage(1);
        $(document).ready(function() {
            $('.right button').click(function() {
                $(this).toggleClass('unfavourite');
            });
        });
    }
    getFavouriteJobs(): void {
        this.favouriteJobs = this.favouriteJobsService.getFavouriteJobs();
        this.favouriteJobFirst = this.favouriteJobs.slice(0, 1);
    }

    setPage(page: number) {
        // get pager object from service
        this.pager = this.paginationService.getPager(this.favouriteJobs.length, page);
        // get current page of items
        this.pagedFavouriteJobs = this.favouriteJobs.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }
}
