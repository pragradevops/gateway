import { Component, OnInit } from '@angular/core';

declare let $: any;
@Component({
    selector: 'jhi-jobs-detail',
    templateUrl: './jobs-detail.component.html',
    styleUrls: ['./jobs-detail.scss']
})
export class JobsDetailComponent implements OnInit {
    constructor() {}

    ngOnInit() {
        $(document).ready(function() {
            $('.data-preview .favourite').click(function() {
                $(this).toggleClass('active');
            });
        });
    }
}
