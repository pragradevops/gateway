import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
// Services
import { AppliedJobsService } from '../../service/applied-jobs.service';
import { PaginationService } from '../../service/pagination.service';
import { ModalService } from '../../service/modal.service';
// Model classes
// components
import { WithdrawalComponent } from '../../modal/job/withdrawal/withdrawal.component';

@Component({
    selector: 'jhi-applied',
    templateUrl: './applied.component.html',
    styleUrls: ['./applied.scss']
})
export class AppliedComponent implements OnInit {
    appliedJobs: any = {};
    appliedJobFirst: any = {};
    // pager object
    pager: any = {};

    // paged items
    pagedAppliedJobs: any[];
    modalRef: NgbModalRef;

    constructor(
        private appliedJobsService: AppliedJobsService,
        private paginationService: PaginationService,
        private modalService: ModalService
    ) {}

    ngOnInit() {
        this.getAppliedJobs();
        this.setPage(1);
    }
    openCancel() {
        this.modalRef = this.modalService.open(WithdrawalComponent);
    }
    getAppliedJobs(): void {
        this.appliedJobs = this.appliedJobsService.getAppliedJobs();
        this.appliedJobFirst = this.appliedJobs.slice(0, 1);
    }

    setPage(page: number) {
        // get pager object from service
        this.pager = this.paginationService.getPager(this.appliedJobs.length, page);
        // get current page of items
        this.pagedAppliedJobs = this.appliedJobs.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }
}
