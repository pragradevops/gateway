import { Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
// components
import { JobsComponent } from './jobs.component';
import { JobsDetailComponent } from './jobs-detail/jobs-detail.component';

export const jobsTalentRoute: Routes = [
    {
        path: 'talent/jobs',
        component: JobsComponent,
        data: {
            // authorities: ['ROLE_EMPLOYEE'],
            pageTitle: 'Aleeph Advisors | Jobs'
        }
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'talent/jobs-detail',
        component: JobsDetailComponent,
        data: {
            // authorities: ['ROLE_EMPLOYEE'],
            pageTitle: 'Aleeph Advisors | Jobs'
        }
        // canActivate: [UserRouteAccessService]
    }
];
