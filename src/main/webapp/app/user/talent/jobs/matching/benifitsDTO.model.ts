interface BenifitsDTOResponseObj {
    description: string;
    id: string;
    jobPostId: string;
}

interface JobPostDTOResponseObj {
    availability: string;
    closingDate: string;
    experienceRequired: string;
    id: string;
    jobCategory: string;
    jobCity: string;
    jobCountry: string;
    jobDescription: string;
    jobPostStatus: string;
    jobTitle: string;
    jobType: string;
    preference: string;
    profileId: string;
    qualification: string;
    salaryMax: string;
    salaryMin: string;
    userId: number;
}

interface RequiredSkillDTOResponseObj {
    experience: string;
    id: string;
    jobPostId: string;
    proficiencyLevel: string;
    skill: string;
}

interface RequirementDTOResponseObj {
    description: string;
    id: string;
    jobPostId: string;
}

interface JobPostResponseObj {
    benefitDTOList: Array<BenifitsDTOResponseObj>;
    requiredSkillDTOList: Array<RequiredSkillDTOResponseObj>;
    requirementDTOList: Array<RequirementDTOResponseObj>;
    jobPostDTO: JobPostDTOResponseObj;
}
