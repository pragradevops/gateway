import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SERVER_API_URL } from '../../../../app.constants';
// mock-apis
import { MATCHING_JOBS } from '../../mock-apis/jobs/matching';

interface BenifitsDTOResponseObj {
    description: string;
    id: string;
    jobPostId: string;
}

interface JobPostDTOResponseObj {
    availability: string;
    closingDate: string;
    experienceRequired: string;
    id: string;
    jobCategory: string;
    jobCity: string;
    jobCountry: string;
    jobDescription: string;
    jobPostStatus: string;
    jobTitle: string;
    jobType: string;
    preference: string;
    profileId: string;
    qualification: string;
    salaryMax: string;
    salaryMin: string;
    userId: number;
}

interface RequiredSkillDTOResponseObj {
    experience: string;
    id: string;
    jobPostId: string;
    proficiencyLevel: string;
    skill: string;
}

interface RequirementDTOResponseObj {
    description: string;
    id: string;
    jobPostId: string;
}

interface JobPostResponseObj {
    benefitDTOList: Array<BenifitsDTOResponseObj>;
    requiredSkillDTOList: Array<RequiredSkillDTOResponseObj>;
    requirementDTOList: Array<RequirementDTOResponseObj>;
    jobPostDTO: JobPostDTOResponseObj;
}

@Injectable()
export class MatchingService {
    matchingJobs: any[] = [];

    private resourceUrl = SERVER_API_URL + '/jobms/api/complete-job-post';

    constructor(private http: HttpClient) {}

    getMatchingJobIds(profileId: string) {
        return ['5b241b73d4d9692db4df680d', '2'];
    }

    getMatchingJobs(id: string[]): any {
        return MATCHING_JOBS;
    }
}
