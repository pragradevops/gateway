import { Component, OnInit } from '@angular/core';
// services
import { MatchingJobService } from '../../service/matching-job.service';
import { PaginationService } from '../../service/pagination.service';
// Model classes
// import { MatchingJob } from '../../model/jobs/matching';
declare let $: any;
@Component({
    selector: 'jhi-matching',
    templateUrl: './matching.component.html',
    styleUrls: ['./matching.scss']
})
export class MatchingComponent implements OnInit {
    matchingJobIds: string[];
    matchingJobs: any = {};
    matchingJobsFirst: any;
    // pager object
    pager: any = {};
    profileId: string;
    // paged items
    pagedMatchingJobs: any[];

    constructor(private matchingService: MatchingJobService, private paginationService: PaginationService) {}

    ngOnInit() {
        $(document).ready(function() {
            $('.right .favourite').click(function() {
                $(this).toggleClass('active');
            });
        });
        this.getMatchingJobs();
        this.setPage(1);
    }

    getMatchingJobs(): void {
        this.matchingJobs = this.matchingService.getMatchingJobs();
        this.matchingJobsFirst = this.matchingJobs.slice(0, 1);
    }

    setPage(page: number) {
        // get pager object from service
        this.pager = this.paginationService.getPager(this.matchingJobIds.length, page);
        // get current page of items
        // this.pagedMatchingJobs = this.matchingJobIds.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }
}
