import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// HireNG modules
import { SharedModule } from '../../shared/shared.module';

// components
import { AppliedComponent } from './applied/applied.component';
import { MatchingComponent } from './matching/matching.component';
import { FavouriteComponent } from './favourite/favourite.component';
import { jobsTalentRoute } from './jobs.route';
import { JobsComponent } from './jobs.component';
import { JobsDetailComponent } from './jobs-detail/jobs-detail.component';

// services
import { MatchingService } from './matching/matching.service';

@NgModule({
    imports: [CommonModule, RouterModule.forChild(jobsTalentRoute), SharedModule, NgbModule],
    declarations: [AppliedComponent, MatchingComponent, FavouriteComponent, JobsComponent, JobsDetailComponent],
    providers: [MatchingService]
})
export class JobsModule {}
