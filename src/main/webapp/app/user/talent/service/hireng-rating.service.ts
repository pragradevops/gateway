import { Injectable } from '@angular/core';
// mock-apis
import { HIRENG_RATINGS } from '../mock-apis/hireng-ratings';

@Injectable({ providedIn: 'root' })
export class HirengRatingService {
    constructor() {}

    getRatingHireNG(): any {
        return HIRENG_RATINGS;
    }
}
