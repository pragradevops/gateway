import { Injectable } from '@angular/core';
//
import { RESCHEDULE_INTERVIEWS } from '../../../mock-apis/employer/interviews/reschedule';

@Injectable({ providedIn: 'root' })
export class RescheduleInterviewService {
    constructor() {}
    getRescheduleInterviews(): any {
        return RESCHEDULE_INTERVIEWS;
    }
}
