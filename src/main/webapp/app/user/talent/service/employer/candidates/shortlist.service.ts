import { Injectable } from '@angular/core';
import { SHORTLISTED_CANDIDATES } from '../../../mock-apis/employer/candidate/shortlist';

@Injectable({ providedIn: 'root' })
export class ShortlistedCandidateService {
    constructor() {}
    getShortlistedCandidates(): any {
        return SHORTLISTED_CANDIDATES;
    }
}
