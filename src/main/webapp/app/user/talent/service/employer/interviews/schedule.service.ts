import { Injectable } from '@angular/core';
//
import { SCHEDULE_INTERVIEWS } from '../../../mock-apis/employer/interviews/schedule';

@Injectable({ providedIn: 'root' })
export class ScheduleInterviewService {
    constructor() {}
    getScheduleInterviews(): any {
        return SCHEDULE_INTERVIEWS;
    }
}
