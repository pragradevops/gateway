import { Injectable } from '@angular/core';
//
import { MATCHING_CANDIDATES } from '../../../mock-apis/employer/candidate/matching';

@Injectable({ providedIn: 'root' })
export class MatchingCandidateService {
    constructor() {}
    getMatchingCandidates(): any {
        return MATCHING_CANDIDATES;
    }
}
