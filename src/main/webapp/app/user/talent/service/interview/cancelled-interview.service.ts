import { Injectable } from '@angular/core';
// Variables Import
import { Cancelled } from '../../model/interviews/cancelled';
// Mock API Import
import { CANCELLED_INTERVIEWS } from '../../mock-apis/interviews/cancelled';

@Injectable({ providedIn: 'root' })
export class CancelledInterviewService {
    constructor() {}

    getCancelledInterviews(): any[] {
        return CANCELLED_INTERVIEWS;
    }
}
