import { Injectable } from '@angular/core';
import { UPCOMING_INTERVIEWS } from '../../mock-apis/interviews/upcoming';

@Injectable({ providedIn: 'root' })
export class UpcomingInterviewService {
    constructor() {}

    getUpcomingInterviews(): any[] {
        return UPCOMING_INTERVIEWS;
    }
}
