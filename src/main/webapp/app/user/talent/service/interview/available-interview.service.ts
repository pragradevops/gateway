import { Injectable } from '@angular/core';
import { AVAILABLE_INTERVIEWS } from '../../mock-apis/interviews/available';

@Injectable({ providedIn: 'root' })
export class AvailableInterviewService {
    constructor() {}

    getAvailableInterviews(): any[] {
        return AVAILABLE_INTERVIEWS;
    }
}
