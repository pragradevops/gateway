import { Injectable } from '@angular/core';
// import { Observable, of } from 'rxjs';
import { APPLIED_JOBS } from '../mock-apis/jobs/applied';

@Injectable({ providedIn: 'root' })
export class AppliedJobsService {
    constructor() {}

    // getAppliedJobs(): Observable<AppliedJob[]> {
    //   return of(APPLIED_JOBS);
    // }

    getAppliedJobs(): any {
        return APPLIED_JOBS;
    }
}
