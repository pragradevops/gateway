import { Injectable } from '@angular/core';
// service
import { NEARBY_CANDIDATES, NEARBY_JOBS } from '../mock-apis/jobs/nearby-jobs';

@Injectable({ providedIn: 'root' })
export class NearbyJobsService {
    constructor() {}

    getNearBYJobs(): any[] {
        return NEARBY_JOBS;
    }

    getNearBYCandidates(): any[] {
        return NEARBY_CANDIDATES;
    }
}
