import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SERVER_API_URL } from '../../../app.constants';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class FileUploadService {
    document: any = {};
    private resourceUrl = SERVER_API_URL + '/api/upload-file?fileType=imgfile';

    constructor(private http: HttpClient) {}

    pushFileToStorage(file: File): Observable<any> {
        const formdata: FormData = new FormData();
        console.log(formdata);
        formdata.append('file', file);

        return this.http.post(this.resourceUrl, formdata);
    }
}
