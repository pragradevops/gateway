import { Injectable } from '@angular/core';
//
import { FAVOURITE_JOBS } from '../mock-apis/jobs/favourite';

@Injectable({ providedIn: 'root' })
export class FavouriteJobService {
    constructor() {}

    getFavouriteJobs(): any {
        return FAVOURITE_JOBS;
    }
}
