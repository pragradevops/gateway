import { Injectable } from '@angular/core';
//
import { MATCHING_JOBS } from '../mock-apis/jobs/matching';

@Injectable({ providedIn: 'root' })
export class MatchingJobService {
    constructor() {}
    getMatchingJobs(): any {
        return MATCHING_JOBS;
    }
}
