import { Injectable } from '@angular/core';
// mock-apis
import { COMPANY_NAME } from '../mock-apis/referenceData/companyName';
import { DESIGNATION } from '../mock-apis/referenceData/designation';
import { LOCATION } from '../mock-apis/referenceData/location';
// import { Observable, of } from 'rxjs';
// import { of } from 'rxjs/Observable/of';
// import { Observable } from 'rxjs/Observable';
// import { of } from 'rxjs/Observable/of';

@Injectable({ providedIn: 'root' })
export class ReferenceDataService {
    constructor() {}

    getDesignation(): string[] {
        return DESIGNATION;
    }

    getCompanyName(): string[] {
        return COMPANY_NAME;
    }

    getLocation(): string[] {
        return LOCATION;
    }
}
