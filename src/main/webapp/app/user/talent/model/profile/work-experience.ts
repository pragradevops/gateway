import { BaseEntity } from '../../../../shared/model/base-entity';

export enum JobTypeEnum {
    FULL_TIME = 'FULL_TIME',
    PART_TIME = 'PART_TIME'
}
export class WorkExperience implements BaseEntity {
    constructor(
        public id?: string,
        public currentEmployer?: true,
        public designation?: string,
        public employerName?: string,
        public endLocalDate?: string,
        public jobDescription?: string,
        public jobLocation?: string,
        public jobType?: JobTypeEnum,
        public profileId?: string,
        public startLocalDate?: string
    ) {}
}
