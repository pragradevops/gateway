export enum JobTypeEnum {
    FULL_TIME = 'Full Time', PART_TIME = 'Part Time'
}
export enum SalaryFigureTypeEnum {
    INR= 'INR',
    DOLLAR= 'Dollar'
}
export enum SalaryTypeEnum {
    MONTH= 'Month',
    YEAR= 'Year'
}
export class AppliedJob {
    companyName: string;
    designation: string;
    jobType?: JobTypeEnum;
    location?: string[];
    appliedDate?: Date;
    companyLogoUrl?: string;
    description: string;
    salaryFigureType?: SalaryFigureTypeEnum;
    salaryStart: number;
    salaryEnd?: number;
    salaryType?: SalaryTypeEnum;
}
