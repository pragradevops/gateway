export enum JobTypeEnum {
    FULL_TIME = 'Full Time', PART_TIME = 'Part Time'
}

export class FavouriteJob {
    companyName: string;
    designation: string;
    jobType?: JobTypeEnum;
    location?: string[];
    favouriteDate?: Date;
    companyLogoUrl?: string;
    description: string;
    openPosition?: number;
    isFavourite: boolean;
}
