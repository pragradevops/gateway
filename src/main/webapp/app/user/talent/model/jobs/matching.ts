export enum JobTypeEnum {
    FULL_TIME = 'Full Time', PART_TIME = 'Part Time'
}

export class MatchingJob {
    designation: string;
    jobType?: JobTypeEnum;
    location?: string[];
    companyLogoUrl?: string;
    isFavourite: boolean;
    setReminder: boolean;
}
