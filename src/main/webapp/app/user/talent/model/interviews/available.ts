export enum JobTypeEnum {
    FULL_TIME= 'Full Time', PART_TIME= 'Part Time'
}
export enum TimeZoneTypeEnum {
    IST= 'IST',
    EST= 'EST'
}
export enum MeridianTypeEnum {
    AM= 'A.M',
    PM= 'P.M'
}
export class AvailableInterview {
    companyName: string;
    designation: string;
    jobType?: JobTypeEnum;
    location?: string[];
    interviewDate?: Date;
    companyLogoUrl?: string;
    description: string;
    startInterviewTime: number;
    endInterviewTime?: number;
    meridianTimeType?: MeridianTypeEnum;
    timeZoneType?: TimeZoneTypeEnum;
}
