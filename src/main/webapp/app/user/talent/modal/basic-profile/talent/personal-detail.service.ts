import { Injectable } from '@angular/core';
// import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SERVER_API_URL } from '../../../../../app.constants';

import { PersonalDetail } from './personal-detail.model';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class PersonalDetailService {
    private resourceUrl = SERVER_API_URL + '/api/basic-profile';

    constructor(private http: HttpClient) {}

    create(personalDetail: PersonalDetail): Observable<PersonalDetail> {
        const copy = this.convert(personalDetail);
        return this.http.post(this.resourceUrl, copy).pipe(
            map(res => {
                return this.convertItemFromServer(res);
            })
        );
    }

    /**
     * Convert a returned JSON object to Profile.
     */
    private convertItemFromServer(json: any): PersonalDetail {
        const entity: PersonalDetail = Object.assign(new PersonalDetail(), json);
        return entity;
    }
    /**
     * Convert a Profile to a JSON which can be sent to the server.
     */
    private convert(personalDetail: PersonalDetail): PersonalDetail {
        const copy: PersonalDetail = Object.assign({}, personalDetail);
        return copy;
    }
}
