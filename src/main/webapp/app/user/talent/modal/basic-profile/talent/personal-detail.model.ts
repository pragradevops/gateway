export enum GenderTypeEnum {
    MALE = 'MALE', FEMALE = 'FEMALE', OTHERS = 'OTHERS'
}

export class PersonalDetail {

    constructor(
        public addressLine?: string,
        public alternativeMobile?: string,
        public firstName?: string,
        public imageUrl?: string,
        public lastName?: string,
        public lat?: string,
        public lng?: string,
        public mobile?: string,
        public dateOfBirth?: string,
        public gender?: GenderTypeEnum.MALE
    ) {
    }
}
