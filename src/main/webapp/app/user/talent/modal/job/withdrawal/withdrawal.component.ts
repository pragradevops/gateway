import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'jhi-withdrawal',
    templateUrl: './withdrawal.component.html',
    styleUrls: ['./withdrawal.scss']
})
export class WithdrawalComponent implements OnInit {
    constructor(public activeModal: NgbActiveModal) {}

    ngOnInit() {}
}
