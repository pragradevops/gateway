import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'jhi-reschedule',
    templateUrl: './reschedule.component.html',
    styleUrls: ['./reschedule.scss']
})
export class RescheduleModalComponent implements OnInit {
    constructor(public activeModal: NgbActiveModal) {}

    ngOnInit() {}
}
