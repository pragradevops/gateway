import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'jhi-cancel',
    templateUrl: './cancel.component.html',
    styleUrls: ['./cancel.scss']
})
export class CancelModalComponent implements OnInit {
    constructor(public activeModal: NgbActiveModal) {}

    ngOnInit() {}
}
