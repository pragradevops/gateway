import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'jhi-contact-us',
    templateUrl: './contact-us.component.html',
    styleUrls: ['./contact-us.scss']
})
export class ContactUsComponent implements OnInit {
    contactUs = {};
    success = false;

    // Form control validator variable
    nameContactUs = new FormControl('', Validators.required);
    emailContactUs = new FormControl('', Validators.required);
    countryContactUs = new FormControl('', Validators.required);
    phonecontactUs = new FormControl('', Validators.required);

    constructor(public activeModal: NgbActiveModal) {}

    ngOnInit() {}

    // Designation error message | validation: Not Null
    getNameErrorMessage() {
        return this.nameContactUs.hasError('required') ? 'You must enter a value' : '';
    }
    // Comapny Name error message | validation: Not Null
    getEmailErrorMessage() {
        return this.emailContactUs.hasError('required') ? 'You must enter a value' : '';
    }
    // Satrt date error message | validation: Not Null
    getCountryErrorMessage() {
        return this.countryContactUs.hasError('required') ? 'You must enter a value' : '';
    }
    // Satrt date error message | validation: Not Null
    getPhoneErrorMessage() {
        return this.phonecontactUs.hasError('required') ? 'You must enter a value' : '';
    }

    save() {
        this.success = true;
        console.log(this.contactUs);
    }
}
