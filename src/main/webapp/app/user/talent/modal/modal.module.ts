import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedMaterialModule } from '../../../shared/shared-material.module';
// Components
import { AcceptModalComponent } from './interview/accept/accept.component';
import { RescheduleModalComponent } from './interview/reschedule/reschedule.component';
import { CancelModalComponent } from './interview/cancel/cancel.component';
import { ViewStatusComponent } from './job/view-status/view-status.component';
import { WithdrawalComponent } from './job/withdrawal/withdrawal.component';
import { ContactUsComponent } from './home/contact-us/contact-us.component';
// services
import { PersonalDetailService } from './basic-profile/talent/personal-detail.service';

@NgModule({
    imports: [CommonModule, BrowserModule, FormsModule, RouterModule, SharedMaterialModule],
    declarations: [
        AcceptModalComponent,
        RescheduleModalComponent,
        CancelModalComponent,
        ViewStatusComponent,
        WithdrawalComponent,
        ContactUsComponent
    ],
    entryComponents: [AcceptModalComponent, RescheduleModalComponent, CancelModalComponent, WithdrawalComponent, ContactUsComponent],
    providers: [PersonalDetailService]
})
export class ModalModule {}
