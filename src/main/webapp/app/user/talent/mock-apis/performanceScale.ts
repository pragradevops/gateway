export const PERFORMANCE_SCALE: any[] = [
    { 'name': 'GPA out of 10', 'value': 'GPA_10', 'numbericValue': 10 },
    { 'name': 'GPA out of 4', 'value': 'GPA_4', 'numbericValue': 4 },
    { 'name': 'Percentage', 'value': 'Percentage', 'numbericValue': 100 },
];
