export const PROFIENCY_LEVEL: any[] = [
    { 'name': 'Beginner', 'value': 'BEGINNER' },
    { 'name': 'Intermediate', 'value': 'INTERMEDIATE' },
    { 'name': 'Expert', 'value': 'EXPERT' }
];
