export const SHORTLISTED_CANDIDATES = [
    {
        candidateUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQU-XdCROfQds_Rw8QZpb4tx6P29uw55wrAa8_9GNoHbn-4CW25',
        candidateName: 'Liyakat Husain',
        jobTitle: 'Developer',
        jobType: 'Full Time',
        candidateCity: 'Noida',
        candidateCountry: 'India',
        jobPostStatus: 'ACTIVE',
        jobCategory: 'IT',
        interviewType: 'Face To Face',
        interviewDate: 'March 20, 2018',
        interviewTime: '9:00 AM - 1:00 PM',

        // salary
        salaryMin: '30',
        salaryMax: '50',
        salaryFigureType: 'INR',
        salaryType: 'k',

        // experience
        experienceMin: '3',
        experienceMax: '5',
        experienceType: 'years',

        // status
        appliedDate: '13-06-2018',
        applicationStatus: 'In Process',
        isFavourite: true,
        isMatching: true,

        // skills
        skills: ['C', 'Java', 'Angular', 'HTML', 'CSS']
    },
    {
        candidateUrl: 'https://static.dealdash.com/team/James.jpg',
        candidateName: 'Krishna',
        jobTitle: 'Sr. Engineer',
        jobType: 'Full Time',
        candidateCity: 'Ghaziabad',
        candidateCountry: 'India',
        jobPostStatus: 'ACTIVE',
        jobCategory: 'IT',
        interviewType: 'Face To Face',
        interviewDate: 'March 20, 2018',
        interviewTime: '9:00 AM - 1:00 PM',

        // salary
        salaryMin: '20',
        salaryMax: '55',
        salaryFigureType: 'INR',
        salaryType: 'k',

        // experience
        experienceMin: '3',
        experienceMax: '5',
        experienceType: 'years',

        // status
        appliedDate: '13-06-2018',
        applicationStatus: 'In Process',
        isFavourite: true,
        isMatching: true,

        // skills
        skills: ['C', 'Java', 'Angular', 'HTML', 'CSS']
    },
    {
        candidateUrl: 'https://www.cada.ca/images/headshots/quote_ken_shaw.jpg',
        candidateName: 'Himanshu',
        jobTitle: 'Angular Developer',
        jobType: 'Over Time',
        candidateCity: 'Lahore',
        candidateCountry: 'Pakistan',
        jobPostStatus: 'ACTIVE',
        jobCategory: 'IT',
        interviewType: 'Face To Face',
        interviewDate: 'March 20, 2018',
        interviewTime: '9:00 AM - 1:00 PM',

        // salary
        salaryMin: '36',
        salaryMax: '53',
        salaryFigureType: 'INR',
        salaryType: 'k',

        // experience
        experienceMin: '3',
        experienceMax: '5',
        experienceType: 'years',

        // status
        appliedDate: '13-06-2018',
        applicationStatus: 'In Process',
        isFavourite: true,
        isMatching: true,

        // skills
        skills: ['C', 'Java', 'Angular', 'HTML', 'CSS']
    },
    {
        candidateUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQeCkPYWxM19ruENfPzyhizj3-EVLfDFA8RpO5lpG1V69iwfHbMHg',
        candidateName: 'Ashutosh',
        jobTitle: 'Java Developer',
        jobType: 'Full Time',
        candidateCity: 'Noida',
        candidateCountry: 'India',
        jobPostStatus: 'ACTIVE',
        jobCategory: 'IT',
        interviewType: 'Face To Face',
        interviewDate: 'March 20, 2018',
        interviewTime: '9:00 AM - 1:00 PM',

        // salary
        salaryMin: '33',
        salaryMax: '45',
        salaryFigureType: 'INR',
        salaryType: 'k',

        // experience
        experienceMin: '3',
        experienceMax: '5',
        experienceType: 'years',

        // status
        appliedDate: '13-06-2018',
        applicationStatus: 'In Process',
        isFavourite: true,
        isMatching: true,

        // skills
        skills: ['C', 'Java', 'Angular', 'HTML', 'CSS']
    },
    {
        logoUrl: 'https://jnswire.s3.amazonaws.com/jns-media/9e/dd/91311/Silverman-Michael.jpeg',
        candidateName: 'Neha',
        jobTitle: 'Hacker',
        jobType: 'Full Time',
        candidateCity: 'Australia',
        candidateCountry: 'USA',
        jobPostStatus: 'ACTIVE',
        jobCategory: 'IT',

        // salary
        salaryMin: '30',
        salaryMax: '50',
        salaryFigureType: 'INR',
        salaryType: 'k',

        // experience
        experienceMin: '3',
        experienceMax: '5',
        experienceType: 'years',

        // status
        appliedDate: '13-06-2018',
        applicationStatus: 'In Process',
        isFavourite: true,
        isMatching: true,

        // skills
        skills: ['C', 'Java', 'Angular', 'HTML', 'CSS']
    },
    {
        logoUrl: 'https://static.dealdash.com/team/James.jpg',
        candidateName: 'Urban Ladder',
        jobTitle: 'Developer',
        jobType: 'Full Time',
        candidateCity: 'Noida',
        candidateCountry: 'India',
        jobPostStatus: 'ACTIVE',
        jobCategory: 'IT',
        interviewType: 'Face To Face',
        interviewDate: 'March 20, 2018',
        interviewTime: '9:00 AM - 1:00 PM',

        // salary
        salaryMin: '3',
        salaryMax: '5',
        salaryFigureType: 'INR',
        salaryType: 'k',

        // experience
        experienceMin: '3',
        experienceMax: '5',
        experienceType: 'years',

        // status
        appliedDate: '13-06-2018',
        applicationStatus: 'In Process',
        isFavourite: true,
        isMatching: true,

        // skills
        skills: ['C', 'Java', 'Angular', 'HTML', 'CSS']
    },
    {
        logoUrl: 'https://seeklogo.com/images/F/flipkart-logo-3F33927DAA-seeklogo.com.png',
        candidateName: 'Urban Ladder',
        jobTitle: 'Developer',
        jobType: 'Full Time',
        candidateCity: 'Noida',
        candidateCountry: 'India',
        jobPostStatus: 'ACTIVE',
        jobCategory: 'IT',

        // salary
        salaryMin: '3',
        salaryMax: '5',
        salaryFigureType: 'INR',
        salaryType: 'k',

        // experience
        experienceMin: '3',
        experienceMax: '5',
        experienceType: 'years',

        // status
        appliedDate: '13-06-2018',
        applicationStatus: 'In Process',
        isFavourite: true,
        isMatching: true,

        // skills
        skills: ['C', 'Java', 'Angular', 'HTML', 'CSS']
    },
    {
        logoUrl: 'https://seeklogo.com/images/F/flipkart-logo-3F33927DAA-seeklogo.com.png',
        candidateName: 'Urban Ladder',
        jobTitle: 'Developer',
        jobType: 'Full Time',
        candidateCity: 'Noida',
        candidateCountry: 'India',
        jobPostStatus: 'ACTIVE',
        jobCategory: 'IT',
        interviewType: 'Face To Face',
        interviewDate: 'March 20, 2018',
        interviewTime: '9:00 AM - 1:00 PM',

        // salary
        salaryMin: '3',
        salaryMax: '5',
        salaryFigureType: 'INR',
        salaryType: 'k',

        // experience
        experienceMin: '3',
        experienceMax: '5',
        experienceType: 'years',

        // status
        appliedDate: '13-06-2018',
        applicationStatus: 'In Process',
        isFavourite: true,
        isMatching: true,

        // skills
        skills: ['C', 'Java', 'Angular', 'HTML', 'CSS']
    },
    {
        logoUrl: 'https://seeklogo.com/images/F/flipkart-logo-3F33927DAA-seeklogo.com.png',
        candidateName: 'Urban Ladder',
        jobTitle: 'Developer',
        jobType: 'Full Time',
        candidateCity: 'Noida',
        candidateCountry: 'India',
        jobPostStatus: 'ACTIVE',
        jobCategory: 'IT',
        interviewType: 'Face To Face',
        interviewDate: 'March 20, 2018',
        interviewTime: '9:00 AM - 1:00 PM',

        // salary
        salaryMin: '3',
        salaryMax: '5',
        salaryFigureType: 'INR',
        salaryType: 'k',

        // experience
        experienceMin: '3',
        experienceMax: '5',
        experienceType: 'years',

        // status
        appliedDate: '13-06-2018',
        applicationStatus: 'In Process',
        isFavourite: true,
        isMatching: true,

        // skills
        skills: ['C', 'Java', 'Angular', 'HTML', 'CSS']
    },
    {
        logoUrl: 'https://seeklogo.com/images/F/flipkart-logo-3F33927DAA-seeklogo.com.png',
        candidateName: 'Urban Ladder',
        jobTitle: 'Developer',
        jobType: 'Full Time',
        candidateCity: 'Noida',
        candidateCountry: 'India',
        jobPostStatus: 'ACTIVE',
        jobCategory: 'IT',

        // salary
        salaryMin: '3',
        salaryMax: '5',
        salaryFigureType: 'INR',
        salaryType: 'k',

        // experience
        experienceMin: '3',
        experienceMax: '5',
        experienceType: 'years',

        // status
        appliedDate: '13-06-2018',
        applicationStatus: 'In Process',
        isFavourite: true,
        isMatching: true,

        // skills
        skills: ['C', 'Java', 'Angular', 'HTML', 'CSS']
    }
];
