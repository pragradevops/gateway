export const MATCHING_CANDIDATES = [
    {
        candidateUrl: 'https://seeklogo.com/images/F/flipkart-logo-3F33927DAA-seeklogo.com.png',
        candidateName: 'Husain',
        jobTitle: 'Developer',
        jobType: 'Full Time',
        candidateCity: 'Noida',
        candidateCountry: 'India',
        jobPostStatus: 'ACTIVE',
        jobCategory: 'IT',

        // salary
        salaryMin: '3',
        salaryMax: '5',
        salaryFigureType: 'INR',
        salaryType: 'LPA',

        // experience
        experienceMin: '3',
        experienceMax: '5',
        experienceType: 'years',

        // status
        appliedDate: '13-06-2018',
        applicationStatus: 'In Process',
        isFavourite: true,
        isMatching: true,

        // skills
        skills: ['C', 'Java', 'Angular', 'HTML', 'CSS']
    },
    {
        candidateUrl: 'https://seeklogo.com/images/F/flipkart-logo-3F33927DAA-seeklogo.com.png',
        candidateName: 'Maskoor',
        jobTitle: 'Deisgner',
        jobType: 'Full Time',
        candidateCity: 'Ghaziabad',
        candidateCountry: 'India',
        jobPostStatus: 'ACTIVE',
        jobCategory: 'IT',

        // salary
        salaryMin: '3',
        salaryMax: '5',
        salaryFigureType: 'INR',
        salaryType: 'LPA',

        // experience
        experienceMin: '3',
        experienceMax: '5',
        experienceType: 'years',

        // status
        appliedDate: '13-06-2018',
        applicationStatus: 'In Process',
        isFavourite: true,
        isMatching: true,

        // skills
        skills: ['C', 'Java', 'Angular', 'HTML', 'CSS']
    },
    {
        candidateUrl: 'https://seeklogo.com/images/F/flipkart-logo-3F33927DAA-seeklogo.com.png',
        candidateName: 'Himanshu',
        jobTitle: 'Developer',
        jobType: 'Over Time',
        candidateCity: 'Lahore',
        candidateCountry: 'Pakistan',
        jobPostStatus: 'ACTIVE',
        jobCategory: 'IT',

        // salary
        salaryMin: '3',
        salaryMax: '5',
        salaryFigureType: 'INR',
        salaryType: 'LPA',

        // experience
        experienceMin: '3',
        experienceMax: '5',
        experienceType: 'years',

        // status
        appliedDate: '13-06-2018',
        applicationStatus: 'In Process',
        isFavourite: true,
        isMatching: true,

        // skills
        skills: ['C', 'Java', 'Angular', 'HTML', 'CSS']
    },
    {
        candidateUrl: 'https://seeklogo.com/images/F/flipkart-logo-3F33927DAA-seeklogo.com.png',
        candidateName: 'Ashutosh',
        jobTitle: 'Java Developer',
        jobType: 'Full Time',
        candidateCity: 'Noida',
        candidateCountry: 'India',
        jobPostStatus: 'ACTIVE',
        jobCategory: 'IT',

        // salary
        salaryMin: '3',
        salaryMax: '5',
        salaryFigureType: 'INR',
        salaryType: 'LPA',

        // experience
        experienceMin: '3',
        experienceMax: '5',
        experienceType: 'years',

        // status
        appliedDate: '13-06-2018',
        applicationStatus: 'In Process',
        isFavourite: true,
        isMatching: true,

        // skills
        skills: ['C', 'Java', 'Angular', 'HTML', 'CSS']
    },
    {
        logoUrl: 'https://seeklogo.com/images/F/flipkart-logo-3F33927DAA-seeklogo.com.png',
        candidateName: 'Neha',
        jobTitle: 'Hacker',
        jobType: 'Full Time',
        candidateCity: 'Australia',
        candidateCountry: 'USA',
        jobPostStatus: 'ACTIVE',
        jobCategory: 'IT',

        // salary
        salaryMin: '3',
        salaryMax: '5',
        salaryFigureType: 'INR',
        salaryType: 'LPA',

        // experience
        experienceMin: '3',
        experienceMax: '5',
        experienceType: 'years',

        // status
        appliedDate: '13-06-2018',
        applicationStatus: 'In Process',
        isFavourite: true,
        isMatching: true,

        // skills
        skills: ['C', 'Java', 'Angular', 'HTML', 'CSS']
    },
    {
        logoUrl: 'https://seeklogo.com/images/F/flipkart-logo-3F33927DAA-seeklogo.com.png',
        candidateName: 'Urban Ladder',
        jobTitle: 'Developer',
        jobType: 'Full Time',
        candidateCity: 'Noida',
        candidateCountry: 'India',
        jobPostStatus: 'ACTIVE',
        jobCategory: 'IT',

        // salary
        salaryMin: '3',
        salaryMax: '5',
        salaryFigureType: 'INR',
        salaryType: 'LPA',

        // experience
        experienceMin: '3',
        experienceMax: '5',
        experienceType: 'years',

        // status
        appliedDate: '13-06-2018',
        applicationStatus: 'In Process',
        isFavourite: true,
        isMatching: true,

        // skills
        skills: ['C', 'Java', 'Angular', 'HTML', 'CSS']
    },
    {
        logoUrl: 'https://seeklogo.com/images/F/flipkart-logo-3F33927DAA-seeklogo.com.png',
        candidateName: 'Urban Ladder',
        jobTitle: 'Developer',
        jobType: 'Full Time',
        candidateCity: 'Noida',
        candidateCountry: 'India',
        jobPostStatus: 'ACTIVE',
        jobCategory: 'IT',

        // salary
        salaryMin: '3',
        salaryMax: '5',
        salaryFigureType: 'INR',
        salaryType: 'LPA',

        // experience
        experienceMin: '3',
        experienceMax: '5',
        experienceType: 'years',

        // status
        appliedDate: '13-06-2018',
        applicationStatus: 'In Process',
        isFavourite: true,
        isMatching: true,

        // skills
        skills: ['C', 'Java', 'Angular', 'HTML', 'CSS']
    },
    {
        logoUrl: 'https://seeklogo.com/images/F/flipkart-logo-3F33927DAA-seeklogo.com.png',
        candidateName: 'Urban Ladder',
        jobTitle: 'Developer',
        jobType: 'Full Time',
        candidateCity: 'Noida',
        candidateCountry: 'India',
        jobPostStatus: 'ACTIVE',
        jobCategory: 'IT',

        // salary
        salaryMin: '3',
        salaryMax: '5',
        salaryFigureType: 'INR',
        salaryType: 'LPA',

        // experience
        experienceMin: '3',
        experienceMax: '5',
        experienceType: 'years',

        // status
        appliedDate: '13-06-2018',
        applicationStatus: 'In Process',
        isFavourite: true,
        isMatching: true,

        // skills
        skills: ['C', 'Java', 'Angular', 'HTML', 'CSS']
    },
    {
        logoUrl: 'https://seeklogo.com/images/F/flipkart-logo-3F33927DAA-seeklogo.com.png',
        candidateName: 'Urban Ladder',
        jobTitle: 'Developer',
        jobType: 'Full Time',
        candidateCity: 'Noida',
        candidateCountry: 'India',
        jobPostStatus: 'ACTIVE',
        jobCategory: 'IT',

        // salary
        salaryMin: '3',
        salaryMax: '5',
        salaryFigureType: 'INR',
        salaryType: 'LPA',

        // experience
        experienceMin: '3',
        experienceMax: '5',
        experienceType: 'years',

        // status
        appliedDate: '13-06-2018',
        applicationStatus: 'In Process',
        isFavourite: true,
        isMatching: true,

        // skills
        skills: ['C', 'Java', 'Angular', 'HTML', 'CSS']
    },
    {
        logoUrl: 'https://seeklogo.com/images/F/flipkart-logo-3F33927DAA-seeklogo.com.png',
        candidateName: 'Urban Ladder',
        jobTitle: 'Developer',
        jobType: 'Full Time',
        candidateCity: 'Noida',
        candidateCountry: 'India',
        jobPostStatus: 'ACTIVE',
        jobCategory: 'IT',

        // salary
        salaryMin: '3',
        salaryMax: '5',
        salaryFigureType: 'INR',
        salaryType: 'LPA',

        // experience
        experienceMin: '3',
        experienceMax: '5',
        experienceType: 'years',

        // status
        appliedDate: '13-06-2018',
        applicationStatus: 'In Process',
        isFavourite: true,
        isMatching: true,

        // skills
        skills: ['C', 'Java', 'Angular', 'HTML', 'CSS']
    }
];
