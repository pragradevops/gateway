export const MATCHING_JOBS = [
    {
        logoUrl: 'https://seeklogo.com/images/F/flipkart-logo-3F33927DAA-seeklogo.com.png',
        employerName: 'Genpact ',
        jobTitle: 'Java Developer',
        jobType: 'Full Time',
        jobCity: 'Noida',
        jobCountry: 'India',
        jobPostStatus: 'ACTIVE',
        jobCategory: 'IT',

        // salary
        salaryMin: '3',
        salaryMax: '5',
        salaryFigureType: 'INR',
        salaryType: 'LPA',

        // experience
        experienceMin: '3',
        experienceMax: '5',
        experienceType: 'years',

        // status
        appliedDate: '13-06-2018',
        applicationStatus: 'In Process',
        isFavourite: true,
        isMatching: true,

         // job description
       jobDescription:
       'Being part of the HR requires you to have in-depth knowledge about the latest and proven practises' +
       'in human resource management. This is a crucial role as we are a a leading media company and require' +
       'the HR function to be the catalyst for improving performance, collaboration and effective communication.' +
       'What you will do-All day-to-day tasks required for HR compliance-Organisational development and effectiveness' +
       '-Building policies and regulations to maintain high standards of performance,-Recruitment and orientation/on boarding of employees',
       // job requirements
       jobRequirements: {
           description:
           'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.' +
           'The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, making it look' +
           'like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and' +
           'a search for will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident,' +
           'sometimes on purpose (injected humour and the like).',
           requirements: [
               'Guide and direct all facets of Human Resources including recruitment, compensation & benefits, performance management & succession planning.',
               'Lead and oversee the day to day operation of the Human Resources Department as well as play a critical ',
               'role in assisting in the implementation of the HR strategy. Assist in assessing organizational needs, developing aligned human resource ',
               'solutions and implement strategies in the areas of employee relations. Ensure compliance of all legal requirements & professional requirements.'
           ]
       },
       // job benifits
       jobBenifits: {
           description:
           'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.' +
           'The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, making it look like readable English.' +
           'Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for will uncover many web sites' +
           ' still in their infancy.Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',
           benifits: ['Meal Coupon', 'Medical Insurance']
       },
        // skills
        skills: ['C', 'Java', 'Angular', 'HTML', 'CSS']
    },
    {
        logoUrl: 'https://seeklogo.com/images/F/flipkart-logo-3F33927DAA-seeklogo.com.png',
        employerName: 'Genpact ',
        jobTitle: 'Java Developer',
        jobType: 'Full Time',
        jobCity: 'Noida',
        jobCountry: 'India',
        jobPostStatus: 'ACTIVE',
        jobCategory: 'IT',

        // salary
        salaryMin: '3',
        salaryMax: '5',
        salaryFigureType: 'INR',
        salaryType: 'LPA',

        // experience
        experienceMin: '3',
        experienceMax: '5',
        experienceType: 'years',

        // status
        appliedDate: '13-06-2018',
        applicationStatus: 'In Process',
        isFavourite: true,
        isMatching: true,

         // job description
       jobDescription:
       'Being part of the HR requires you to have in-depth knowledge about the latest and proven practises' +
       'in human resource management. This is a crucial role as we are a a leading media company and require' +
       'the HR function to be the catalyst for improving performance, collaboration and effective communication.' +
       'What you will do-All day-to-day tasks required for HR compliance-Organisational development and effectiveness' +
       '-Building policies and regulations to maintain high standards of performance,-Recruitment and orientation/on boarding of employees',
       // job requirements
       jobRequirements: {
           description:
           'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.' +
           'The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, making it look' +
           'like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and' +
           'a search for will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident,' +
           'sometimes on purpose (injected humour and the like).',
           requirements: [
               'Guide and direct all facets of Human Resources including recruitment, compensation & benefits, performance management & succession planning.',
               'Lead and oversee the day to day operation of the Human Resources Department as well as play a critical ',
               'role in assisting in the implementation of the HR strategy. Assist in assessing organizational needs, developing aligned human resource ',
               'solutions and implement strategies in the areas of employee relations. Ensure compliance of all legal requirements & professional requirements.'
           ]
       },
       // job benifits
       jobBenifits: {
           description:
           'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.' +
           'The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, making it look like readable English.' +
           'Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for will uncover many web sites' +
           ' still in their infancy.Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',
           benifits: ['Meal Coupon', 'Medical Insurance']
       },
        // skills
        skills: ['C', 'Java', 'Angular', 'HTML', 'CSS']
    },
    {
        logoUrl: 'https://seeklogo.com/images/F/flipkart-logo-3F33927DAA-seeklogo.com.png',
        employerName: 'Genpact ',
        jobTitle: 'Java Developer',
        jobType: 'Full Time',
        jobCity: 'Noida',
        jobCountry: 'India',
        jobPostStatus: 'ACTIVE',
        jobCategory: 'IT',

        // salary
        salaryMin: '3',
        salaryMax: '5',
        salaryFigureType: 'INR',
        salaryType: 'LPA',

        // experience
        experienceMin: '3',
        experienceMax: '5',
        experienceType: 'years',

        // status
        appliedDate: '13-06-2018',
        applicationStatus: 'In Process',
        isFavourite: true,
        isMatching: true,

         // job description
       jobDescription:
       'Being part of the HR requires you to have in-depth knowledge about the latest and proven practises' +
       'in human resource management. This is a crucial role as we are a a leading media company and require' +
       'the HR function to be the catalyst for improving performance, collaboration and effective communication.' +
       'What you will do-All day-to-day tasks required for HR compliance-Organisational development and effectiveness' +
       '-Building policies and regulations to maintain high standards of performance,-Recruitment and orientation/on boarding of employees',
       // job requirements
       jobRequirements: {
           description:
           'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.' +
           'The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, making it look' +
           'like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and' +
           'a search for will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident,' +
           'sometimes on purpose (injected humour and the like).',
           requirements: [
               'Guide and direct all facets of Human Resources including recruitment, compensation & benefits, performance management & succession planning.',
               'Lead and oversee the day to day operation of the Human Resources Department as well as play a critical ',
               'role in assisting in the implementation of the HR strategy. Assist in assessing organizational needs, developing aligned human resource ',
               'solutions and implement strategies in the areas of employee relations. Ensure compliance of all legal requirements & professional requirements.'
           ]
       },
       // job benifits
       jobBenifits: {
           description:
           'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.' +
           'The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, making it look like readable English.' +
           'Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for will uncover many web sites' +
           ' still in their infancy.Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',
           benifits: ['Meal Coupon', 'Medical Insurance']
       },
        // skills
        skills: ['C', 'Java', 'Angular', 'HTML', 'CSS']
    },
    {
        logoUrl: 'https://seeklogo.com/images/F/flipkart-logo-3F33927DAA-seeklogo.com.png',
        employerName: 'Genpact ',
        jobTitle: 'Java Developer',
        jobType: 'Full Time',
        jobCity: 'Noida',
        jobCountry: 'India',
        jobPostStatus: 'ACTIVE',
        jobCategory: 'IT',

        // salary
        salaryMin: '3',
        salaryMax: '5',
        salaryFigureType: 'INR',
        salaryType: 'LPA',

        // experience
        experienceMin: '3',
        experienceMax: '5',
        experienceType: 'years',

        // status
        appliedDate: '13-06-2018',
        applicationStatus: 'In Process',
        isFavourite: true,
        isMatching: true,

         // job description
       jobDescription:
       'Being part of the HR requires you to have in-depth knowledge about the latest and proven practises' +
       'in human resource management. This is a crucial role as we are a a leading media company and require' +
       'the HR function to be the catalyst for improving performance, collaboration and effective communication.' +
       'What you will do-All day-to-day tasks required for HR compliance-Organisational development and effectiveness' +
       '-Building policies and regulations to maintain high standards of performance,-Recruitment and orientation/on boarding of employees',
       // job requirements
       jobRequirements: {
           description:
           'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.' +
           'The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, making it look' +
           'like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and' +
           'a search for will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident,' +
           'sometimes on purpose (injected humour and the like).',
           requirements: [
               'Guide and direct all facets of Human Resources including recruitment, compensation & benefits, performance management & succession planning.',
               'Lead and oversee the day to day operation of the Human Resources Department as well as play a critical ',
               'role in assisting in the implementation of the HR strategy. Assist in assessing organizational needs, developing aligned human resource ',
               'solutions and implement strategies in the areas of employee relations. Ensure compliance of all legal requirements & professional requirements.'
           ]
       },
       // job benifits
       jobBenifits: {
           description:
           'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.' +
           'The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, making it look like readable English.' +
           'Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for will uncover many web sites' +
           ' still in their infancy.Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',
           benifits: ['Meal Coupon', 'Medical Insurance']
       },
        // skills
        skills: ['C', 'Java', 'Angular', 'HTML', 'CSS']
    },
    {
        logoUrl: 'https://seeklogo.com/images/F/flipkart-logo-3F33927DAA-seeklogo.com.png',
        employerName: 'Genpact ',
        jobTitle: 'Java Developer',
        jobType: 'Full Time',
        jobCity: 'Noida',
        jobCountry: 'India',
        jobPostStatus: 'ACTIVE',
        jobCategory: 'IT',

        // salary
        salaryMin: '3',
        salaryMax: '5',
        salaryFigureType: 'INR',
        salaryType: 'LPA',

        // experience
        experienceMin: '3',
        experienceMax: '5',
        experienceType: 'years',

        // status
        appliedDate: '13-06-2018',
        applicationStatus: 'In Process',
        isFavourite: true,
        isMatching: true,

        // job description
        jobDescription:
        'Being part of the HR requires you to have in-depth knowledge about the latest and proven practises' +
        'in human resource management. This is a crucial role as we are a a leading media company and require' +
        'the HR function to be the catalyst for improving performance, collaboration and effective communication.' +
        'What you will do-All day-to-day tasks required for HR compliance-Organisational development and effectiveness' +
        '-Building policies and regulations to maintain high standards of performance,-Recruitment and orientation/on boarding of employees',
        // job requirements
        jobRequirements: {
            description:
            'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.' +
            'The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, making it look' +
            'like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and' +
            'a search for will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident,' +
            'sometimes on purpose (injected humour and the like).',
            requirements: [
                'Guide and direct all facets of Human Resources including recruitment, compensation & benefits, performance management & succession planning.',
                'Lead and oversee the day to day operation of the Human Resources Department as well as play a critical ',
                'role in assisting in the implementation of the HR strategy. Assist in assessing organizational needs, developing aligned human resource ',
                'solutions and implement strategies in the areas of employee relations. Ensure compliance of all legal requirements & professional requirements.'
            ]
        },
        // job benifits
        jobBenifits: {
            description:
            'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.' +
            'The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, making it look like readable English.' +
            'Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for will uncover many web sites' +
            ' still in their infancy.Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',
            benifits: ['Meal Coupon', 'Medical Insurance']
        },
        // skills
        skills: ['C', 'Java', 'Angular', 'HTML', 'CSS']
    },
    {
        logoUrl: 'https://seeklogo.com/images/F/flipkart-logo-3F33927DAA-seeklogo.com.png',
        employerName: 'Genpact ',
        jobTitle: 'Java Developer',
        jobType: 'Full Time',
        jobCity: 'Noida',
        jobCountry: 'India',
        jobPostStatus: 'ACTIVE',
        jobCategory: 'IT',

        // salary
        salaryMin: '3',
        salaryMax: '5',
        salaryFigureType: 'INR',
        salaryType: 'LPA',

        // experience
        experienceMin: '3',
        experienceMax: '5',
        experienceType: 'years',

        // status
        appliedDate: '13-06-2018',
        applicationStatus: 'In Process',
        isFavourite: true,
        isMatching: true,

        // job description
        jobDescription:
        'Being part of the HR requires you to have in-depth knowledge about the latest and proven practises' +
        'in human resource management. This is a crucial role as we are a a leading media company and require' +
        'the HR function to be the catalyst for improving performance, collaboration and effective communication.' +
        'What you will do-All day-to-day tasks required for HR compliance-Organisational development and effectiveness' +
        '-Building policies and regulations to maintain high standards of performance,-Recruitment and orientation/on boarding of employees',
        // job requirements
        jobRequirements: {
            description:
            'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.' +
            'The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, making it look' +
            'like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and' +
            'a search for will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident,' +
            'sometimes on purpose (injected humour and the like).',
            requirements: [
                'Guide and direct all facets of Human Resources including recruitment, compensation & benefits, performance management & succession planning.',
                'Lead and oversee the day to day operation of the Human Resources Department as well as play a critical ',
                'role in assisting in the implementation of the HR strategy. Assist in assessing organizational needs, developing aligned human resource ',
                'solutions and implement strategies in the areas of employee relations. Ensure compliance of all legal requirements & professional requirements.'
            ]
        },
        // job benifits
        jobBenifits: {
            description:
            'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.' +
            'The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, making it look like readable English.' +
            'Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for will uncover many web sites' +
            ' still in their infancy.Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',
            benifits: ['Meal Coupon', 'Medical Insurance']
        },
        // skills
        skills: ['C', 'Java', 'Angular', 'HTML', 'CSS']
    },
    {
        logoUrl: 'https://seeklogo.com/images/F/flipkart-logo-3F33927DAA-seeklogo.com.png',
        employerName: 'Genpact ',
        jobTitle: 'Java Developer',
        jobType: 'Full Time',
        jobCity: 'Noida',
        jobCountry: 'India',
        jobPostStatus: 'ACTIVE',
        jobCategory: 'IT',

        // salary
        salaryMin: '3',
        salaryMax: '5',
        salaryFigureType: 'INR',
        salaryType: 'LPA',

        // experience
        experienceMin: '3',
        experienceMax: '5',
        experienceType: 'years',

        // status
        appliedDate: '13-06-2018',
        applicationStatus: 'In Process',
        isFavourite: true,
        isMatching: true,

         // job description
       jobDescription:
       'Being part of the HR requires you to have in-depth knowledge about the latest and proven practises' +
       'in human resource management. This is a crucial role as we are a a leading media company and require' +
       'the HR function to be the catalyst for improving performance, collaboration and effective communication.' +
       'What you will do-All day-to-day tasks required for HR compliance-Organisational development and effectiveness' +
       '-Building policies and regulations to maintain high standards of performance,-Recruitment and orientation/on boarding of employees',
       // job requirements
       jobRequirements: {
           description:
           'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.' +
           'The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, making it look' +
           'like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and' +
           'a search for will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident,' +
           'sometimes on purpose (injected humour and the like).',
           requirements: [
               'Guide and direct all facets of Human Resources including recruitment, compensation & benefits, performance management & succession planning.',
               'Lead and oversee the day to day operation of the Human Resources Department as well as play a critical ',
               'role in assisting in the implementation of the HR strategy. Assist in assessing organizational needs, developing aligned human resource ',
               'solutions and implement strategies in the areas of employee relations. Ensure compliance of all legal requirements & professional requirements.'
           ]
       },
       // job benifits
       jobBenifits: {
           description:
           'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.' +
           'The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, making it look like readable English.' +
           'Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for will uncover many web sites' +
           ' still in their infancy.Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',
           benifits: ['Meal Coupon', 'Medical Insurance']
       },
        // skills
        skills: ['C', 'Java', 'Angular', 'HTML', 'CSS']
    },
    {
        logoUrl: 'https://seeklogo.com/images/F/flipkart-logo-3F33927DAA-seeklogo.com.png',
        employerName: 'Genpact ',
        jobTitle: 'Java Developer',
        jobType: 'Full Time',
        jobCity: 'Noida',
        jobCountry: 'India',
        jobPostStatus: 'ACTIVE',
        jobCategory: 'IT',

        // salary
        salaryMin: '3',
        salaryMax: '5',
        salaryFigureType: 'INR',
        salaryType: 'LPA',

        // experience
        experienceMin: '3',
        experienceMax: '5',
        experienceType: 'years',

        // status
        appliedDate: '13-06-2018',
        applicationStatus: 'In Process',
        isFavourite: true,
        isMatching: true,

        // job description
        jobDescription:
        'Being part of the HR requires you to have in-depth knowledge about the latest and proven practises' +
        'in human resource management. This is a crucial role as we are a a leading media company and require' +
        'the HR function to be the catalyst for improving performance, collaboration and effective communication.' +
        'What you will do-All day-to-day tasks required for HR compliance-Organisational development and effectiveness' +
        '-Building policies and regulations to maintain high standards of performance,-Recruitment and orientation/on boarding of employees',
        // job requirements
        jobRequirements: {
            description:
            'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.' +
            'The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, making it look' +
            'like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and' +
            'a search for will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident,' +
            'sometimes on purpose (injected humour and the like).',
            requirements: [
                'Guide and direct all facets of Human Resources including recruitment, compensation & benefits, performance management & succession planning.',
                'Lead and oversee the day to day operation of the Human Resources Department as well as play a critical ',
                'role in assisting in the implementation of the HR strategy. Assist in assessing organizational needs, developing aligned human resource ',
                'solutions and implement strategies in the areas of employee relations. Ensure compliance of all legal requirements & professional requirements.'
            ]
        },
        // job benifits
        jobBenifits: {
            description:
            'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.' +
            'The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, making it look like readable English.' +
            'Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for will uncover many web sites' +
            ' still in their infancy.Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',
            benifits: ['Meal Coupon', 'Medical Insurance']
        },
        // skills
        skills: ['C', 'Java', 'Angular', 'HTML', 'CSS']
    },
    {
        logoUrl: 'https://seeklogo.com/images/F/flipkart-logo-3F33927DAA-seeklogo.com.png',
        employerName: 'Genpact ',
        jobTitle: 'Java Developer',
        jobType: 'Full Time',
        jobCity: 'Noida',
        jobCountry: 'India',
        jobPostStatus: 'ACTIVE',
        jobCategory: 'IT',

        // salary
        salaryMin: '3',
        salaryMax: '5',
        salaryFigureType: 'INR',
        salaryType: 'LPA',

        // experience
        experienceMin: '3',
        experienceMax: '5',
        experienceType: 'years',

        // status
        appliedDate: '13-06-2018',
        applicationStatus: 'In Process',
        isFavourite: true,
        isMatching: true,

         // job description
       jobDescription:
       'Being part of the HR requires you to have in-depth knowledge about the latest and proven practises' +
       'in human resource management. This is a crucial role as we are a a leading media company and require' +
       'the HR function to be the catalyst for improving performance, collaboration and effective communication.' +
       'What you will do-All day-to-day tasks required for HR compliance-Organisational development and effectiveness' +
       '-Building policies and regulations to maintain high standards of performance,-Recruitment and orientation/on boarding of employees',
       // job requirements
       jobRequirements: {
           description:
           'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.' +
           'The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, making it look' +
           'like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and' +
           'a search for will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident,' +
           'sometimes on purpose (injected humour and the like).',
           requirements: [
               'Guide and direct all facets of Human Resources including recruitment, compensation & benefits, performance management & succession planning.',
               'Lead and oversee the day to day operation of the Human Resources Department as well as play a critical ',
               'role in assisting in the implementation of the HR strategy. Assist in assessing organizational needs, developing aligned human resource ',
               'solutions and implement strategies in the areas of employee relations. Ensure compliance of all legal requirements & professional requirements.'
           ]
       },
       // job benifits
       jobBenifits: {
           description:
           'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.' +
           'The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, making it look like readable English.' +
           'Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for will uncover many web sites' +
           ' still in their infancy.Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',
           benifits: ['Meal Coupon', 'Medical Insurance']
       },
        // skills
        skills: ['C', 'Java', 'Angular', 'HTML', 'CSS']
    },
    {
        logoUrl: 'https://seeklogo.com/images/F/flipkart-logo-3F33927DAA-seeklogo.com.png',
        employerName: 'Genpact ',
        jobTitle: 'Java Developer',
        jobType: 'Full Time',
        jobCity: 'Noida',
        jobCountry: 'India',
        jobPostStatus: 'ACTIVE',
        jobCategory: 'IT',

        // salary
        salaryMin: '3',
        salaryMax: '5',
        salaryFigureType: 'INR',
        salaryType: 'LPA',

        // experience
        experienceMin: '3',
        experienceMax: '5',
        experienceType: 'years',

        // status
        appliedDate: '13-06-2018',
        applicationStatus: 'In Process',
        isFavourite: true,
        isMatching: true,

         // job description
       jobDescription:
       'Being part of the HR requires you to have in-depth knowledge about the latest and proven practises' +
       'in human resource management. This is a crucial role as we are a a leading media company and require' +
       'the HR function to be the catalyst for improving performance, collaboration and effective communication.' +
       'What you will do-All day-to-day tasks required for HR compliance-Organisational development and effectiveness' +
       '-Building policies and regulations to maintain high standards of performance,-Recruitment and orientation/on boarding of employees',
       // job requirements
       jobRequirements: {
           description:
           'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.' +
           'The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, making it look' +
           'like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and' +
           'a search for will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident,' +
           'sometimes on purpose (injected humour and the like).',
           requirements: [
               'Guide and direct all facets of Human Resources including recruitment, compensation & benefits, performance management & succession planning.',
               'Lead and oversee the day to day operation of the Human Resources Department as well as play a critical ',
               'role in assisting in the implementation of the HR strategy. Assist in assessing organizational needs, developing aligned human resource ',
               'solutions and implement strategies in the areas of employee relations. Ensure compliance of all legal requirements & professional requirements.'
           ]
       },
       // job benifits
       jobBenifits: {
           description:
           'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.' +
           'The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, making it look like readable English.' +
           'Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for will uncover many web sites' +
           ' still in their infancy.Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',
           benifits: ['Meal Coupon', 'Medical Insurance']
       },
        // skills
        skills: ['C', 'Java', 'Angular', 'HTML', 'CSS']
    }
];
