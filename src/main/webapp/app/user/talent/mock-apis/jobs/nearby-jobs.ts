export const NEARBY_JOBS: any = [
    {
        latLong: {
            lat: 28.599608,
            lng: 77.3825164
        },
        companyName: 'Aleeph Advisors',
        address: 'Suite Number: 205, H-160, Sector 63',
        location: ' Noida, UP, India',
        designation: 'Software Developer'
    },
    {
        latLong: {
            lat: 28.5996,
            lng: 77.3788704
        },
        companyName: 'Google Inc.',
        address: 'Suite Number: 205, H-160, Sector 63',
        location: ' Noida, UP, India',
        designation: 'Software Developer'
    },
    {
        latLong: {
            lat: 28.599,
            lng: 77.3788704
        },
        companyName: 'Amazon India',
        address: 'Suite Number: 205, H-160, Sector 63',
        location: ' Noida, UP, India',
        designation: 'Software Developer'
    }
];

export const NEARBY_CANDIDATES: any = [
    {
        latLong: {
            lat: 28.599608,
            lng: 77.3825164
        },
        candidateName: 'Sandeep Bisht',
        address: 'Suite Number: 205, H-160, Sector 63',
        location: 'Saket, New Delhi',
        designation: 'Developer (Full Time)'
    },
    {
        latLong: {
            lat: 28.5996,
            lng: 77.3788704
        },
        candidateName: 'Somya Banerji',
        address: 'Suite Number: 205, H-160, Sector 63',
        location: 'Naraina, New Delhi',
        designation: 'Developer (Full Time)'
    },
    {
        latLong: {
            lat: 28.599,
            lng: 77.3788704
        },
        candidateName: 'Lalit Saxena',
        address: 'Suite Number: 205, H-160, Sector 63',
        location: 'Noida, UP, India',
        designation: 'Developer (Full Time)'
    }
];
