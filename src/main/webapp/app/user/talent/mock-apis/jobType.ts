export const JOB_TYPE: any[] = [
    { 'name': 'Full Time', 'value': 'FULL_TIME' },
    { 'name': 'Part Time', 'value': 'PART_TIME' },
    { 'name': 'Contract', 'value': 'CONTRACT' },
    { 'name': 'Internship', 'value': 'INTERNSHIP' },
    { 'name': 'Volunteer', 'value': 'VOLUNTEER' }
];
