export const HIRENG_RATINGS: any = [
    {
        'scoreHireNG': 7.5,
        'isSocialRating': true,
        'isBehaviouralRating': true,
        'isEmployeeRating': false,
        'socialRating': 1.7,
        'behaviouralRating': 2.5,
        'employeeRating': null
    }
];
