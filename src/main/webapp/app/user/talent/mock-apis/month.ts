export const MONTH: any[] = [
    { 'name': 'January', 'value': '01' },
    { 'name': 'February', 'value': '02' },
    { 'name': 'March', 'value': '03' },
    { 'name': 'April', 'value': '04' },
    { 'name': 'May', 'value': '05' },
    { 'name': 'June', 'value': '06' },
    { 'name': 'July', 'value': '07' },
    { 'name': 'August', 'value': '08' },
    { 'name': 'September', 'value': '09' },
    { 'name': 'October', 'value': '10' },
    { 'name': 'November', 'value': '11' },
    { 'name': 'December', 'value': '12' }
];

export const MONTH_NAME: any[] = [
    { 'name': 'January', 'value': 'January' },
    { 'name': 'February', 'value': 'February' },
    { 'name': 'March', 'value': 'March' },
    { 'name': 'April', 'value': 'April' },
    { 'name': 'May', 'value': 'May' },
    { 'name': 'June', 'value': 'June' },
    { 'name': 'July', 'value': 'July' },
    { 'name': 'August', 'value': 'August' },
    { 'name': 'September', 'value': 'September' },
    { 'name': 'October', 'value': 'October' },
    { 'name': 'November', 'value': 'November' },
    { 'name': 'December', 'value': 'December' }
];

export const YEAR: any[] = [
    { 'name': '2018', 'value': '2018' },
    { 'name': '2017', 'value': '2017' },
    { 'name': '2016', 'value': '2016' },
    { 'name': '2015', 'value': '2015' },
    { 'name': '2014', 'value': '2014' },
    { 'name': '2013', 'value': '2013' },
    { 'name': '2012', 'value': '2012' },
    { 'name': '2011', 'value': '2011' },
    { 'name': '2010', 'value': '2010' },
    { 'name': '2009', 'value': '2009' },
    { 'name': '2008', 'value': '2008' },
    { 'name': '2007', 'value': '2007' },
    { 'name': '2006', 'value': '2006' },
    { 'name': '2005', 'value': '2005' },
    { 'name': '2004', 'value': '2004' },
    { 'name': '2003', 'value': '2003' },
    { 'name': '2002', 'value': '2002' },
    { 'name': '2001', 'value': '2001' },
    { 'name': '2000', 'value': '2000' },
    { 'name': '1999', 'value': '1999' },
    { 'name': '1998', 'value': '1998' },
    { 'name': '1997', 'value': '1997' },
    { 'name': '1996', 'value': '1996' },
    { 'name': '1995', 'value': '1995' },
    { 'name': '1994', 'value': '1994' },
    { 'name': '1993', 'value': '1993' },
    { 'name': '1992', 'value': '1992' },
    { 'name': '1991', 'value': '1991' },
    { 'name': '1990', 'value': '1990' },
];
