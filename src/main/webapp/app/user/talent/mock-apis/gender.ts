export const GENDER: any[] = [
    { 'name': 'Male', 'value': 'MALE' },
    { 'name': 'Female', 'value': 'FEMALE' },
    { 'name': 'Others', 'value': 'OTHERS' }
];
