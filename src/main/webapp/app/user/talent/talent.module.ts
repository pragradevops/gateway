import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { talentRoute } from './talent.route';

// HireNG Modules
import { DashboardModule } from './dashboard/dashboard.module';
import { JobsModule } from './jobs/jobs.module';
import { InterviewModule } from './interviews/interview.module';
import { ProfileModule } from './profile/profile.module';
import { ModalModule } from './modal/modal.module';
import { SharedModule } from '../shared/shared.module';

// Component
import { TalentComponent } from './talent.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(talentRoute),
        NgbModule.forRoot(),
        DashboardModule,
        JobsModule,
        InterviewModule,
        ProfileModule,
        SharedModule,
        ModalModule
    ],
    declarations: [TalentComponent]
})
export class TalentModule {}
