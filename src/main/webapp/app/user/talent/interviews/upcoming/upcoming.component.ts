import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { UpcomingInterviewService } from '../../service/interview/upcoming-interview.service';
// import { PaginationService } from '../../service/pagination.service';
import { ModalService } from '../../service/modal.service';

// Model classes
import { UpcomingInterview } from '../../model/interviews/upcoming';
// componets
import { AcceptModalComponent, CancelModalComponent, RescheduleModalComponent } from '../../modal/interview';
declare let $: any;
@Component({
    selector: 'jhi-upcoming',
    templateUrl: './upcoming.component.html',
    styleUrls: ['./upcoming.scss']
})
export class UpcomingComponent implements OnInit {
    upcomingInterviews: UpcomingInterview[];
    upcomingInterviewsFirst: any = {};
    modalRef: NgbModalRef;
    constructor(private modalService: ModalService, private upcomingInterviewService: UpcomingInterviewService) {}
    // pager object
    // pager: any = {};

    // paged items
    // pagedUpcomingInterviews: any[];

    // constructor(private upcomingInterviewService: UpcomingInterviewService, private paginationService: PaginationService) { }

    ngOnInit() {
        this.getUpcomingInterviews();
        // this.setPage(1);
        $(document).ready(function() {
            $('.info .reminder').click(function() {
                $(this).toggleClass('active');
            });
        });
    }

    getUpcomingInterviews(): void {
        this.upcomingInterviews = this.upcomingInterviewService.getUpcomingInterviews();
        this.upcomingInterviewsFirst = this.upcomingInterviews.slice(0, 1);
    }

    // setPage(page: number) {
    //   // get pager object from service
    //   this.pager = this.paginationService.getPager(this.upcomingInterviews.length, page);
    //   // get current page of items
    //   this.pagedUpcomingInterviews = this.upcomingInterviews.slice(this.pager.startIndex, this.pager.endIndex + 1);
    // }
    openCancel() {
        this.modalRef = this.modalService.open(CancelModalComponent);
    }
    openReschedule() {
        this.modalRef = this.modalService.open(RescheduleModalComponent);
    }
}
