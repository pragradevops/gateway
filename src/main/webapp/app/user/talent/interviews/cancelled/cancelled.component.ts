import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
// services
import { CancelledInterviewService } from '../../service/interview/cancelled-interview.service';
import { ModalService } from '../../service/modal.service';
// model
import { Cancelled } from '../../model/interviews/cancelled';
// components
import { RescheduleModalComponent } from '../../modal/interview';

@Component({
    selector: 'jhi-cancelled',
    templateUrl: './cancelled.component.html',
    styleUrls: ['./cancelled.scss']
})
export class CancelledComponent implements OnInit {
    cancelledInterviews: Cancelled[];
    cancelledInterviewsFirst: any = {};

    modalRef: NgbModalRef;
    constructor(private modalService: ModalService, private cancelledInterviewService: CancelledInterviewService) {}

    ngOnInit() {
        this.getCancelledInterviews();
    }
    getCancelledInterviews(): void {
        this.cancelledInterviews = this.cancelledInterviewService.getCancelledInterviews();
        this.cancelledInterviewsFirst = this.cancelledInterviews.slice(0, 1);
    }

    openReschedule() {
        this.modalRef = this.modalService.open(RescheduleModalComponent);
    }
}
