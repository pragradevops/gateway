import { Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { InterviewsComponent } from './interviews.component';
import { InterviewsDetailComponent } from '../interviews/interviews-detail/interviews-detail.component';

export const interviewRoute: Routes = [
    {
        path: 'talent/interview',
        component: InterviewsComponent,
        data: {
            // authorities: ['ROLE_EMPLOYEE'],
            pageTitle: 'Aleeph Advisors | Interview'
        }
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'talent/interview-detail',
        component: InterviewsDetailComponent,
        data: {
            // authorities: ['ROLE_EMPLOYEE'],
            pageTitle: 'Aleeph Advisors | Interview'
        }
        // canActivate: [UserRouteAccessService]
    }
];
