import { Component, OnInit } from '@angular/core';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'jhi-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['dashboard.scss']
})
export class DashboardComponent implements OnInit {
    faCoffee = faCoffee;
    constructor() {}

    ngOnInit() {}
}
