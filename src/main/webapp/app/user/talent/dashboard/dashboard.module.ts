import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

// talent dashboard components
import { DashboardComponent } from './dashboard.component';
// jobs components
import { TrendingComponent } from './job/trending/trending.component';
import { AppliedComponent } from './job/applied/applied.component';
// interview components
import { InterviewComponent } from './interview/interview.component';
import { UpcomingComponent } from './interview/upcoming/upcoming.component';
import { AvailableComponent } from './interview/available/available.component';
// streaming components
import { StreamingComponent } from './streaming/streaming.component';

// HireNg modules
import { SharedMaterialModule } from 'app/shared/shared-material.module';
import { SharedModule } from '../../shared/shared.module';

// Dashboard route variable
import { DASHBOARD_ROUTE } from './dashboard.route';

@NgModule({
    imports: [
        CommonModule,
        SharedMaterialModule,
        NgbModule.forRoot(),
        // RouterModule.forChild(DASHBOARD_ROUTE),
        SharedModule,
        FontAwesomeModule
    ],
    declarations: [
        TrendingComponent,
        AppliedComponent,
        InterviewComponent,
        UpcomingComponent,
        AvailableComponent,
        StreamingComponent,
        DashboardComponent
    ]
})
export class DashboardModule {}
