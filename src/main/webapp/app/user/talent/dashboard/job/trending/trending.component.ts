import { Component, OnInit } from '@angular/core';
import { MatchingJobService } from '../../../service/matching-job.service';

declare let $: any;

@Component({
    selector: 'jhi-trending',
    templateUrl: './trending.component.html',
    styleUrls: ['./trending.scss']
})
export class TrendingComponent implements OnInit {
    src: string[] = [
        'http://greenchemistryandcommerce.org/assets/media/images/logos/connora.png',
        'http://greenchemistryandcommerce.org/assets/media/images/logos/connora.png',
        'http://greenchemistryandcommerce.org/assets/media/images/logos/connora.png',
        'http://greenchemistryandcommerce.org/assets/media/images/logos/connora.png',
        'http://greenchemistryandcommerce.org/assets/media/images/logos/connora.png',
        'http://greenchemistryandcommerce.org/assets/media/images/logos/connora.png'
    ];

    map1: any;
    activeSliderId;
    matchingJobs: any = {};
    matchingJobsFirst: any;

    constructor(private matchingJobService: MatchingJobService) {}

    ngOnInit() {
        $(document).ready(function() {
            $('.slider-icon-bar .like').click(function() {
                $(this).toggleClass('active');
            });
        });

        // pass a function to map
        this.map1 = this.src.map(function(x, index, arr) {
            if (index < arr.length) {
                return {
                    src: arr[index],
                    src1: arr[++index],
                    src2: arr[++index]
                };
            }
        });
        this.map1 = this.map1.slice(0, this.src.length - 2);
        this.getMatchingJobs();
        // console.log(this.map1);
    }

    getMatchingJobs(): void {
        this.matchingJobs = this.matchingJobService.getMatchingJobs();
        this.matchingJobsFirst = this.matchingJobs.slice(0, 1);
    }

    cycleToSlide(photo) {
        // console.log(photo.id - 1);
        const slideId = photo.id - 1;

        this.activeSliderId = 'ngb-slide-' + slideId;
        // console.log(this.activeSliderId);
    }
}
