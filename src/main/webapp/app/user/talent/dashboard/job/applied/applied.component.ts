import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

// Services
import { ModalService } from '../../../service/modal.service';
import { AppliedJobsService } from '../../../service/applied-jobs.service';

// Model classes
import { AppliedJob } from '../../../model/jobs/applied';

// componets
import { WithdrawalComponent } from '../../../modal/job/withdrawal/withdrawal.component';

@Component({
    selector: 'jhi-applied',
    templateUrl: './applied.component.html',
    styleUrls: ['./applied.scss']
})
export class AppliedComponent implements OnInit {
    appliedJobs: any = {};
    modalRef: NgbModalRef;

    constructor(private appliedJobsService: AppliedJobsService, private modalService: ModalService) {}

    ngOnInit() {
        this.getAppliedJobs();
    }

    getAppliedJobs(): void {
        this.appliedJobs = this.appliedJobsService.getAppliedJobs().slice(0, 4);
    }

    openWithdrawal() {
        this.modalRef = this.modalService.open(WithdrawalComponent);
    }
}
