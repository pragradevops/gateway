import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
// servive
import { ModalService } from '../../../service/modal.service';
import { UpcomingInterviewService } from '../../../service/interview/upcoming-interview.service';

// componets
import { CancelModalComponent, RescheduleModalComponent } from '../../../modal/interview';

@Component({
    selector: 'jhi-upcoming',
    templateUrl: './upcoming.component.html',
    styleUrls: ['./upcoming.scss']
})
export class UpcomingComponent implements OnInit {
    modalRef: NgbModalRef;
    upcomingInterviews: any = {};

    constructor(private modalService: ModalService, private upcomingInterviewService: UpcomingInterviewService) {}

    ngOnInit() {
        this.getUpcomingInterview();
    }
    getUpcomingInterview(): void {
        this.upcomingInterviews = this.upcomingInterviewService.getUpcomingInterviews().slice(0, 4);
        console.log(this.upcomingInterviews);
    }
    openCancel() {
        this.modalRef = this.modalService.open(CancelModalComponent);
    }
    openReschedule() {
        this.modalRef = this.modalService.open(RescheduleModalComponent);
    }
}
