import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

// components
import { AcceptModalComponent, CancelModalComponent, RescheduleModalComponent } from '../../../modal/interview';

// service
import { ModalService } from '../../../service/modal.service';
import { AvailableInterviewService } from '../../../service/interview/available-interview.service';

@Component({
    selector: 'jhi-available',
    templateUrl: './available.component.html',
    styleUrls: ['./available.scss']
})
export class AvailableComponent implements OnInit {
    availableInterviews: any = {};
    modalRef: NgbModalRef;

    constructor(private modalService: ModalService, private availableInterviewService: AvailableInterviewService) {}

    ngOnInit() {
        this.getAvailableInterview();
    }
    getAvailableInterview(): void {
        this.availableInterviews = this.availableInterviewService.getAvailableInterviews().slice(0, 4);
    }

    openAccept() {
        console.log('inn accept function');
        this.modalRef = this.modalService.open(AcceptModalComponent);
    }
    openCancel() {
        this.modalRef = this.modalService.open(CancelModalComponent);
    }
    openReschedule() {
        this.modalRef = this.modalService.open(RescheduleModalComponent);
    }
}
