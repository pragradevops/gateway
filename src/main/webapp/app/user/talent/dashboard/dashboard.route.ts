import { Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';

// component
import { DashboardComponent } from './dashboard.component';

export const DASHBOARD_ROUTE: Routes = [
    {
        path: 'talent',
        component: DashboardComponent,
        data: {
            pageTitle: 'Aleeph Advisors | Dashboard',
            authorities: ['ROLE_EMPLOYEE']
        },
        canActivate: [UserRouteAccessService]
    }
];
