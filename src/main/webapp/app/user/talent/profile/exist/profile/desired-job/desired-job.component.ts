import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'jhi-desired-job',
    templateUrl: './desired-job.component.html',
    styleUrls: ['./desired-job.scss']
})
export class DesiredJobComponent implements OnInit {
    constructor() {}

    ngOnInit() {}
    desiredToggle() {
        document.querySelector('.form-desired').classList.toggle('show');
    }
}
