import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'jhi-education',
    templateUrl: './education.component.html',
    styleUrls: ['./education.scss']
})
export class EducationComponent implements OnInit {
    constructor() {}

    ngOnInit() {}
    educationToggle() {
        document.querySelector('.form-education').classList.toggle('show');
    }
}
