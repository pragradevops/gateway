import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'jhi-skill',
    templateUrl: './skill.component.html',
    styleUrls: ['./skill.scss']
})
export class SkillComponent implements OnInit {
    constructor() {}

    ngOnInit() {}
    skillToggle() {
        document.querySelector('.form-skill').classList.toggle('show');
    }
}
