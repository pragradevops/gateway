import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// HireNg Modules
import { SharedModule } from 'app/user/shared/shared.module';
import { SharedMaterialModule } from 'app/shared/shared-material.module';
import { SharedProfileModule } from '../shared/shared.module';
// summary profile component
import { SummaryComponent } from './summary/summary.component';

// exist profile component
import { ProfileComponent } from './profile/profile.component';
import { DesiredJobComponent } from './profile/desired-job/desired-job.component';
import { EducationComponent } from './profile/education/education.component';
import { ExperienceComponent } from './profile/experience/experience.component';
import { SkillComponent } from './profile/skill/skill.component';
import { ResumeComponent } from './profile/resume/resume.component';

@NgModule({
    imports: [CommonModule, SharedMaterialModule, FormsModule, ReactiveFormsModule, SharedModule, SharedProfileModule],
    declarations: [
        SummaryComponent,
        ProfileComponent,
        DesiredJobComponent,
        EducationComponent,
        ExperienceComponent,
        SkillComponent,
        ResumeComponent
    ]
})
export class ExistModule {}
