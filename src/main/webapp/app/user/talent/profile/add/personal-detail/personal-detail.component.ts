import { Component, OnInit } from '@angular/core';
import { JhiEventManager } from 'ng-jhipster';
import { Observable } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

// service
import { FileUploadService } from '../../../service/file-upload.service';
import { PersonalDetailService } from '../../shared/service/personal-detail.service';

// model
import { GenderTypeEnum, PersonalDetail } from '../../shared/model/personal-detail.model';

// select input type json
import { GENDER } from '../../../mock-apis/gender';

//
import * as moment from 'moment';
import { Moment } from 'moment';

@Component({
    selector: 'jhi-personal-detail',
    templateUrl: './personal-detail.component.html',
    styleUrls: ['./personal-detail.scss']
})
export class PersonalDetailComponent implements OnInit {
    firstFormGroup: FormGroup;
    maxDate = new Date();

    // model
    personalDetail: PersonalDetail = {};
    isSaving: boolean;

    // select input type variables
    Gender: any[] = GENDER;

    // upload file
    selectedFiles: FileList;
    currentFileUpload: File;
    progress: { percentage: number } = { percentage: 0 };
    localImageURL = null;

    constructor(
        private _formBuilder: FormBuilder,
        private personalDetailService: PersonalDetailService,
        private fileUploadService: FileUploadService,
        private eventManager: JhiEventManager
    ) {}

    ngOnInit() {
        this.firstFormGroup = this._formBuilder.group({
            firstNameCtrl: ['', Validators.required],
            lastNameCtrl: ['', Validators.required],
            genderCtrl: ['', Validators.required],
            dobCtrl: ['', Validators.required],
            phoneNumberCtrl: ['', Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])],
            alternateNumberCtrl: ['', Validators.compose([Validators.minLength(10), Validators.maxLength(10)])]
        });
    }

    // image upload method to check image type
    selectFile(event) {
        const file = event.target.files.item(0);
        if (file.type.match('image.*')) {
            this.selectedFiles = event.target.files;
        } else {
            alert('invalid format!');
        }
        this.upload();
    }

    // image upload method
    upload() {
        this.progress.percentage = 0;
        this.currentFileUpload = this.selectedFiles.item(0);
        console.log(this.currentFileUpload);
        this.fileUploadService.pushFileToStorage(this.currentFileUpload).subscribe((res: string) => {
            this.localImageURL = res;
            console.log(this.localImageURL);
        });
    }

    //
    save() {
        this.isSaving = true;

        // convert date object into string('DD-MM-YYYY')
        // this.personalDetail.dateOfBirth = moment(this.personalDetail.dateOfBirth).format('DD-MM-YYYY');

        this.personalDetail.imageUrl = '';
        this.personalDetail.addressLine = '';
        this.personalDetail.lat = '';
        this.personalDetail.lng = '';

        // console the personalDetail
        console.log(this.personalDetail);
        this.subscribeToSaveResponse(this.personalDetailService.create(this.personalDetail));
    }

    private subscribeToSaveResponse(result: Observable<PersonalDetail>) {
        result.subscribe((res: PersonalDetail) => this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: PersonalDetail) {
        this.eventManager.broadcast({ name: 'profileDetailModification', content: 'OK' });
        this.isSaving = false;
    }

    private onSaveError() {
        this.isSaving = false;
    }

    // Error Messages Functions
    getFirstNameErrorMessage() {
        return this.firstFormGroup.get('firstNameCtrl').hasError('required') ? 'You must enter a value' : null;
    }

    getLastNameErrorMessage() {
        return this.firstFormGroup.get('lastNameCtrl').hasError('required') ? 'You must enter a value' : null;
    }

    getGenderErrorMessage() {
        return this.firstFormGroup.get('genderCtrl').hasError('required') ? 'You must enter a value' : null;
    }

    getDobErrorMessage() {
        return this.firstFormGroup.get('dobCtrl').hasError('required') ? 'You must enter a value' : null;
    }

    getPhoneNumberErrorMessage() {
        return this.firstFormGroup.get('phoneNumberCtrl').hasError('required')
            ? 'You must enter a value'
            : this.firstFormGroup.get('phoneNumberCtrl').hasError('minlength') ? 'You must enter correct number' : null;
    }

    getAlternateNumberErrorMessage() {
        return this.firstFormGroup.get('alternateNumberCtrl').hasError('minlength') ? 'You must enter correct number' : null;
    }
}
