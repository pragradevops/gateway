import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

// services
import { UploadResumeService } from '../../shared/service/upload-resume.service';
import { ProfileService } from '../../shared/service/profile.service';
import { Account, Principal } from 'app/core';

// model
import { UploadResume } from '../../shared/model/upload-resume.model';

@Component({
    selector: 'jhi-upload-resume',
    templateUrl: './upload-resume.component.html',
    styleUrls: ['./upload-resume.scss']
})
export class UploadResumeComponent implements OnInit {
    uploadResumeFormGroup: FormGroup;

    // userId variables
    account: any = {};
    userId: number;

    // profile variables
    userProfile: any = {};
    profileName: UploadResume = {};
    profileTitles: any = {};
    profileTitleExist = false;

    createdProfileName = null;
    isEdit = null;

    //
    isSaving: boolean;
    routeSub: any;

    // id's
    profileExist = false;
    profileId: string;

    constructor(
        private _formBuilder: FormBuilder,
        private uploadResumeService: UploadResumeService,
        private profileService: ProfileService,
        private eventManager: JhiEventManager,
        private principal: Principal,
        private router: Router,
        private route: ActivatedRoute
    ) {}

    ngOnInit() {
        this.uploadResumeFormGroup = this._formBuilder.group({
            profileNameCtrl: ['', Validators.required]
        });
        // fetching userId of the user
        this.getUserId();
    }

    // fetching the userId
    getUserId() {
        this.principal.identity().then(account => {
            this.account = account;
            this.userId = this.account.id;
            this.assignVariables();
        });
        // fetching profileId from router link
        this.profileIdByUrl();
    }

    // fetching profileId from router link
    profileIdByUrl() {
        this.routeSub = this.route.params.subscribe(params => {
            this.profileId = params['id'];
            if (this.profileId) {
                this.profileExist = true;
            }
            this.userProfileById(this.profileId);
        });
    }

    // fetching userProfile from a particular profileId
    userProfileById(id: string) {
        // if profile id exist, fetching profile to a particulat id
        if (id) {
            this.profileService.getProfile(id).subscribe((res: any) => {
                this.userProfile = res;
                // this.userProfileTitle();
            });
        }
    }

    //
    save() {
        this.isSaving = true;

        this.profileName.id = null;
        if (this.profileName.id !== undefined) {
            this.subscribeToSaveResponse(this.uploadResumeService.update(this.profileName));
        } else {
            this.subscribeToSaveResponse(this.uploadResumeService.create(this.profileName));
        }
    }

    private subscribeToSaveResponse(result: Observable<UploadResume>) {
        result.subscribe((res: UploadResume) => this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: UploadResume) {
        this.createdProfileName = result.profileName;
        console.log(this.createdProfileName);
        // on succes, route to the particular id
        this.router.navigate(['/talent/profile', result.id]);
        this.profileIdByUrl();
        this.eventManager.broadcast({ name: 'profileNameModification', content: 'OK' });
        this.isSaving = false;
    }

    private onSaveError() {
        this.isSaving = false;
    }

    assignVariables() {
        this.profileName.profileScore = 0.0;
        this.profileName.profileStatus = 'ACTIVE';
        this.profileName.resumeFileLocation = '';
        this.profileName.resumeFileName = '';
        this.profileName.resumeFileType = 'WORD2003';
        this.profileName.userId = this.userId;
    }

    // Error Messages Functions
    getProfileNameErrorMessage() {
        return this.uploadResumeFormGroup.get('profileNameCtrl').hasError('required') ? 'You must enter a value' : null;
    }
}
