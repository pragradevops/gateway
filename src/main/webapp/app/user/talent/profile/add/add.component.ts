import { Component, OnInit, ViewChild } from '@angular/core';

// profile wizard component
import { PersonalDetailComponent } from './personal-detail/personal-detail.component';
import { UploadResumeComponent } from './upload-resume/upload-resume.component';
import { ExperienceComponent } from './experience/experience.component';
import { EducationComponent } from './education/education.component';
import { SkillComponent } from './skill/skill.component';
import { DesiredJobComponent } from './desired-job/desired-job.component';

@Component({
    selector: 'jhi-add',
    templateUrl: './add.component.html',
    styleUrls: ['./add.scss']
})
export class AddComponent implements OnInit {
    @ViewChild('PersonalDetailComponent') personalDetailComponent: PersonalDetailComponent;
    @ViewChild('PersonalDetailComponent') uploadResumeComponent: UploadResumeComponent;
    @ViewChild('PersonalDetailComponent') experienceComponent: ExperienceComponent;
    @ViewChild('PersonalDetailComponent') educationComponent: EducationComponent;
    @ViewChild('PersonalDetailComponent') skillComponent: SkillComponent;
    @ViewChild('PersonalDetailComponent') desiredJobComponent: DesiredJobComponent;

    isLinear = false;

    constructor() {}

    ngOnInit() {}
}
