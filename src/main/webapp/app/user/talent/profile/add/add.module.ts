import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// HireNg Modules
import { SharedModule } from 'app/user/shared/shared.module';
import { SharedProfileModule } from '../shared/shared.module';
import { SharedMaterialModule } from 'app/shared/shared-material.module';

// component
import { AddComponent } from './add.component';
import { PersonalDetailComponent } from './personal-detail/personal-detail.component';
import { UploadResumeComponent } from './upload-resume/upload-resume.component';
import { DesiredJobComponent } from './desired-job/desired-job.component';
import { ExperienceComponent } from './experience/experience.component';
import { EducationComponent } from './education/education.component';
import { SkillComponent } from './skill/skill.component';

@NgModule({
    imports: [CommonModule, SharedMaterialModule, SharedProfileModule, FormsModule, ReactiveFormsModule, SharedModule],
    declarations: [
        AddComponent,
        PersonalDetailComponent,
        UploadResumeComponent,
        DesiredJobComponent,
        ExperienceComponent,
        EducationComponent,
        SkillComponent
    ]
})
export class AddModule {}
