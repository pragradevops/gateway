import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

// services
import { EducationService } from '../../shared/service/education.service';
import { ProfileService } from '../../shared/service/profile.service';

// Model
import { Education } from '../../shared/model/education.model';

import { PERFORMANCE_SCALE } from '../../../mock-apis/performanceScale';

@Component({
    selector: 'jhi-education',
    templateUrl: './education.component.html',
    styleUrls: ['./education.scss']
})
export class EducationComponent implements OnInit {
    // firstFormGroup: FormGroup;

    // //
    // isSaving: boolean;
    // routeSub: any;

    // // duration years
    // years: number[] = [];

    // // id's
    // profileExist = false;
    // profileId: string;

    // // education variables
    // userProfile: any = {};
    // education: Education = {};
    // educations: any = {};
    // educationExist = false;

    //
    // PerformanceScale: any[] = PERFORMANCE_SCALE;

    constructor(
        private _formBuilder: FormBuilder,
        private educationService: EducationService,
        private eventManager: JhiEventManager,
        private route: ActivatedRoute,
        private profileService: ProfileService
    ) {}

    ngOnInit() {
        // this.profileIdByUrl();
    }

    // fetching profileId from router link
    // profileIdByUrl() {
    //     this.routeSub = this.route.params.subscribe(params => {
    //         this.profileId = params['id'];
    //         if (this.profileId) {
    //             this.profileExist = true;
    //         }
    //         this.userProfileById(this.profileId);
    //     });
    // }

    // // fetching userProfile from a particular profileId
    // userProfileById(id: string) {
    //     // if profile id exist, fetching profile to a particulat id
    //     if (id) {
    //         this.profileService.getProfile(id).subscribe((res: any) => {
    //             this.userProfile = res;
    //         });
    //     }
    // }

    // private subscribeToSaveResponse(result: Observable<Education>) {
    //     result.subscribe((res: Education) => this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    // }

    // private onSaveSuccess(result: Education) {
    //     this.eventManager.broadcast({ name: 'educationListModification', content: 'OK' });
    //     this.isSaving = false;
    //     // on successfully saving, calling the profile
    //     this.profileIdByUrl();
    // }

    // private onSaveError() {
    //     this.isSaving = false;
    // }
}
