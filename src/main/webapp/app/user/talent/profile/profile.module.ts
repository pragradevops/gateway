import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// HireNg Modules
import { AddModule } from './add/add.module';
import { ExistModule } from './exist/exist.module';
import { SharedProfileModule } from './shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        // HireNg Modules
        AddModule,
        ExistModule,
        SharedProfileModule
    ],
    declarations: []
})
export class ProfileModule {}
