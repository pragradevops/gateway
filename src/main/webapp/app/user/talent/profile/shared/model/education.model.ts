import { BaseEntity } from 'app/shared/model/base-entity';

export class Education implements BaseEntity {
    constructor(
        public id?: string,
        public achieved?: string,
        public countryId?: string,
        public currentlyPursuing?: true,
        public discipline?: string,
        public educationType?: string,
        public endYear?: string,
        public instituteSchoolName?: string,
        public minor?: string,
        public performanceScale?: string,
        public profileId?: string,
        public startYear?: string,
        public universityBoardName?: string
    ) {}
}
