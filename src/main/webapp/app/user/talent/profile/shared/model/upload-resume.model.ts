import { BaseEntity } from 'app/shared/model/base-entity';

export class UploadResume implements BaseEntity {
    constructor(
        public id?: string,
        public profileName?: string,
        public profileScore?: number,
        public profileStatus?: string,
        public resumeFileLocation?: string,
        public resumeFileName?: string,
        public resumeFileType?: string,
        public userId?: number
    ) {}
}
