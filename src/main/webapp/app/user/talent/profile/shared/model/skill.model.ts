import { BaseEntity } from 'app/shared/model/base-entity';

export enum ProficiencyLevelEnum {
    BEGINNER = 'BEGINNER',
    INTERMEDIATE = 'INTERMEDIATE',
    ADVANCE = 'ADVANCE'
}

export class Skill implements BaseEntity {
    constructor(
        public id?: string,
        public lastUsedYear?: number,
        public proficiencyLevel?: ProficiencyLevelEnum,
        public profileId?: string,
        public selfRate?: number,
        public skillName?: string,
        public yearsUsed?: number
    ) {}
}
