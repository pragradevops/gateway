import { SharedProfileModule } from './shared.module';

describe('SharedModule', () => {
    let sharedModule: SharedProfileModule;

    beforeEach(() => {
        sharedModule = new SharedProfileModule();
    });

    it('should create an instance', () => {
        expect(sharedModule).toBeTruthy();
    });
});
