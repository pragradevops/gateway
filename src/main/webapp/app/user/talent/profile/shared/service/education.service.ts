import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { SERVER_API_URL } from 'app/app.constants';
import { HttpService } from 'app/shared/httpservice/http.service';

// model
import { Education } from '../model/education.model';

@Injectable({
    providedIn: 'root'
})
export class EducationService extends HttpService<Education> {
    constructor(http: HttpClient) {
        super(http, SERVER_API_URL, '/profilems/api/education-profiles');
    }
}
