import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SERVER_API_URL } from 'app/app.constants';

// service
import { Principal } from 'app/core';

@Injectable({
    providedIn: 'root'
})
export class ProfileService {
    private allProfilesUrl = SERVER_API_URL + '/profilems/api/complete-profile_1';
    private particularProfileUrl = SERVER_API_URL + '/profilems/api/complete-profile';
    private basicUserDetailUrl = SERVER_API_URL + '/api/account';

    constructor(private http: HttpClient, private principal: Principal) {}

    // method to fetch profile for a particular profileId
    getProfile(id: string): any {
        return this.http.get(`${this.particularProfileUrl}/${id}`).pipe(
            map((res: Response) => {
                return res.json();
            })
        );
    }
}
