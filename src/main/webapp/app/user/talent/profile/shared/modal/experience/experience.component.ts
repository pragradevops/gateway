import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

//
import { JOB_TYPE } from '../../../../mock-apis/jobType';

@Component({
    selector: 'jhi-experience-modal',
    templateUrl: './experience.component.html',
    styleUrls: ['./experience.scss']
})
export class ExperienceComponent implements OnInit {
    firstFormGroup: FormGroup;

    //
    JobType: any[] = JOB_TYPE;
    currentEmployer = true;

    constructor(private _formBuilder: FormBuilder) {}

    ngOnInit() {
        this.firstFormGroup = this._formBuilder.group({
            designationCtrl: ['', Validators.required],
            companyNameCtrl: ['', Validators.required],
            jobTypeCtrl: ['', Validators.required],
            locationCtrl: ['', Validators.required],
            descriptionCtrl: [''],
            startDateCtrl: ['', Validators.required],
            endDateCtrl: ['', Validators.required],
            currentlyPursuingCtrl: ['']
        });
    }

    // Error Messages Functions
    getDesignationErrorMessage() {
        return this.firstFormGroup.get('designationCtrl').hasError('required') ? 'You must enter a value' : null;
    }

    getCompanyNameErrorMessage() {
        return this.firstFormGroup.get('companyNameCtrl').hasError('required') ? 'You must enter a value' : '';
    }

    getJobTypeErrorMessage() {
        return this.firstFormGroup.get('jobTypeCtrl').hasError('required') ? 'You must enter a value' : '';
    }

    getLocationErrorMessage() {
        return this.firstFormGroup.get('locationCtrl').hasError('required') ? 'You must enter a value' : '';
    }

    save() {
        console.log('saved');
    }
}
