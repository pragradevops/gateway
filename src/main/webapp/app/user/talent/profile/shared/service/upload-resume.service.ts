import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { SERVER_API_URL } from 'app/app.constants';
import { HttpService } from 'app/shared/httpservice/http.service';

// model
import { UploadResume } from '../model/upload-resume.model';

@Injectable({
    providedIn: 'root'
})
export class UploadResumeService extends HttpService<UploadResume> {
    constructor(http: HttpClient) {
        super(http, SERVER_API_URL, '/profilems/api/profiles');
    }
}
