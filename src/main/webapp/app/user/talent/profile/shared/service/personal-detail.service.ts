import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { SERVER_API_URL } from 'app/app.constants';
import { HttpService } from 'app/shared/httpservice/http.service';
import { PersonalDetail } from '../model/personal-detail.model';

@Injectable({
    providedIn: 'root'
})
export class PersonalDetailService extends HttpService<PersonalDetail> {
    constructor(http: HttpClient) {
        super(http, SERVER_API_URL, '/api/basic-profile');
    }
}
