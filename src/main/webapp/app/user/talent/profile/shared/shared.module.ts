import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// HireNg modules
import { SharedMaterialModule } from 'app/shared/shared-material.module';

// Modal Components
import { DesiredJobComponent } from './modal/desired-job/desired-job.component';
import { EducationComponent } from './modal/education/education.component';
import { ExperienceComponent } from './modal/experience/experience.component';
// import { PersonalDetailComponent } from './modal/personal-detail/personal-detail.component';
import { SkillComponent } from './modal/skill/skill.component';

@NgModule({
    imports: [CommonModule, SharedMaterialModule, FormsModule, ReactiveFormsModule, NgbModule],
    declarations: [EducationComponent, DesiredJobComponent, ExperienceComponent, SkillComponent],
    exports: [EducationComponent, DesiredJobComponent, ExperienceComponent, SkillComponent]
})
export class SharedProfileModule {}
