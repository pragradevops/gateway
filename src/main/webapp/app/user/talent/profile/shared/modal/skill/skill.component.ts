import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

// services
import { SkillService } from '../../../shared/service/skill.service';
import { ProfileService } from '../../../shared/service/profile.service';

// model
import { Skill, ProficiencyLevelEnum } from '../../model/skill.model';

// mock-apis
import { EXPERIENCE } from '../../../../mock-apis/experience';
import { LAST_USED } from '../../../../mock-apis/skills';
import { PROFIENCY_LEVEL } from '../../../../mock-apis/profiencyLevel';

@Component({
    selector: 'jhi-skill-modal',
    templateUrl: './skill.component.html',
    styleUrls: ['./skills.scss']
})
export class SkillComponent implements OnInit {
    // formGroup variable
    skillFormGroup: FormGroup;

    // profile variable
    isSaving: boolean;
    profileId: string;
    profileExist = false;

    // skills variables
    userProfile: any = {};
    skill: Skill = {};
    // skills: Skill[] = [];
    skillExist = false;
    selfRate = 0;

    // routing variable
    routeSub: any;

    // mock-apis variables
    Experience: any[] = EXPERIENCE;
    LastUsed: any[] = LAST_USED;
    ProfiencyLevel: any[] = PROFIENCY_LEVEL;

    constructor(
        private _formBuilder: FormBuilder,
        private skillService: SkillService,
        private eventManager: JhiEventManager,
        private route: ActivatedRoute,
        private profileService: ProfileService
    ) {}

    ngOnInit() {
        this.skillFormGroup = this._formBuilder.group({
            skillNameCtrl: ['', Validators.required],
            profiencyLevelCtrl: ['', Validators.required],
            experienceCtrl: [''],
            LastUsedYearCtrl: ['']
        });
        this.skill.selfRate = 0;
        // this.getProfileIdByRouteUrl();
    }

    // // fetching profileId from router link
    // getProfileIdByRouteUrl() {
    //     this.routeSub = this.route.params.subscribe((params) => {
    //         this.profileId = params['id'];
    //         if (this.profileId) {
    //             this.profileExist = true;
    //         }
    //         // fetch user profile by fetched route id
    //         this.getUserProfileByRouteId(this.profileId);
    //     });
    // }

    // // fetching userProfile from the fetched routeUrl profileId
    // getUserProfileByRouteId(id: string) {
    //     // if profile id exist, fetching profile to a particulat id
    //     if (id) {
    //         this.profileService.getProfile(id)
    //             .subscribe((res: any) => {
    //                 this.userProfile = res;
    //                 // assign skillDTO from fetched userProfile to a local variable
    //                 this.skillDTOFromProfile();
    //             });
    //     }
    // }

    // // saving workExperienceList from userProfile
    // skillDTOFromProfile() {
    //     this.skillExist = true;
    //     this.skills = this.userProfile.skillProfileDTOList;
    //     console.log(this.skills);
    // }

    //
    save() {
        // this.isSaving = true;
        // this.skill.id = null;
        // this.skill.profileId = this.profileId;
        // console.log(this.skill);
        // // create the skill
        // this.subscribeToSaveResponse(
        //     this.skillService.create(this.skill));
    }

    // private subscribeToSaveResponse(result: Observable<Skill>) {
    //     result.subscribe((res: Skill) => this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    // }

    // private onSaveSuccess(result: Skill) {
    //     this.eventManager.broadcast({ name: 'skillListModification', content: 'OK' });
    //     this.isSaving = false;
    //     // on successfully saving, re-call the profile
    //     this.getProfileIdByRouteUrl();
    // }

    // private onSaveError() {
    //     this.isSaving = false;
    // }

    // Error Messages Functions
    getSkillNameErrorMessage() {
        return this.skillFormGroup.get('skillNameCtrl').hasError('required') ? 'You must enter a value' : null;
    }
    getProfiencyLevelErrorMessage() {
        return this.skillFormGroup.get('profiencyLevelCtrl').hasError('required') ? 'You must enter a value' : null;
    }
}
