import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { SERVER_API_URL } from 'app/app.constants';
import { HttpService } from 'app/shared/httpservice/http.service';
import { Skill } from '../model/skill.model';

@Injectable({
    providedIn: 'root'
})
export class SkillService extends HttpService<Skill> {
    constructor(http: HttpClient) {
        super(http, SERVER_API_URL, '/profilems/api/skill-profiles');
    }
}
