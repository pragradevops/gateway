import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { JOB_TYPE } from '../../../../mock-apis/jobType';

@Component({
    selector: 'jhi-desired-job-modal',
    templateUrl: './desired-job.component.html',
    styleUrls: ['./desired-job.scss']
})
export class DesiredJobComponent implements OnInit {
    firstFormGroup: FormGroup;
    JobType: any[] = JOB_TYPE;

    constructor(private _formBuilder: FormBuilder) {}

    ngOnInit() {
        this.firstFormGroup = this._formBuilder.group({
            DesiredDesignationCtrl: ['', Validators.required],
            EmploymentTypeCtrl: ['', Validators.required],
            JobLocationCtrl: ['', Validators.required],
            JobTypeCtrl: ['', Validators.required]
        });
    }
    // Error Messages Functions
    getDesiredDesignationErrorMessage() {
        return this.firstFormGroup.get('DesiredDesignationCtrl').hasError('required') ? 'You must enter a value' : null;
    }
    getEmploymentTypeErrorMessage() {
        return this.firstFormGroup.get('EmploymentTypeCtrl').hasError('required') ? 'You must enter a value' : null;
    }
    getJobTypeErrorMessage() {
        return this.firstFormGroup.get('JobTypeCtrl').hasError('required') ? 'You must enter a value' : '';
    }
    getJobLocationErrorMessage() {
        return this.firstFormGroup.get('JobLocationCtrl').hasError('required') ? 'You must enter a value' : null;
    }
}
