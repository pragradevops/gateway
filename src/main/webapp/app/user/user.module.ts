import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// HireNG Modules
import { TalentModule } from './talent/talent.module';
import { EmployerModule } from './employer/employer.module';

@NgModule({
    imports: [CommonModule, TalentModule, EmployerModule],
    declarations: [],
    providers: []
})
export class UserModule {}
