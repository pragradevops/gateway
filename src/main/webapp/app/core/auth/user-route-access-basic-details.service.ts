import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';

import { Account, Principal, StateStorageService } from 'app/core';

@Injectable({ providedIn: 'root' })
export class UserRouteAccessBasicDetailsService implements CanActivate {
    account: Account;

    constructor(private router: Router, private principal: Principal, private stateStorageService: StateStorageService) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Promise<boolean> {
        const authorities = route.data['authorities'];

        // We need to call the checkSession / and so the authenticateSession.getTokenState() function, to ensure,
        // that the client has a principal too, if they already logged in by the server.
        // This could happen on a page refresh.

        return this.checkSession(authorities, state.url);
    }

    checkSession(authorities: string[], url: string): boolean | Promise<boolean> {
        alert('You basic data has been saved');

        return Promise.resolve(
            this.principal.identity().then(account => {
                this.account = account;

                console.log(this.account);
                // set the property, accountAuthority with the user's account athorities[0]

                if (this.account.firstName) {
                    return false;
                } else {
                    return true;
                }
            })
        );
    }
}
