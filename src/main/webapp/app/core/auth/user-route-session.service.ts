import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Account, AuthServerProvider, Principal, StateStorageService } from 'app/core';

@Injectable({ providedIn: 'root' })
export class UserRouteSessionService implements CanActivate {
    account: Account;

    constructor(
        private router: Router,
        private principal: Principal,
        private authenticateSession: AuthServerProvider,
        private stateStorageService: StateStorageService
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Promise<boolean> {
        const authorities = route.data['authorities'];
        // We need to call the checkSession / and so the authenticateSession.getTokenState() function, to ensure,
        // that the client has a principal too, if they already logged in by the server.
        // This could happen on a page refresh.

        return this.checkSession(authorities, state.url);
    }

    checkSession(authorities: string[], url: string): boolean | Promise<boolean> {
        const authenticateSession = this.authenticateSession;

        console.log('token state', authenticateSession.getToken());
        if (authenticateSession.getToken()) {
            return Promise.resolve(
                this.principal.identity().then(account => {
                    this.account = account;

                    console.log(this.account);

                    // set the property, accountAuthority with the user's account athorities[0]
                    const accountAuthority = this.account.authorities[0];
                    console.log(accountAuthority);

                    if (!accountAuthority) {
                        return true;
                    }

                    if (this.account) {
                        // logic to set the next routeUrl, have to mature the logic.
                        // tslint:disable-next-line:max-line-length
                        let routeUrl;
                        if (this.principal.hasAnyAuthority(['ROLE_ADMIN', 'ROLE_USER'])) {
                            routeUrl = 'admin/home';
                        } else if (this.principal.hasAnyAuthority(['ROLE_EMPLOYEE'])) {
                            routeUrl = 'talent/dashboard';
                        } else if (this.principal.hasAnyAuthority(['ROLE_TALENT'])) {
                            routeUrl = 'talent/dashboard';
                        } else if (this.principal.hasAnyAuthority(['ROLE_EMPLOYER'])) {
                            routeUrl = 'employer/dashboard';
                        } else {
                            routeUrl = 'accessdenied';
                        }

                        // //const routeUrl =
                        //     accountAuthority === 'ROLE_EMPLOYEE'
                        //         ? this.account.firstName !== null ? 'talent/dashboard' : 'talent/basic-details'
                        //         : accountAuthority === 'ROLE_EMPLOYER'
                        //             ? this.account.firstName !== null ? 'employer/dashboard' : 'employer/basic-profile'
                        //             : 'accessdenied';
                        console.log(routeUrl);

                        this.stateStorageService.storeUrl(url);
                        this.router.navigate([routeUrl]);

                        return false;
                    }
                })
            );
        } else {
            return true;
        }
    }
}
