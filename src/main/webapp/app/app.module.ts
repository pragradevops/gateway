import './vendor.ts';

import { Injector, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { LocalStorageService, Ng2Webstorage, SessionStorageService } from '@rars/ngx-webstorage';
import { JhiEventManager } from 'ng-jhipster';

import { AuthInterceptor } from './blocks/interceptor/auth.interceptor';
import { AuthExpiredInterceptor } from './blocks/interceptor/auth-expired.interceptor';
import { ErrorHandlerInterceptor } from './blocks/interceptor/errorhandler.interceptor';
import { NotificationInterceptor } from './blocks/interceptor/notification.interceptor';
import { NghireSharedModule } from 'app/shared';
import { GatewayCoreModule } from 'app/core';
import { NghireAppRoutingModule } from './app-routing.module';
import { GatewayHomeModule } from './home/home.module';
import { NghireAccountModule } from './account/account.module';
import { NghireEntityModule } from './entities/entity.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

// jhipster-needle-angular-add-module-import JHipster will add new module here
import { ErrorComponent, JhiMainComponent, NavbarComponent, PageRibbonComponent } from './layouts';

// HireNg Modules
// import { SharedMaterialModule } from 'app/shared/shared-material.module';
import { CommonNavFooterModule } from 'app/shared/common/common.module';
import { NgHireCommonModule } from './layouts/common/common.module';
import { NghireAdminModule } from 'app/admin/admin.module';
import { UserModule } from './user/user.module';

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        NghireAppRoutingModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-' }),
        NghireSharedModule,
        GatewayCoreModule,
        GatewayHomeModule,
        NghireAccountModule,
        NghireEntityModule,
        NghireAdminModule,
        FontAwesomeModule,
        // jhipster-needle-angular-add-module JHipster will add new module here
        // --------------
        // HitreNg Modules import
        // SharedMaterialModule,
        CommonNavFooterModule,
        NgHireCommonModule,
        NghireAdminModule,
        UserModule
    ],
    declarations: [JhiMainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true,
            deps: [LocalStorageService, SessionStorageService]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthExpiredInterceptor,
            multi: true,
            deps: [Injector]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ErrorHandlerInterceptor,
            multi: true,
            deps: [JhiEventManager]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: NotificationInterceptor,
            multi: true,
            deps: [Injector]
        }
    ],
    bootstrap: [JhiMainComponent]
})
export class NghireAppModule {}
