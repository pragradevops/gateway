import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { NghireSharedModule } from 'app/shared';
import { HomeComponent } from 'app/home/home.component';
import { HOME_ROUTE } from 'app/layouts/common/home';

@NgModule({
    imports: [NghireSharedModule, RouterModule.forChild([HOME_ROUTE])],
    declarations: [HomeComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayHomeModule {}
