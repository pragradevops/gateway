import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { NghireSharedModule } from 'app/shared';
import { GatewaySharedCommonModule } from '../shared/shared-common.module';
import { SharedMaterialModule } from '../shared/shared-material.module';

import {
    accountState,
    ActivateComponent,
    ActivateUserComponent,
    RegisterComponent,
    PasswordComponent,
    PasswordResetFinishComponent,
    PasswordResetInitComponent,
    PasswordStrengthBarComponent,
    SettingsComponent,
    SocialAuthComponent,
    SocialRegisterComponent
} from './';

@NgModule({
    imports: [NghireSharedModule, RouterModule.forChild(accountState), GatewaySharedCommonModule, SharedMaterialModule],
    declarations: [
        SocialRegisterComponent,
        SocialAuthComponent,
        ActivateComponent,
        ActivateUserComponent,
        RegisterComponent,
        PasswordComponent,
        PasswordStrengthBarComponent,
        PasswordResetInitComponent,
        PasswordResetFinishComponent,
        SettingsComponent
    ],

    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NghireAccountModule {}
