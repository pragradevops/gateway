import { Route } from '@angular/router';

import { UserRouteAccessService } from 'app/core';
import { ActivateUserComponent } from './activate-user.component';

export const activateUserRoute: Route = {
    path: 'activate-user',
    component: ActivateUserComponent,
    data: {
        authorities: [],
        pageTitle: 'Activation Account'
    },
    canActivate: [UserRouteAccessService]
};
