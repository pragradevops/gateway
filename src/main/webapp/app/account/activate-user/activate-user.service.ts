import { Injectable } from '@angular/core';
import { SERVER_API_URL } from '../../app.constants';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class ActivateUserService {
    constructor(private http: HttpClient) {}

    save(RegisterUser: any) {
        return this.http.post(SERVER_API_URL + 'api/activate-account', RegisterUser);
    }
}
