import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { ActivateUserService } from './activate-user.service';
import { LoginModalService } from 'app/core';

declare let $: any;

@Component({
    selector: 'jhi-activate-user',
    templateUrl: './activate-user.component.html',
    styleUrls: ['./activate-user.scss']
})
export class ActivateUserComponent implements OnInit {
    confirmPassword: string;
    doNotMatch: string;
    error: string;
    keyMissing: boolean;
    registerUser: any;
    success: string;
    modalRef: NgbModalRef;
    key: string;

    constructor(
        private activateUserService: ActivateUserService,
        private loginModalService: LoginModalService,
        private route: ActivatedRoute
    ) {}

    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            this.key = params['key'];
        });
        console.log(this.key);
        this.registerUser = {};
        this.registerUser.key = this.key;
        this.keyMissing = !this.key;
    }

    accountActivation() {
        this.doNotMatch = null;
        this.error = null;
        if (this.registerUser.password !== this.confirmPassword) {
            this.doNotMatch = 'ERROR';
        } else {
            this.activateUserService.save(this.registerUser).subscribe(
                () => {
                    this.success = 'OK';
                },
                () => {
                    this.success = null;
                    this.error = 'ERROR';
                }
            );
        }
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }
    hd(hello) {
        $(hello).css({
            display: 'none'
        });
    }
    sw(hello) {
        $(hello).css({
            display: 'block'
        });
    }
}
