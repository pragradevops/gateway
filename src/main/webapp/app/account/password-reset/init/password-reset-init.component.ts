import { AfterViewInit, Component, ElementRef, OnInit, Renderer } from '@angular/core';
import { EMAIL_NOT_FOUND_TYPE } from 'app/shared';
import { PasswordResetInitService } from './password-reset-init.service';
import { NgbModalRef, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LoginModalService } from 'app/core';
@Component({
    selector: 'jhi-password-reset-init',
    templateUrl: './password-reset-init.component.html',
    styleUrls: ['./password-reset.scss']
})
export class PasswordResetInitComponent implements OnInit, AfterViewInit {
    error: string;
    errorEmailNotExists: string;
    resetAccount: any;
    success: string;
    modalRef: NgbModalRef;
    email: {
        required: true;
    };
    constructor(
        private passwordResetInitService: PasswordResetInitService,
        private elementRef: ElementRef,
        private renderer: Renderer,
        public activeModal: NgbActiveModal,
        private loginModalService: LoginModalService
    ) {}

    ngOnInit() {
        this.resetAccount = {};
    }
    ngAfterViewInit() {}
    requestReset() {
        this.error = null;
        this.errorEmailNotExists = null;
        console.log(this.errorEmailNotExists, this.error, this.success);

        this.passwordResetInitService.save(this.resetAccount.email).subscribe(
            () => {
                this.success = 'OK';
            },
            response => {
                this.success = null;
                if (response.status === 400 && response.error.type === EMAIL_NOT_FOUND_TYPE) {
                    this.errorEmailNotExists = 'ERROR';
                } else {
                    this.error = 'ERROR';
                }
            }
        );
    }
    openLogin() {
        this.activeModal.dismiss('to state login');
        this.modalRef = this.loginModalService.open();
    }
}
