import { Component, ElementRef, OnInit, Renderer } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { NgbModalRef, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { EMAIL_ALREADY_USED_TYPE, LOGIN_ALREADY_USED_TYPE } from 'app/shared';
import { LoginModalService } from 'app/core';

import { Register } from './register.service';

declare let $: any;
// const LoginModalService = forwardRef(() => LoginModalService);
@Component({
    selector: 'jhi-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.scss']
})
export class RegisterComponent implements OnInit {
    confirmPassword: string;
    doNotMatch: string;

    error: string;
    errorEmailExists: string;
    errorUserExists: string;
    registerAccount: any;
    success: boolean;
    modalRef: NgbModalRef;
    isEmployer = {
        value: false,
        placeholder: 'Email ID'
    };

    constructor(
        private loginModalService: LoginModalService,
        public activeModal: NgbActiveModal,
        private registerService: Register,
        private elementRef: ElementRef,
        private renderer: Renderer
    ) {}

    ngOnInit() {
        this.success = false;
        this.registerAccount = {};
        // by default: role_employee
        this.registerAccount.role = 'ROLE_EMPLOYEE';
    }

    cancel() {
        this.registerAccount = {};
    }

    register() {
        if (this.registerAccount.password !== this.confirmPassword) {
            this.doNotMatch = 'ERROR';
        } else {
            this.doNotMatch = null;
            this.error = null;
            this.errorUserExists = null;
            this.errorEmailExists = null;
            this.registerAccount.langKey = 'en';
            this.registerService.save(this.registerAccount).subscribe(
                () => {
                    this.success = true;
                },
                response => this.processError(response)
            );
        }
    }

    openLogin() {
        this.activeModal.dismiss('to state login');
        this.modalRef = this.loginModalService.open();
    }

    // method to handle user-role
    userRole(role: string): void {
        this.isEmployer.value = role === 'EMPLOYER' ? true : false;
        this.isEmployer.placeholder = role === 'EMPLOYER' ? 'Please enter only company email id' : 'Email ID';
    }

    private processError(response: HttpErrorResponse) {
        this.success = null;
        if (response.status === 400 && response.error.type === LOGIN_ALREADY_USED_TYPE) {
            this.errorUserExists = 'ERROR';
        } else if (response.status === 400 && response.error.type === EMAIL_ALREADY_USED_TYPE) {
            this.errorEmailExists = 'ERROR';
        } else {
            this.errorEmailExists = 'ERROR';
        }
    }
}
