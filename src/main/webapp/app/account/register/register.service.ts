import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class Register {
    constructor(private http: HttpClient) {}

    save(account: any): Observable<any> {
        // console/.log(this.http.post(SERVER_API_URL + 'api/register-email', account));
        return this.http.post(SERVER_API_URL + 'api/register-email', account).pipe(
            map((res: Response) => {
                console.log(res);
                // const jsonResponse = res.json();
                // console.log(jsonResponse);
            })
        );
    }
}
