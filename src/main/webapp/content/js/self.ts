//
// /* Start jQuery Coding */
//
//
// 	/* end jQuery coding */
//
//
// 		/* Scrolling Effect Scripting */
//
// window.onscroll = function() {myFunction()};
//
//      var navbar = document.getElementById("navbar");
//      var sticky = navbar.offsetTop;
//
//      function myFunction() {
//        if (window.pageYOffset >= sticky) {
//          navbar.classList.add("sticky")
//          document.getElementById("fixed-top").style.background='#fff';
//        } else {
//          navbar.classList.remove("sticky");
//          document.getElementById("fixed-top").style.background='rgba(255, 255, 255, 0.9)';
//        }
//      }
//
//    /* /end scrolling effect */
//
//
//
//    /* Start Map Scripting */
//
//
//              function initMap(){
//                  // Start Map Location
//                  var location = {lat:28.628454 , lng:72.376945};
//                  var map = new google.maps.Map(document.getElementById("Amap"),{
//                      zoom:6,
//                      center:location
//                  });
//                      // End Map Location
//
//                  // Start addMarker function
//                 function addMarker(cordinates,title){
//
//                     // marker position
//                     var marker = new google.maps.Marker({
//                         position:cordinates,
//                         map:map
//                     });
//
//                     // marker title
//                         var infoWindow = new google.maps.InfoWindow({
//                             content:title
//                         });
//
//                  // title events
//                         marker.addListener('mouseover', function(){
//                             infoWindow.open(map, marker);
//                         });
//                         marker.addListener('mouseout', function(){
//                             infoWindow.close();
//                         });
//                     };
//
//                     // addMarker function end
//
//
//                     addMarker({lat:28.648454 , lng:72.376945},"<p>Mohd. Husain <br /> Aleeph Jr. Web Developer </p>");
//                     addMarker({lat:29.648454 , lng:73.366945},"<p>Ashutosh Sir <br /> Aleeph Sr. Java Developer </p>");
//                     addMarker({lat:28.658454 , lng:71.386945},"<p>Himanshu Sir <br /> Aleeph Sr. Angular Developer </p>");
//                     addMarker({lat:29.658454 , lng:70.396945},"<p>Anwar Sir <br /> Aleeph Sr. Graphic Designer </p>");
//                  };
//
//          /* End Map Scripting */
//
